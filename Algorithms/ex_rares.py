intrebari = ["Care este solutia ecuatiei x-2=2?","Care este capitala Angliei?","Cati cai se pot inhama maxim la o caruta?","Cat inseamna pi?(doar 2 zecimale)"]
raspunsuri = ["4","londra","6","3.14"]
numaratoare_corecte=0
k=0

count_intrebari=0
while True:
    for i in range(len(intrebari)):
        count_intrebari+=1
        instiintare_intrebari = f"Intrebarea {i+1} din {len(intrebari)}:"
        print(instiintare_intrebari)
        ghici = input(f"{intrebari[i]} \n Raspuns: ")
        ghici = ghici.lower()


        if ghici == raspunsuri[i]:  # Am schimbat cele doua if-uri intre ele. Prima data verifici daca a rapsuns bine
            # apoi treci pe elif, si bagi in while.
            numaratoare_corecte += 1
            print("Corect!")
            print("\n")
        elif ghici != raspunsuri[i]:
            k = 0 # aici e important sa faci k =0 dupa fiecare raspuns, pentru ca daca nu il faci aici cu 0, iti va da 3 incercari pentru
            # toate intrebarile, nu 3 incercari pe intrebare
            while ghici != raspunsuri[i]:
                print("\n")
                print(f"Gresit!  Mai ai {3-k-1} incercari!")
                ghici = input(f"{intrebari[i]} \n Raspuns: ")
                k+=1
                if ghici == raspunsuri[i]:
                    print("Corect")
                    break
                elif k ==2 and ghici != raspunsuri[i]:
                    print("\n")
                    print("Din pacate ai epuizat incercarile pentru aceasta intrebare!")
                    print("\n")
                    break
                # elif k == 2 and ghici == raspunsuri[i]: # sintaxa asta este in plus.. deja ai pus-o mai sus
                #     break


    if numaratoare_corecte==4 and len(intrebari) == count_intrebari:
        print(f"Felicitari ai raspuns corect la toate intrebarile!")
    elif numaratoare_corecte==0 and len(intrebari) == count_intrebari:
        nu_ai_raspuns = "Nu ai raspuns corect la nicio intrebare!"
        print(nu_ai_raspuns)
    else:
        if len(intrebari) == count_intrebari:
            lacate_intrebari = f"Ai raspuns corect la {numaratoare_corecte} din {len(intrebari)} intrebari"
            print(lacate_intrebari)
    repetam_raspuns = input("Repetam? da/nu  ")
    while repetam_raspuns not in ("da","nu"):
        repetam_raspuns = input("Repetam? da/nu  ")
    if repetam_raspuns == "nu":
        break