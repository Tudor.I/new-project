from timer import time_execution
import time
import datetime
import functools
# @time_execution
import cProfile
#1) Recursiv:
# @time_execution
def fibonacci_recursive(n):
    start = time.time()
    if n == 0 or n == 1:
        return n
    else:
        return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)

def fibonaccii(n):
    if n <= 0:
        print("Incorrect input")
    # First Fibonacci number is 0
    elif n == 1:
        return 0
    # Second Fibonacci number is 1
    elif n == 2:
        return 1
    else:
        return fibonaccii(n - 1) + fibonaccii(n - 2)
# start4 = time.time()
# for n in range(1, 10000):
#     print(n, ":", fibonaccii(n))
# """http://courses.csail.mit.edu/6.01/spring07/lectures/lecture4.pdf"""
# stop4 = start4 - time.time()

# print(stop4)
#
# print(fibonacci_recursive(30))

start = time.time()
cProfile.run('fibonacci_recursive(30)')
stop = time.time() - start
print("Timp de executie, varianta recursiva","pentru n = 30", stop)
# @time_execution
#2) Fibonacci for:
def fibonacci_iter(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

start1 = time.time()
cProfile.run('fibonacci_iter(200000)')
stop1 = time.time() - start1
print("Timp de executie, varianta for","pentru n = 200000", stop1)
# @time_execution
def fibonacci_generator(n):
    a = 0
    b = 1
    i = 0
    while i <= n:
        yield a
        a, b = b, a + b
        i +=1

print(fibonacci_generator(11))
start2 = time.time()
cProfile.run('[i for i in enumerate(fibonacci_generator(50000))]')
for i in enumerate(fibonacci_generator(11)):
    print(i)
stop2 = time.time() - start2

print("Timp de executie, varianta generator",f'pentru n = 60000', stop2)



# 1 1 1 1 1 1 1 1 1 1 1 1 1

#                    1 1 1