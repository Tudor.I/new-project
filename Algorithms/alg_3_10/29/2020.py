from Algorithms.timer import time_execution


@time_execution
def naive_minimum(l, count):
    # count = 0
    if len(l) == 0:
        return None
    minimum = float("inf")

    for elem in l:
        count = count + 1
        if elem < minimum:
            minimum = elem
    return count

if __name__ == "__main__":
    for elements in [100_000, 1_000_000, 10_000_000]:
        count = 0
        print(f"For {elements} elements: ", end="")
        # naive_minimum(range(elements), count)
        count = naive_minimum(range(elements), count)
        print(count)

# numarare de operatiuni, timp de lucru pentru fiecare element din lista