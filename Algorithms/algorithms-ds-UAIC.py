
#exercitiul nr 1
def result(n):
    if n < 1 or n > 1000: return 0
    i = 1
    a = []
    while i <= n:
        if i == 8: a.append(i)
        sum = 1
        q = i
        while q != 0:
            rest = q % 10
            q = q // 10
            sum = sum * rest
        if sum == 8:
            a.append(i)
        i+=1

# analiza complexitatii
"""
Time complexity in wrose case: O(nr cifre * n)
Finitudine: Daca i <= n -> break, si daca q == 0 -> break
Corectitudine: pe cateva exemple, mi-a produs rezultatul dorit, rulat in limbaj Python.
Viitoare solutie de implementat mai optima
"""

# exercitiul nr 2
def check_if_10_length(n):
    if n < 1000000000 or n > 9999999999: return False
    q = n
    i = 0
    while i < 10:
        c = q % 10
        u = q // 10
        while u != 0:
            rest = u % 10
            if rest == c: return False
            u = u // 10
        q = q // 10
        i+=1
    return True

def check_for_next_number(n):
    if check_if_10_length(n):
        new_number = n+1
        while new_number < 9999999999:
            if check_if_10_length(new_number):
                return new_number
            new_number+=1
    else:
        return False

#analiza complexitatii
"""
Pe cazul de fata: T(n) = nr_de_cifre(10) * nr_de_cifre(9) * nr_de_pasi_ca_sa_gaseasca_primul_succesor
Daca generalizam, toate la N, fa fi aproximativ O(n^3) ceea ce nu este ok. Insa pana acum, asta este brute force ul la care m-am gandit.
Corectitudine: pe cateva cazuri izolate, mi-a furnizat rezultatul dorit, in python
Finitudine: 3 conditii de oprire.
"""

