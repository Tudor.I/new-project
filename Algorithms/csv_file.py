import csv
import math
import random
import datetime
import time
from time import struct_time
import functools
import re
class Timp:

    def __init__(self, hours, minutes, seconds):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __repr__(self):
        return f'{self.hours} ore, {self.minutes} minute, {self.seconds} secunde'

    def calcul_secunde(self):
        total = self.hours * 3600 + self.minutes * 60 + self.seconds
        return total


with open("Music_library.csv", encoding="utf-8") as f:
    reader1 = csv.reader(f)
    lista_library = [row for row in reader1]

def is_num(string):
    """Returneaza True daca string-ul este int (nu ia in considerare cazurile cand
                sintrgul poate fi un int/float negativ)"""
    if string.isdigit():
        return True
    else:
        return False
def most_times_played(l):
    """"Returneaza o lista cu melodiile ascultate de cele mai multe ori"""

    lista_times_played_maxim = [] * len(l)
    i = 0
    while i < len(l):
        if is_num(l[i][6]):
            lista_times_played_maxim.append(int(l[i][6]))
            i+=1
        else:
            i+=1
            continue
    i = 1
    maxim_played = max(lista_times_played_maxim)
    lista_melodii = [] * len(l)
    while i < len(l):
        if l[i][6] == '':
            i += 1
            continue
        if int(l[i][6]) == maxim_played:
            lista_melodii.append(l[i])
        i += 1
    return lista_melodii


def scoatere_melodii_fara_times_played(l):
    size = len(l)

    lista_melodii = [] * size
    lst = []

    for i in range(size):
        ls = l[i][-1]
        if not is_num(l[i][-1]):
            lista_melodii.append(l.pop(i))
            # lst = lst.append(l[i])
    return lista_melodii


def merge_sort1(l):
    """"Returneaza o lista cu elementele ordonate descrescator"""

    if len(l) < 2:
        return l
    mijloc = len(l) // 2
    stanga = merge_sort1(l[:mijloc])
    dreapta = merge_sort1(l[mijloc:])
    return merge(stanga, dreapta)


def merge(lst_stg, lst_dr):
    stg = 0
    dr = 0
    lst_sortata = []
    while stg < len(lst_stg) and dr < len(lst_dr):
        if int(lst_stg[stg][6]) > int(lst_dr[dr][6]):
            lst_sortata.append(lst_stg[stg])
            stg += 1
        else:
            lst_sortata.append(lst_dr[dr])
            dr += 1
    lst_sortata.extend(lst_stg[stg:])
    lst_sortata.extend(lst_dr[dr:])
    return lst_sortata


def introducere_elemente(lista, lista_elem_de_introdus):
    """Functia introduce elementele scoase print functia scoatere_elemente_fara_times_played
                                din lista de cantece"""

    for element in lista_elem_de_introdus:
        if element[6] == 'Times Played':
            lista.insert(element, 0)
        else:
            element[6] = None
            lista.append(element)
    return lista


def likes(lista):
    """Functia returneaza o lista cu elementele care au cel putin un like"""

    melodii_cu_cel_putin_un_like = []
    i = 0
    while i < len(lista):
        if is_num(lista[i][5]) and int(lista[i][5]) > 0:
            melodii_cu_cel_putin_un_like.append(lista[i])
            i += 1
            continue
        else:
            i += 1
            continue
    return melodii_cu_cel_putin_un_like


def total_ore_auditie(lst):
    """Returneaza totalul de ore de auditie al playlist-ului"""

    lista_timp_melodie = []
    i = 0
    while i < len(lst):
        if lst[i][1].replace(':','',2).isdigit():
            lista_timp_melodie.append(lst[i][1])
        i += 1
    total = 0
    for line in lista_timp_melodie:
        m, s = map(lambda x: int(x) if is_num(x) else 0, line.split(":"))
        total += 60 * m + s
    days = total // 86400
    days_in_hours = days * 24
    hours = (total - days * 86400) // 3600
    minutes = (total - days * 86400 - hours * 3600) // 60
    seconds = total - days * 86400 - hours * 3600 - minutes * 60
    timp = Timp(hours=days_in_hours + hours, minutes=minutes, seconds=seconds)
    return timp

def genul_de_muzica_cel_mai_ascultat(lst):
    """Returneaza cel mai ascultat gen, care a fost redat cel putin o data"""

    lista_gen = []
    i = 0
    while i < len(lst):
        if is_num(lst[i][6]) and int(lst[i][6]) > 0:
            lista_gen.append(lst[i][4])
            i += 1
            continue
        else:
            i +=1
    lista_temp = sorted(set(lista_gen))
    genul = 0
    count = 0
    for gen in lista_temp:
        if count < lista_gen.count(gen):
            count = lista_gen.count(gen)
            genul = gen

    return f'Cel mai iubit gen: {genul}, ascultat de cel putin {count} ori'

def genul_de_muzica_cel_mai_ascultat2(lst):
    """Returneaza cel mai ascultat gen in functie de numarul total de redari din playlist per melodie aferenta genului"""

    lista_gen = []
    i = 0
    count1 = 0
    while i < len(lst):
        count1 +=1
        if is_num(lst[i][6]) and int(lst[i][6]) > 0:
            n = 0
            while n < int(lst[i][6]):
                count1 +=1
                lista_gen.append(lst[i][4])
                n +=1
        i += 1
    lista_temp = sorted(set(lista_gen))
    genul = 0
    count = 0
    for gen in lista_temp:
        if count < lista_gen.count(gen):
            count = lista_gen.count(gen)
            genul = gen
            count1 += gen
    return f'Cel mai iubit gen: {genul}, ascultat de cel putin {count} ori'

def genul_de_muzica_cel_mai_ascultat3(lst):
    """Returneaza cel mai ascultat gen in functie de numarul total de redari din playlist per melodie aferenta genului"""

    lista_gen = []
    i = 0
    count1 = 0
    string= 0
    # lista_gen = [lista[4] for lista in lst if is_num(lista[4]) and int(lista[6]) > 0]

    while i < len(lst):
        count1 += 1
        if is_num(lst[i][6]) and int(lst[i][6]) > 0:
            string = ((lst[i][4] + ',') * int(lst[i][6])).split(',', int(lst[i][6]) - 1)
            string_temp = string[-1].replace(',', '',1)
            string[-1] = string_temp
            lista_gen.extend(string)
        i += 1
    lista_temp = sorted(set(lista_gen))
    genul = 0
    count = 0
    for gen in lista_temp:
        count1 +=1
        if count < lista_gen.count(gen):
            count = lista_gen.count(gen)
            genul = gen

    return f'Cel mai iubit gen: {genul}, ascultat de cel putin {count} ori'
def procent_ascultat(lst):
    """Functia returneaza procentul din timpul total al playlist-ului ascultat, respectiv cel neascultat"""
    total_ore = total_ore_auditie(lst)
    timp_total = total_ore.calcul_secunde()
    melodii_auditate = []
    i = 1
    while i < len(lst):
        if is_num(lst[i][6]) and int(lst[i][6]) >= 1:
            melodii_auditate.append(lst[i][1])
        i+=1
    total = 0
    for line in melodii_auditate:
        m, s = map(lambda x: int(x) if is_num(x) else 0, line.split(":"))
        total += 60 * m + s
    procent_ascultat = (total / timp_total) * 100
    procent_neascultat = 100 - procent_ascultat
    return f'Din totalul de {total_ore}, adica {timp_total} secunde, a fost ascultat un procent de: {round(procent_ascultat, 2)}%\nProcentul nescultat este in valoare de {round(procent_neascultat, 2)}%" '
if __name__ == "__main__":
    print(len(lista_library))
    y = scoatere_melodii_fara_times_played(lista_library)
    print(len(lista_library))
    # print(genul_de_muzica_cel_mai_ascultat3(lista_library))
    # # print(genul_de_muzica_cel_mai_ascultat2(lista_library))
    # #
    # # print(procent_ascultat(lista_library))
    # y = scoatere_melodii_fara_times_played(lista_library)
    # print(y)
    # print(lista_library)
    # n = merge_sort1(lista_library)
    # lista_finala = introducere_elemente(lista=n, lista_elem_de_introdus=y)
    # print(n)
    # # lista_like = likes(lista_finala)
    # # m = total_ore_auditie(lista_library)
    # # print(m)
    # # print(str(sum))
    # x = 'vocal'
    # y =  (x + ',') * 6
    # print(y)
    # y = y.split(',')
    # print(y)



























    # def merge_sort(lista_de_liste):
    #     size = len(lista_de_liste)
    #     if size > 1:
    #         middle = size // 2
    #         stg_liste = lista_de_liste[:middle]
    #         dr_liste = lista_de_liste[middle:]
    #
    #         merge_sort(stg_liste)
    #         merge_sort(dr_liste)
    #
    #         stg = 0
    #         dr = 0
    #         idx_lst = 0
    #         left_size = len(stg_liste)
    #         right_size = len(dr_liste)
    #         while stg < left_size and dr < right_size:
    #             if int(stg_liste[stg][6]) > int(dr_liste[dr][6]):
    #                 lista_de_liste[idx_lst] = stg_liste[stg]
    #                 stg += 1
    #
    #             else:
    #                 lista_de_liste[idx_lst] = dr_liste[dr]
    #                 dr += 1
    #             idx_lst += 1
    #
    #         while stg < left_size:
    #             lista_de_liste[idx_lst] = stg_liste[stg]
    #             stg += 1
    #             idx_lst += 1
    #         while dr < right_size:
    #             lista_de_liste[idx_lst] = dr_liste[dr]
    #             dr += 1
    #             stg += 1
    #     return lista_de_liste

