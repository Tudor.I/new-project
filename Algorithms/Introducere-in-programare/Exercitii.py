def furnica(n):
    """recursivitate"""
    if n == 0: return 0
    if n == 1: return 1
    return (n - 2) + (n - 1)


def furnica_iterativ(n):
    """iterativ"""
    a = 0
    b = 1
    for i in range(n):
        tmp = b
        b = a + b
        a = tmp
    return b


def down(row, col, matrix, i):
    while 0 <= row < len(matrix) and 0 <= col < len(matrix):
        matrix[row][col] = furnica_iterativ(i)
        i += 1
        col -= 1
        row += 1
    if col == -1:
        col = 0
    elif col == len(matrix):
        col = len(matrix) - 1
    if row == -1:
        row = 0
    elif row == len(matrix):
        row = len(matrix) - 1
    if row == len(matrix)-1 and col == 0 and matrix[row][col] != 0:
        col+=1
    return (row, col, matrix, i)


def up(row, col, matrix, i):
    while 0 <= row < len(matrix) and 0 <= col < len(matrix):
        matrix[row][col] = furnica_iterativ(i)
        i += 1
        row -= 1
        col += 1
    if col == -1:
        col = 0
    elif col == len(matrix):
        col = len(matrix) - 1
    if row == -1:
        row = 0
    elif row == len(matrix):
        row = len(matrix - 1)
    if col == len(matrix)-1 and row == 0 and matrix[row][col] != 0:
        row+=1
    return (row, col, matrix, i)


def traversarea_tablou_bidimensional(matrix):
    step_to_right = True
    step_down = True
    col, row = 0, 0
    i = 0
    matrix[row][col] = furnica_iterativ(i)
    i+=1
    col+=1
    while i < len(matrix) * len(matrix[0]):
        if step_to_right:
            row, col, matrix, i = down(row, col, matrix, i)
            step_to_right = False
            step_down = True
        elif step_down:
            row, col, matrix, i = up(row, col, matrix, i)
            step_down = False
            step_to_right = True
    return matrix


if __name__ == "__main__":
    matrice = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

    print(traversarea_tablou_bidimensional(matrice))
