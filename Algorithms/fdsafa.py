
import random

def MergeSort(lst):
    if len(lst) > 1:
        middle = len(lst) // 2
        l = lst[:middle]
        r = lst[middle:]

        MergeSort(l)
        MergeSort(r)

        i, j, k = 0, 0, 0

        while i < len(l) and j < len(r):
            if l[i] > r[j]:
                lst[k] = l[i]
                i += 1
            else:
                lst[k] = r[j]
                j += 1
            k += 1

        while i < len(l):
            lst[k] = l[i]
            i += 1
            k += 1

        while j < len(r):
            lst[k] = r[j]
            j += 1
            k += 1

    return lst

lista = []
for i in range(3000):
    lista.append(random.randint(0,3000))

print(MergeSort(lista))


def most_times_played(l):
    """"Returneaza o lista cu melodiile ascultate de cele mai multe ori"""
    lista_times_played_maxim = [] * len(l)
    i = 1
    # for lista in l:
    while i < len(l):
        if l[i][6] == '':
            i += 1
            continue
        else:
            lista_times_played_maxim.append(int(l[i][6]))
        i += 1
    i = 1
    maxim_played = max(lista_times_played_maxim)
    lista_melodii = [] * len(l)
    while i < len(l):
        if l[i][6] == '':
            i += 1
            continue
        if int(l[i][6]) == maxim_played:
            lista_melodii.append(l[i])
        i += 1

    return lista_melodii


def scoatere_melodii_fara_times_played(l):
    size = len(l)
    i = 0
    lista_melodii = [] * size
    while i < size:
        if l[i][6] == 'Times played' or l[i][6] == '':
            lista_melodii.append(l.pop(i))
            size -= 1
            i -= 1
        i += 1
    return lista_melodii


def merge_sort1(l):
    """"Returneaza o lista cu elementele ordonate descrescator"""
    if len(l) < 2:
        return l
    mijloc = len(l) // 2
    stanga = merge_sort1(l[:mijloc])
    dreapta = merge_sort1(l[mijloc:])
    return merge(stanga, dreapta)


def merge(lst_stg, lst_dr):
    stg = 0
    dr = 0
    lst_sortata = []
    while stg < len(lst_stg) and dr < len(lst_dr):
        if int(lst_stg[stg][6]) > int(lst_dr[dr][6]):
            lst_sortata.append(lst_stg[stg])
            stg += 1
        else:
            lst_sortata.append(lst_dr[dr])
            dr += 1
    lst_sortata.extend(lst_stg[stg:])
    lst_sortata.extend(lst_dr[dr:])
    return lst_sortata


def introducere_elemente(lista, lista_elem_de_introdus):
    """Functia introduce elementele scoase print functia scoatere_elemente_fara_times_played
                                din lista de cantece"""
    for element in lista_elem_de_introdus:
        if element[6] == 'Times Played':
            lista.insert(element, 0)
        else:
            element[6] = None
            lista.append(element)
    return lista


def likes(lista):
    """Functia returneaza o lista cu elementele care au cel putin un like
            functia porneste de la elementul aflat la index-ul 1"""

    melodii_cu_cel_putin_un_like = []
    i = 1
    while i < len(lista):
        if lista[i][5] == '' or lista[i][5] is None or lista[i][5] == 'Liked':
            i += 1
            continue
        elif int(lista[i][5]) > 0:
            melodii_cu_cel_putin_un_like.append(lista[i])
        i += 1
    return melodii_cu_cel_putin_un_like


def total_ore_auditie(lst):
    """Returneaza totalul de ore de auditie al playlist-ului"""
    lista_timp_melodie = []
    i = 1
    while i < len(lst):
        lista_timp_melodie.append(lst[i][1])
        i += 1
    total = 0
    for line in lista_timp_melodie:
        m, s = map(int, line.split(":"))
        total += 60 * m + s
    days = total // 86400
    hours = (total - days * 86400) // 3600
    minutes = (total - days * 86400 - hours * 3600) // 60
    seconds = total - days * 86400 - hours * 3600 - minutes * 60
    timp = Timp(days=days, hours=hours, minutes=minutes, seconds=seconds)
    return f'Albumul dureaza in total: {timp.__repr__()}'