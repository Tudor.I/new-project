// alert('Hello world') da un alert 
// document.write("Salut") scrie salut undeva pe pagina
// console.log("Salut") spune salut in consola dupa inspect element
var a = 5;
var b = 5;

var myName = "Tudor"
var hello = "Hello "
document.write(hello + " " + myName)

// definirea unei functii in javascript

        document.write("<br>");
// De retinut. O functie poate fi apelata/ importanta din alt fisier
// daca o scriem intr un fisier javascript si o aducem in html
// inaintea fisierului in care vrem sa apelam


for (i=1;i<=10;i++){
    document.write(i + " ");
}
document.write("<br>");

var i = 1
while(i<=10){
    document.write(i + " ");
    i++;
}

document.write("<br>");

var i = 1;
do {
    document.write(i + " ")
    i++;
    
} while(i<=10);

var x = [1, 'y', 4]; 
var y = [23, 'x', 1]; 
var z = ['g', 'x', 3]; 
// acolo se defineste array ul
var matrice = [x,y,z]

document.write("<br>")
document.write(matrice[1][0])

x.push(43);
y.push(8);
// aceasta instructiune
document.write("<br>")
document.write(x);
document.write("<br>")
document.write(y);
document.write("<br>")
var l = x.pop()
// elimina ultimul element
document.write(x)
document.write("<br>")
document.write(l)

document.write("<br>")

x.shift()
// elimina primul element
document.write(x)

var elemente = [0,1]
for(i=2; i<10; i++)
{
    var new_element = elemente[i-2] + elemente[i-1];
    elemente.push(new_element);
}
document.write("<br>")
document.write("<br>")
document.write("The 10th element is:" +" " + elemente[9])
document.write("<br>")

a = 10
b = 10
document.write(comparare(a,b))
// aici este apelata functia comparare din functii.js
document.write("<br>")
document.write(suma(a,b))
// cuvant cheie document pt a interactiona cu documentul html


// aici vom prelua din html, din formular
// elementele dupa id prin functia getelementbyid
// si facem apoi ce vrem noi cu ele
document.getElementById("calcul").onclick = function(){
    var el = parseInt(document.getElementById("number1").value);
    var el2 = parseInt(document.getElementById("number2").value);
    var total = suma(el, el2);
    document.getElementById("rezultat").value = total;
}
// var el = document.getElementById("numar1");
// var el2 = document.getElementById("numar2");
// var sum = suma(el, el2)

