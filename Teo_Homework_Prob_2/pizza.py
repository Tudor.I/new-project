class Pizza:
    def __init__(self, nume):
        """Initializam parametrii clasei Pizza"""
        self.meniu = {
            "Peperoni": 6.5,
            "Capriciosa": 3.8,
            "Quatro formaggi": 5,
            "Quatro carni": 4.8
        }

        assert nume.capitalize() in self.meniu.keys(), f'{nume} nu se afla in meniu!'
        # for key in enumerate(list(key_menu)):
        #     print(key)

        self.nume = nume
        self.pret_dolari = self.meniu[nume.capitalize()]

    def get_nume(self):
        """Returneaza numele de pizza comandata"""

        return f'Ati comandat o pizza {self.nume}'

    def get_pret(self):
        """Returneaza pretul aferent la pizza comandata"""

        return f'Pizza aleasa costa {self.pret_dolari} dolari'


class Portofel:
    def __init__(self, suma):
        """Initializam parametrii clasei Portofel"""

        assert isinstance(suma, float), "Ati introdus o suma invalida"
        self.suma = suma

    def get_suma(self):
        """Returneaza suma disponibila in portofel, in lei"""
        return f'Aveti la dispozitie {self.suma} lei'


class Convertor(Pizza, Portofel):
    def __init__(self, nume, sumalei):
        """Initializeaza parametrii aferenti claselor Pizza, Portofel"""
        Pizza.__init__(self, nume)
        Portofel.__init__(self, suma=sumalei)
        # self.suma = sumalei
        self.schimb_valutar = 0.21

    def get_suma(self):
        """Returneaza numarul de pizza ce pot fi comandate cu suma aflata in portofel, calculata
                                            in dolari"""

        pret_dolari = self.suma * self.schimb_valutar
        pizza_cumparate = pret_dolari // self.pret_dolari
        return f'Aveti la dispozitie {pret_dolari} dolari.' \
               f'Puteti cumpara {pizza_cumparate} pizza de tipul {self.nume}'

def input_de_la_tastatura():
    while True:
        x = input("Apasati enter pentru a continua sau scrieti 'q' pentru a iesi din aplicatie. ")
        if x == 'q':
            break
        else:
            try:
                nume = input("Introduceti numele de pizza: ") # Se cere input de la tastatura pentru pizza, si se verifica prin try daca e in menu sau nu
                lei = float(input("Introduceti suma in lei: ")) #se verifica printr-un try separat ca suma sa fie float. Daca se introduce un input gresit
                #de tip float, se duce in except, si continue il va trimite la inceputul functiei
                converter = Convertor(nume=nume,
                                      sumalei=lei)  # daca au fost introduse bine ambele nume, se creeaza instanta de tip converter
                print(converter.get_suma())
            except ValueError:
                print("Introduceti un numar valid!\nVa rugam incercati din nou")
                continue
            except AssertionError:
                print("Ati introdus un nume de pizza ce nu se afla in menu!\nVa rugam incercati din nou!")
                continue


def input_de_la_tastatura1():
    while True:
        x = input(
            "Apasati enter pentru a continua sau scrieti 'q' pentru a iesi din aplicatie. "
        )
        if x == "q":
            break
        else:
            try:
                nume = input("Introduceti numele de pizza: ")
                lei = float(input("Introduceti suma in lei: "))
                converter = Convertor(nume=nume, sumalei=lei)
                print(converter.get_suma())
            except ValueError:
                print("Introduceti un numar valid!")
                continue
            except AssertionError:
                print("Pizza nu este in meniu!")
                continue
def client(Portofel):
    """Returneaza suma detinuta de client"""
    return Portofel.get_suma()


print(input_de_la_tastatura())
# pizza = Pizza("Peperoni")
# portofel = Portofel(100.0)
# conversie = Convertor(nume=pizza.nume, sumalei=portofel.suma)
# #
# print(client(portofel))
# print(conversie.get_suma())
#

# dictionar = {
#             "Peperoni": 6.5,
#             "Capriciosa": 3.8,
#             "Quatro formaggi": 5,
#             "Quatro carni": 4.8
#         }

# for keys in dictionar.keys():
#     print(keys)

# for values in dictionar.values():
#     print(values)

# for keys, values in dictionar.items():
#     print(keys, values)

# for i in dictionar:
#     print(i)
# print(dictionar.items())