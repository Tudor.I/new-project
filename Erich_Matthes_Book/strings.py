
# import this

if __name__ == "__main__":
    name = "python "
    print(len(name))
    print(name.title())
    name = name.rstrip() # here the whitespace has been removed permanently
    print(len(name)) # rstrip -> removes the whitespace
    # print(this)