
filename = ['alice.txt', 'mu.txt', 'ga.txt', 'moby_dick.txt', 'siddhartha.txt']
def nr_words(file):
    try:
        with open(file, encoding='utf-8') as f:
            content = f.read()
    except FileNotFoundError:
        print("There is no such file found in your directory!")
    else:
        nr_words = content.split()
        num = len(nr_words)
        print(f'The file {file} has {num} words!')

for file in filename:
    nr_words(file)

