import json
number_file = 'fav_number.json'
def stored_fav_number(file):
    try:
        with(open(file)) as f:
            nr = json.load(f)
        return nr
    except FileNotFoundError:
        return None
def favourite_number():
    number = input("Va rog sa va introduceti numarul d-voastra")
    with open('fav_number.json', 'w') as f:
        json.dump(number, f)
        print("Ati introdus cu succes numarul d-voastra")

def your_number(file):
    number = stored_fav_number(file)
    if number:
        x = input(f'{number} este nr d-voastra?')
        if x == 'yes':
            print(f'your fav nr is {number}')
        if x == 'no':
            number = favourite_number()
        try:
            if x not in ['yes', 'no']:
                raise Exception
        except Exception:
            while x not in ['yes', 'no']:
                x = input("Va rugam sa introduceti doar yes/no")


# your_number(number_file)

import random

i=0
rcorect=0
print("\t")
print("Completeaza raspunsul pentru fiecare inmultire. Pentru fiecare raspuns corect, primesti 1 punct!")
print("\t")
while i<10:
    i+=1
    x = random.randrange(1, 10)
    y = random.randrange(1, 10)
    r = int(input(f"{i}.  {x} x {y} = "))
    if x * y == r:
        rcorect+=1
        print("\t" ,"Corect!")

    else:
        print(f"Ai gresit, raspunsul corect este {x} x {y} = {x*y}")
if rcorect==10:
    print("Felicitari ai raspuns corect la toate exercitiile !")
else:
    print(f"Ai raspuns corect la {rcorect} din 10.")