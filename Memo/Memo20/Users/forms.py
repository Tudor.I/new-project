from .models import *
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class SignUpForm(UserCreationForm):
    profile_img = forms.ImageField(required=False, help_text="Optional")
    quotes = forms.CharField(max_length=500, widget=forms.Textarea, required=False, help_text="Optional")

    class Meta:
        model = CustomUser
        fields = ['username', 'password1', 'password2', 'quotes', 'profile_img']


class ProfileForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = '__all__'


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = "__all__"
