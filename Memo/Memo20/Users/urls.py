from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'),
         name='password_reset'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html')),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path('redirect/logout', views.Logout.as_view(), name="logout_page"),
    path("register/", views.ProfileCreate.as_view(), name="register"),
    path('profile/<int:id>/', views.ProfileView.as_view(), name="profile-view"),
    path('profile/update/<int:pk>/', views.profile_update, name="profile-update"),
    path('profile/delete/<int:pk>/', views.ProfileDelete.as_view(), name="profile-delete")
]
