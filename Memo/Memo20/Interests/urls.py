from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.Home.as_view(), name="home"),
    path('interests/create', views.CreateLink.as_view(), name="link-create"),
    path('interests/delete/<int:pk>/', views.DeleteLink.as_view(), name="link-delete"),
    path('interests/update/<int:pk>/', views.UpdateLink.as_view(), name="link-update"),
    path('interests/view/description/<int:pk>/', views.DescriptionView.as_view(), name="description-view"),
    path('interests/search', views.search_engine, name='search-engine'),
    path('interests/search/results', views.search_302, name="search-results")
]
