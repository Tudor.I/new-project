from django.db import models
from django.apps import apps
import datetime
# Create your models here.

from Topics.models import Topic



class Interests(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    url = models.URLField()
    description = models.TextField(max_length=5000, blank=True, null=True)
    creation_date = models.DateTimeField(default=datetime.datetime.now(), blank=True, null=True)
    update_time = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return "Interests"
