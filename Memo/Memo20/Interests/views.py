from django.shortcuts import render
import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *
from .services import send_email


# Create your views here.

class Home(TemplateView):
    template_name = "interestss/home.html"

    def get(self, request, *args, **kwargs):
        page = request.GET.get('page')
        links = Interests.objects.filter()
        paginator = Paginator(links, 10)
        try:
            links = paginator.page(page)
        except PageNotAnInteger:
            links = paginator.page(1)
        except EmptyPage:
            links = paginator.page(paginator.num_pages)
        return render(request, 'interestss/home.html', {'objects_list': links, 'len': len(links)})


class CreateLink(CreateView):
    model = Interests
    context_object_name = "form"
    fields = ['title', 'url', 'description']
    template_name = "interestss/link_create.html"
    success_url = reverse_lazy("home")

    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        form = self.get_form()
        if form.is_valid():
            Interests.objects.create(
                title=form['title'].data,
                url=form['url'].data,
                description=form['description'].data,
                creation_date=datetime.datetime.now(),
            )
            return redirect("home")
        return redirect('link-update', pk=pk)


class DeleteLink(DeleteView):
    model = Interests
    context_object_name = "link"
    template_name = "interestss/link_delete.html"
    success_url = reverse_lazy("home")


class UpdateLink(UpdateView):
    model = Interests
    context_object_name = "link"
    template_name = "interestss/link_update.html"
    success_url = reverse_lazy("home")
    fields = ['title', 'url', 'description']

    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        link = Interests.objects.get(pk=pk)
        form = self.get_form()
        if form.is_valid():
            link.title = form['title'].data
            link.url = form['url'].data
            link.description = form['description'].data
            link.update_time = datetime.datetime.now()
            link.save()
            return redirect("home")
        return redirect('link-update', pk=pk)


class DescriptionView(TemplateView):
    model = Interests
    template_name = "interests/description_view.html"

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        link = Interests.objects.get(pk=pk)
        return render(request, 'interestss/description_view.html', {'description': link.description})


def load_browser(request, srch, load=False):
    words = srch.split()
    links = []
    for word in words:
        links.extend(Interests.objects.filter(Q(title__icontains=word)))
    links = set(links)
    links = list(links)
    page = request.GET.get('page')
    paginator = Paginator(links, 10)
    try:
        links = paginator.page(page)
    except PageNotAnInteger:
        links = paginator.page(1)
    except EmptyPage:
        links = paginator.page(paginator.num_pages)
    if load:
        return render(request, 'interestss/search_view.html', {'objects_list': links, 'len': len(links)})
    else:
        request.session['browser_load'] = srch
        del request.session['src']
        del request.session['search']
        return render(request, 'interestss/search_view.html', {'objects_list': links, 'len': len(links)})


def search_engine(request, *args, **kwargs):
    if request.method == 'POST':
        request.session['search'] = request.POST.get('q')
        request.session['src'] = True
        return redirect('search-results')


def search_302(request, *args, **kwargs):
    if request.method == "GET":
        if request.session.get('src') is not None:
            srch = request.session['search']
            return load_browser(request, srch)
        else:
            srch = request.session.get('browser_load')
            load = True
            return load_browser(request, srch, load)
