from django.shortcuts import render
import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *

# Create your views here.

class TopicsView(TemplateView):
    template_name = "topicss/top.html"
