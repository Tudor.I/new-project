from django.db import models


# Create your models here

class Topic(models.Model):
    name = models.CharField(max_length=70, blank = True, null = True)
    brief_description = models.TextField(max_length=500, blank = True, null = True)
