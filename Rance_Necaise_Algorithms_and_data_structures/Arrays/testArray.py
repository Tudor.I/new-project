from Rance_Necaise_Algorithms_and_data_structures.Arrays.ArrayADT import Array
import ctypes
import unittest

lista = [0]

class TestCase(unittest.TestCase):
    def setUp(self):
        self.vect = Array(1,2,3,4,5,6)


    def test_remove(self):
        self.vect.remove(2)
        self.vect.remove(0)
        self.vect.remove(0)
        self.vect.remove(0)
        self.assertEqual(len(self.vect), 3)
        self.assertEqual(self.vect[0], 5)

    def test_index_of(self):
        self.assertEqual(self.vect.indxOf(4), 3)

    def test_extend_other_Array(self):
        otherArray = Array(1,2,3)
        self.vect.extend(otherArray)
        self.assertEqual(self.vect[5], 6)
        self.assertEqual(self.vect[6], 1)
        self.assertEqual(self.vect[7], 2)
        self.assertEqual(self.vect[8], 3)
        self.assertEqual(len(self.vect), 15)

    def test_subVector(self):
        sub_vector = self.vect.subVector(1,3)
        self.assertEqual(sub_vector.length(), 2)
        self.assertEqual(sub_vector[0], 2)
        self.assertEqual(len(sub_vector), 3)


    def test_add(self):
        self.vect.add(4)
        self.vect.add(6)
        self.vect.add(10)
        self.vect.add(11)
        self.assertEqual(len(self.vect), 15)
        self.assertEqual(self.vect[5], 6)
        self.assertEqual(self.vect[6], 4)
        self.assertEqual(self.vect[7], 6)
        self.assertEqual(self.vect[8], 10)
        self.assertEqual(self.vect[9], 11)
        self.assertEqual(self.vect.length(), 10)
        self.vect.remove(3)
        self.assertEqual(self.vect[3], 5)
        self.vect.remove(5)
        self.vect.remove(2)
        self.vect.remove(3)
        self.vect.remove(2)
        self.assertEqual(self.vect[0], 1)
        self.assertEqual(self.vect[4], 11)
        self.assertEqual(len(self.vect), 7)
        self.assertEqual(self.vect.length(), 5)
        self.vect.__str__()

    def test_remove_len_1(self):
        self.vect.remove(2)
        self.vect.remove(2)
        self.vect.remove(1)
        self.assertEqual(len(self.vect), 7)
        self.vect.remove(0)
        self.vect.remove(0)
        self.assertEqual(len(self.vect), 3)
        self.vect.add(1)
        self.assertEqual(self.vect[0], 6)
        self.assertEqual(self.vect[1], 1)
        self.assertEqual(len(self.vect), 3)
        self.vect.add(2)
        self.vect.add(3)
        self.assertEqual(self.vect[0], 6)
        self.assertEqual(self.vect[1], 1)
        self.assertEqual(self.vect[2], 2)
        self.assertEqual(len(self.vect), 7)
        self.vect.add(2)
        self.vect.add(3)
        self.assertEqual(len(self.vect), 7)
        self.vect.add(2)
        self.vect.add(3)
        self.vect.add(2)
        self.vect.add(3)
        self.assertEqual(len(self.vect), 15)
        self.vect.remove(0)
        self.vect.remove(0)
        self.vect.remove(0)
        self.vect.remove(0)
        self.vect.remove(0)
        self.assertEqual(len(self.vect), 7)
        self.assertEqual(self.vect.length(), 5)



    def test_nonte(self):
        self.vect[2] = None
        self.vect.add(1)
        self.assertEqual(self.vect.length(), 7)

    def test_insert(self):
        self.vect.insert(3, 10)
        self.assertEqual(self.vect[3], 10)
        self.assertEqual(self.vect[4], 4)
        self.assertEqual(self.vect[5], 5)
        self.assertEqual(self.vect[6], 6)
        self.vect.insert(3, 10)
        self.vect.insert(3, 10)
        self.vect.insert(3, 10)
        self.assertEqual(self.vect[6], 10)
        self.assertEqual(self.vect[7], 4)
        self.assertEqual(self.vect[8], 5)
        self.assertEqual(self.vect[9], 6)

