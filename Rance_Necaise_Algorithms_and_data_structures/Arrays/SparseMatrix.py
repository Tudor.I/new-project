class SparseMatrix:
    def __init__(self, numRows, numCols):
        self._numRows = numRows
        self._numCols = numCols
        self._elementList = list()

    def numRows(self):
        return self._numRows

    def numCols(self):
        return self._numCols

    def __setitem__(self, ndxTuple, scalar):
        ndx = self._findPosition(ndxTuple[0], ndxTuple[1])
        if ndx is not None:
            if scalar != 0.0:
                self._elementList[ndx].value = scalar
            else:
                self._elementList.pop(ndx)
        else:
            if scalar != 0.0:
                element = _MatrixElement(ndxTuple[0], ndxTuple[1], scalar)
                self._elementList.append(element)

    def __getitem__(self, row, col):
        assert 0 <= row < self.numRows(), "Subscript out of range"
        assert 0 <= col < self.numCols(), "Subscript out of range"
        ndx = self._findPosition(row, col)
        if ndx is not None:
            return self._elementList[ndx]
        else:
            print("The element is 0")

    def scaleBy(self, scalar):
        for element in self._elementList:
            element.value *= scalar

    def _findPosition(self, row, col):
        """Find the position of the non zero element in the list, using the row and col as parameters"""
        n = len(self._elementList)
        for i in range(n):
            if (row == self._elementList[i].row and
                    col == self._elementList[i].col):
                return i
        return None

    def add(self, element):
        assert isinstance(element, _MatrixElement) and \
               (element not in self._elementList), "Wrong input"
        self._elementList.append(element)

    def __add__(self, otherMatrix):
        assert self.numCols() == otherMatrix.numCols() and (
                self.numRows() == otherMatrix.numRows()
        ), "Wrong matrix size"
        newSparseMatrix = SparseMatrix(self.numRows(), self.numCols())
        for ele in self._elementList:
            newSparseMatrix._elementList.append(ele)

        for element in otherMatrix._elementList:
            value = newSparseMatrix[element.row, element.col]
            value += element.value
            newSparseMatrix[element.row, element.col] = value
        return newSparseMatrix

    def __sub__(self, otherMatrix):
        assert self.numCols() == otherMatrix.numCols() and (
                self.numRows() == otherMatrix.numRows()
        ), "Wrong matrix size"
        newSparseMatrix = SparseMatrix(self.numRows(), self.numCols())
        for ele in self._elementList:
            newSparseMatrix._elementList.append(ele)
        for element in otherMatrix._elementList:
            value = newSparseMatrix[element.row, element.col]
            value -= element.value
            newSparseMatrix[element.row, element.col] = value
        return newSparseMatrix

    def transpose(self):
        newTransposeMatrix = SparseMatrix(numRows=self.numCols(), numCols=self.numRows())
        for elem in self._elementList:
            tmp_row = elem.row
            elem.row = elem.col
            elem.col = tmp_row
            newTransposeMatrix._elementList.append(elem)
        return newTransposeMatrix

    def __mul__(self, otherMatrix):
        assert isinstance(otherMatrix, SparseMatrix), "Wrong matrix type"
        assert self.numCols() == otherMatrix.numRows(), "The two matrices can't be multiplied"
        transpMatrix = otherMatrix.transpose()
        sparseNewMatrix = SparseMatrix(numRows=self.numRows(), numCols=otherMatrix.numRows())
        for apos in range(len(self._elementList)):
            r = self._elementList[apos].row
            for bpos in range(len(transpMatrix._elementList)):
                c = transpMatrix._elementList[bpos].row
                tmpa = apos
                tmpb = bpos
                suma = 0
                while (tmpa < len(self._elementList) and (tmpb < len(transpMatrix._elementList)) and (self._elementList[tmpa].row == r
                                                                                                      and transpMatrix._elementList[tmpb].row == c)):
                    if self._elementList[tmpa].col > transpMatrix._elementList[tmpb].col:
                        tmpa += 1
                    elif self._elementList[tmpa].col < transpMatrix._elementList[tmpb].col:
                        tmpb += 1
                    else:
                        suma += self._elementList[tmpa].value * transpMatrix._elementList[tmpb].value
                        tmpa += 1
                        tmpb += 1
                if suma != 0:
                    new_element =_MatrixElement(r, c, suma)
                    if len(sparseNewMatrix._elementList) == 0:
                        sparseNewMatrix.add(new_element)
                    else:
                        flag = True
                        for element in sparseNewMatrix._elementList:
                            if element.col == new_element.col and element.row == new_element.row:
                                element.value += new_element.value
                                flag = False
                        else:
                            if flag:
                                sparseNewMatrix.add(new_element)
        return sparseNewMatrix


class _MatrixElement:
    def __init__(self, row, col, value):
        self.row = row
        self.col = col
        self.value = value

if __name__ == "__main__":
    A = SparseMatrix(3, 3)
    A.add(_MatrixElement(1,2,10))
    A.add(_MatrixElement(1, 3, 12))
    A.add(_MatrixElement(2, 1, 1))
    A.add(_MatrixElement(2, 3, 2))
    B = SparseMatrix(3, 3)
    B.add(_MatrixElement(1, 1, 2))
    B.add(_MatrixElement(1, 2, 5))
    B.add(_MatrixElement(2, 2, 1))
    B.add(_MatrixElement(3, 1, 8))

    C = A * B
    for element in C._elementList:
        print(element.row, element.col, element.value)

