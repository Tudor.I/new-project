from Rance_Necaise_Algorithms_and_data_structures.Arrays.Array2D_ADT import Array2D
from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array
class GrayscaleImage:
    def __init__(self, nRows, nCols):
        assert nRows >=0, "Pozitive integer"
        assert nCols >=0, "Pozitive integer"
        self._pixels = Array2D(nRows, nCols)
        self._pixels.clear(0)

    def width(self):
        return self._pixels.numCols()

    def height(self):
        return self._pixels.numRows()

    def __getitem__(self, row,col):
        assert 0 <= row < self.height(), "Wrong height"
        assert 0 <= col <self.width(), "Wrong width"
        return self._pixels[row,col]

    def __setitem__(self, row, col, value):
        assert 0 <= row < self.height(), "Wrong height"
        assert 0 <= col < self.width(), "Wrong width"
        assert 0 <= value <= 255, "Intensity out of the range"
        self._pixels[row, col] = value


class RGBColor:
    def __init__(self, red = 0, green = 0, blue = 0):
        assert 0 <= (red and green and blue) <= 255, "Intensity out of range"
        self.red = red
        self.green = green
        self.blue = blue

    def __str__(self):
        return f'{self.red} {self.green} {self.blue}'

# First version of ColorImage ADT. The next one is coming
class ColorImageADT:
    def __init__(self, nrows, ncols):
        self.image = Array2D(nrows, ncols)
        self.clear(RGBColor(0,0,0))

    def width(self):
        return self.image.numCols()

    def height(self):
        return self.image.numRows()

    def clear(self, rgbcolor):
        assert isinstance(rgbcolor, RGBColor), "Wrong color"
        self.image.clear(rgbcolor)

    def __getitem__(self, row, col):
        assert 0 <= row < self.image.numRows(), "Wrong height"
        assert 0 <= col < self.image.numCols(), "Wrong width"
        return self.image[row, col]

    def __setitem__(self, row, col, value):
        assert 0 <= row < self.image.numRows(), "Wrong height"
        assert 0 <= col < self.image.numCols(), "Wrong width"
        assert isinstance(value, RGBColor), "Wrong color type"
        self.image[row, col] = value

def colorToGrayscale(colorIMG):
    assert isinstance(colorIMG, ColorImageADT), "Wrong type of the image"
    resultImage = ColorImageADT(colorIMG.height(), colorIMG.width())
    for row in colorIMG.height():
        for col in colorIMG.width():
            red = colorIMG[row, col].red
            blue = colorIMG[row,col].blue
            green = colorIMG[row, col].green
            gray = round(0.299 * red + 0.587 * green + 0.114 * blue)
            resultImage[row,col] = gray
    return resultImage








class ColorIMGADTArr:
    def __init__(self, numRows, numCols):
        assert numRows >= 0, "Integer positive"
        assert numCols >=0, "Integer positive"
        self._numRows = numRows
        self._numCols = numCols
        self._size = self._numRows*self._numCols
        self._Red = Array(self._size)
        self._Green = Array(self._size)
        self._Blue = Array(self._size)
        for i in range(self._size):
            self._Red[i] = 0
            self._Blue[i] = 0
            self._Green[i] = 0

    def computeIndex(self, row, col):
        assert 0 <= row < self._numRows, "Subscript out of range"
        assert 0 <= col < self._numCols, "Subscript out of range"
        ndx = row * self._numCols + col
        return ndx

    def __getitem__(self, row, col):
        assert 0 <= row < self._numRows, "Subscript out of range"
        assert 0 <= col < self._numCols, "Subscript out of range"
        index = self.computeIndex(row,col)
        pxl = RGBColor()
        pxl.red = self._Red[index]
        pxl.blue = self._Blue[index]
        pxl.green = self._Green[index]
        return pxl

    def __setitem__(self, row, col, value):
        assert 0 <= row < self._numRows, "Subscript out of range"
        assert 0 <= col < self._numCols, "Subscript out of range"
        assert isinstance(value, RGBColor), "Wrong value input"
        index = self.computeIndex(row, col)
        self._Red[index].red = value.red
        self._Green[index].green = value.green
        self._Blue[index].blue = value.blue




