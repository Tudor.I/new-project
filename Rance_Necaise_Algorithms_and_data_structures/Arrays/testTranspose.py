import unittest
from Rance_Necaise_Algorithms_and_data_structures.Arrays.SparseMatrix import SparseMatrix,_MatrixElement

class TestTransposeMatrix(unittest.TestCase):
    def setUp(self):
        self.matrix = SparseMatrix(numRows=2, numCols=3)

    def test_transpose(self):
        new_transp = self.matrix.transpose()
        self.matrix.add(_MatrixElement(1, 2, "a"))
        self.matrix.add(_MatrixElement(2, 2, "b"))
        self.matrix.add(_MatrixElement(0, 2, "c"))
        self.assertEqual(new_transp.numRows(), 3)
        self.assertEqual(new_transp.__getitem__(2,0).value, "c")

    def test_multiply(self):
        self.otherMatrix = SparseMatrix(numRows=3,numCols=2)
        self.matrix.add(_MatrixElement(1, 2, 10))
        self.matrix.add(_MatrixElement(1, 3, 12))
        self.matrix.add(_MatrixElement(2, 1, 1))
        self.matrix.add(_MatrixElement(2, 3, 2))
        self.otherMatrix.add(_MatrixElement(1, 1, 2))
        self.otherMatrix.add(_MatrixElement(1, 2, 5))
        self.otherMatrix.add(_MatrixElement(2, 2, 1))
        self.otherMatrix.add(_MatrixElement(3, 1, 8))
        x = self.matrix * self.otherMatrix
        print(len(x._elementList))
        for ele in x._elementList:
            print(ele.row,ele.col,ele.value)

