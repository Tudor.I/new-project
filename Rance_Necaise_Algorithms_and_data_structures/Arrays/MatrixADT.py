"""
A matrix is a collection of scalar values arranged in rows and columns as a rectangular grid of a fixed size. The elements of the matrix can be accessed by specifying
a given row and column index with indices starting at 0.
 Matrix( rows, ncols ): Creates a new matrix containing nrows and ncols
with each element initialized to 0.
 numRows(): Returns the number of rows in the matrix.
2.4 The Matrix Abstract Data Type 53
 numCols(): Returns the number of columns in the matrix.
 getitem ( row, col ): Returns the value stored in the given matrix element.
Both row and col must be within the valid range.
 setitem ( row, col, scalar ): Sets the matrix element at the given row and
col to scalar. The element indices must be within the valid range.
 scaleBy( scalar ): Multiplies each element of the matrix by the given scalar
value. The matrix is modified by this operation.
 transpose(): Returns a new matrix that is the transpose of this matrix.
 add ( rhsMatrix ): Creates and returns a new matrix that is the result of
adding this matrix to the given rhsMatrix. The size of the two matrices must
be the same.
subtract ( rhsMatrix ): The same as the add() operation but subtracts the
two matrices.
multiply ( rhsMatrix ): Creates and returns a new matrix that is the result
of multiplying this matrix to the given rhsMatrix. The two matrices must be
of appropriate sizes as defined for matrix multiplication.
"""

from Rance_Necaise_Algorithms_and_data_structures.Arrays.Array2D_ADT import Array2D
import matplotlib.pyplot as plt
import numpy as np
class Matrix:
    #creates a mtrix of size numRows x numCols initialized to 0
    def __init__(self, numRows, numCols):
        self._theGrid = Array2D(numRows, numCols)
        self._theGrid.clear(0)

    #returns the number of rows in the matrix
    def numRows(self):
        return self._theGrid.numRows()

    #returns the number of columns in the matrix
    def numCols(self):
        return self._theGrid.numCols()

    #returns the value of element (i,j):x[i,j]
    def __getitem__(self, ndxTuple):
        return self._theGrid[ndxTuple[0], ndxTuple[1]]

    def __setitem__(self, ndxTuple, scalar):
        self._theGrid[ndxTuple[0], ndxTuple[1]] = scalar

    #scales the matrix by the given scalar
    def scaleBy(self, scalar):
        for r in range(self.numRows()):
            for c in range(self.numCols()):
                self[r,c] *= scalar





    def transpose(self):
        new_matrix = Array2D(numRows=self._theGrid.numCols(), numCols=self._theGrid.numRows())
        return new_matrix

    def __add__(self, rhsMatrix):
        assert (rhsMatrix.numRows() == self.numRows()) and (
            rhsMatrix.numCols() == self.numCols()), \
            'Matrix sizes not compatible for the add opperation'
        #create the new matrix
        newMatrix = Matrix(numRows=self.numRows(), numCols = self.numCols())
        # Add the coresponding elements in the two matrices.
        for r in range(self.numRows()):
            for c in range(self.numCols()):
                newMatrix[r,c] = self[r,c] + rhsMatrix[r,c]
        return newMatrix

    def __sub__(self, rhsMatrix):
        assert (rhsMatrix.numRows() == self.numRows()) and (
                rhsMatrix.numCols() == self.numCols()), \
            'Matrix sizes not compatible for the add opperation'
        # create the new matrix
        newMatrix = Matrix(numRows=self.numRows(), numCols = self.numCols())
        # Add the coresponding elements in the two matrices.
        for r in range(self.numRows()):
            for c in range(self.numCols()):
                newMatrix[r, c] = self[r, c] - rhsMatrix[r, c]
        return newMatrix

    def __mul__(self, rhsMatrix):
        #we create a new matrix based on the nr max of rows and cols
        newMatrix = Matrix(numRows=self.numRows(), numCols=rhsMatrix.numCols())

        for r in range(self.numRows()):
            for c in range(rhsMatrix.numCols()):
                for r1 in range(rhsMatrix.numRows()):
                    newMatrix[r,c] += self[r,r1] * rhsMatrix[r1, c]
        return newMatrix

    def display_matrix(self):
        aa = np.zeros((self.numRows(), self.numCols()))
        for i in range(min(self.numRows(), self.numCols())):
            aa[i,i] = i
        return aa

    def plot(self):
        plt.matshow(self.display_matrix())
        plt.show()
if __name__ == "__main__":
    A = Matrix(numRows=6, numCols=3)
    B = Matrix(numRows=3, numCols=5)
    A[0,0] = 34
    A[0,1] = 1
    A[0,2] = 77
    A[1,0] = 2
    A[1,1] = 14
    A[1,2] = 8
    A[2, 0] = 3
    A[2,1] = 17
    A[2,2] = 11
    B[0, 0] = 6
    B[0, 1] = 8
    B[0, 2] = 1
    B[1, 0] = 9
    B[1, 1] = 27
    B[1, 2] = 5
    B[2, 0] = 2
    B[2, 1] = 43
    B[2, 2] = 31
    lista = [(1,2), (2,2), (3,1), (1,1), (4,10), (30, 30), (1,0)]

    C = A * B
    print(C[0,0], C[0,1], C[0,2],'\n',
              C[1,0], C[1,1], C[1,2],'\n',
              C[2,0], C[2,1], C[2,2])

    C.plot()





