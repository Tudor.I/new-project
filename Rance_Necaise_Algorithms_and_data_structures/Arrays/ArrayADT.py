# Implements the Array ADT using array capabilities of the ctypes module.
import ctypes

class Array:
    def __init__(self, *size, multiply=None):

        if multiply is not None and len(size) == 0:
            self._initialsize = len(size)
            self._arraysize = multiply
            PyArrayType = ctypes.py_object * (self._arraysize)
            self._elements = PyArrayType()
        elif len(size) > 0 and multiply is None:
            self._initialsize = len(size)
            self._arraysize = self._initialsize + 1
            #Creating the array structure using the ctypes module
            PyArrayType = ctypes.py_object * (self._arraysize)
            self._elements = PyArrayType()
        elif len(size) == 0 and multiply is None:
            self._initialsize = len(size)
            self._arraysize = self._initialsize + 1
            PyArrayType = ctypes.py_object * (self._arraysize)
            self._elements = PyArrayType()
        else:
            raise Exception("The array can't take two arguments at once")
        # Initialize each element.
        self.clear(None)
        i = 0
        for elem in size:
            self._elements[i] = elem
            i+=1

    def __str__(self):
        for i in range(self.length()):
            print(self._elements[i])


    def __len__(self):
        """Returns the size of the array"""
        return self._arraysize

    def length(self):
        """Returns the number of elements contained by the array"""
        return self._initialsize

    def __getitem__(self, index):
        # Gets the contents of the index element
        assert index >=0 and index < self._arraysize, "Array subscript out of range"
        return self._elements[index]

    def getItem(self, ndx):
        """Returns an item from a specific index, within the length of it"""

        assert ndx >=0 and isinstance(ndx, int) and ndx < self._initialsize, "Subscript out of range"
        return self._elements[ndx]

    def __setitem__(self, index, value):
        """Sets an item at a index value wherever in the array, as it remains within the size of it"""
        assert index >= 0 and index < self._arraysize, "Array subscript out of range"
        self._elements[index] = value

    def setItem(self, ndx, value):
        """Sets a specific value at a specific ndx, within the range of elements contained
                            by the array and not the overall size"""
        assert ndx >=0 and isinstance(ndx,int) and ndx < self._initialsize, "Subscript out of range"
        self._elements[ndx] = value

    def contains(self, item):
        """Verifies whether an item is contained by the array or not. Returns a boolean value"""

        for i in range(self.length()):
            if self._elements[i] == item:
                return True
            else:
                return False

    def add(self, item):
        """Add an item at the end of the array. The size of the array can increase according to the lenght of the array"""

        try:
            self._elements[self.length()] = item
            self._initialsize += 1
        except (IndexError, AssertionError):
            lenght = self.length()
            if self._arraysize % 2 == 0:
                self._arraysize = lenght * 2
                Temporary = ctypes.py_object * self._arraysize
                tmp = Temporary()
                for i in range(self._arraysize):
                    tmp[i] = None
                i = 0
                while i < self.length():
                    tmp[i] = self._elements[i]
                    i += 1
                self._elements = tmp
                self._elements[lenght] = item
                self._initialsize +=1

            else:
                self._arraysize = lenght*2 + 1
                Temporary = ctypes.py_object * self._arraysize
                tmp = Temporary()
                for i in range(self._arraysize):
                    tmp[i] = None
                i = 0
                while i < self.length():
                    tmp[i] = self._elements[i]
                    i += 1
                self._elements = tmp
                self._elements[lenght] = item
                self._initialsize +=1

    # Clears the array by setting each element to the given value
    def clear(self, value):
        """Initialize the array"""

        for i in range(self._arraysize):
            self._elements[i] = value


    def insert(self, ndx, value):
        """Inserts a value at a given index and returns a new array"""

        assert isinstance(ndx, int) and ndx >=0 and ndx < self.length(), "Wrong index type"
        tmp_array = Array(multiply=self.length())
        k = 0
        for i in range(self.length()):
            if i >= ndx:
                tmp_array[k] = self._elements[i]
                k+=1
        self.setItem(ndx, value)
        j = 0
        try:
            for i in range(self.length() + 1):
                if i > ndx:
                    self._elements[i] = tmp_array[j]
                    j += 1
            self._initialsize += 1
        except (AssertionError, IndexError):
            self._arraysize = self.length() * 2
            temporary = ctypes.py_object * (self._arraysize)
            tmp_arr = temporary()
            for i in range(self._arraysize):
                tmp_arr[i] = None
            k = 0
            for i in range(self.length() + 1):
                if i <=ndx:
                    tmp_arr[i] = self._elements[i]
                elif i > ndx:
                    tmp_arr[i] = tmp_array[k]
                    k += 1
            self._elements = tmp_arr
            self._initialsize += 1
        return self._elements

    def remove(self, ndx):
        """Removes an element from a given index and returns a new array. According to the size of the array which is checked after
                    each removal, the array can shrink its size by half when a given lenght is achieved"""

        assert isinstance(ndx, int) and ndx>=0  and ndx<self.length(), "Subscript out of range"

        if self._elements[ndx] == self._elements[self.length()-1]:
            self._elements[ndx] = None
            self._initialsize -= 1
        else:
            Temporary = ctypes.py_object * (self._arraysize)
            tmp = Temporary()
            for i in range(self._arraysize):
                tmp[i] = None
            k = 0
            for j in range(self.length()):
                if j != ndx:
                    tmp[k] = self._elements[j]
                    k +=1

            self._elements = tmp
            self._initialsize -= 1
        if self.length() < self._arraysize // 2:
            if self._arraysize // 2 == 1:
                length = self.length() + 2
            else:
                length = self.length() + 1
            self._arraysize = length
            Temporary = ctypes.py_object * self._arraysize
            tmp = Temporary()
            for i in range(self._arraysize):
                tmp[i] = None
            for i in range(self.length()):
                tmp[i]= self._elements[i]
            i = 0
            while i < length:
                self._elements[i] = tmp[i]
                i += 1
            self._elements = tmp
        return self

    def indxOf(self,item):
        """Returns the index of a given item. If the item is not contained, it raises an error"""
        x = False
        k = -1
        for i in range(self.length()):
            k+=1
            if self._elements[i] == item:
                x = True
                return k
        if not x:
            raise Exception("Item not contained in the array")

    def extend(self, otherArray):
        """Extends the array with a given array. The given array must be an instance of Array Class. It returns a new array."""

        assert isinstance(otherArray, Array), "Wrong Array type"
        if otherArray.length() == 0:
            raise Exception("The given array contains no elements")
        elif len(self) - self.length() >= otherArray.length():
            k = 0
            i = self.length()
            while k < otherArray.length():
                self._elements[i] = otherArray[k]
                k+=1
                i+=1
        else:
            for i in range(otherArray.length()):
                self.add(otherArray[i])
        return self

    def subVector(self, from_ndx, to_ndx):
        """Returns a subArray which contains the elements from the Array from a given index to another given index"""

        assert from_ndx >=0 and from_ndx < self.length() and isinstance(from_ndx,int), "Wrong index"
        assert to_ndx >=0 and from_ndx < self.length() and isinstance(to_ndx, int), "Wrong index"
        assert from_ndx < to_ndx, "From index should be always smaller than To index"

        subArray = Array()
        k = 0
        i = from_ndx
        while i < to_ndx:
            subArray.add(self._elements[i])
            k+=1
            i+=1

        return subArray

    # def temp_array(self, size):
    #     # Temporary = Array(multiply=si)
    #     # tmp = Temporary()
    #     for i in range(size):
    #         tmp[i] = None
    #     return tmp


    def __iter__(self):
        return _ArrayIterator(self._elements)

class _ArrayIterator:
    def __init__(self, theArray):
        self.arrayRef = theArray
        self._curNdx = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._curNdx < len(self.arrayRef):
            entry = self.arrayRef[self._curNdx]
            self._curNdx +=1
            return entry
        else:
            raise StopIteration


"""
The initialization of the array is done y calling the clear() method.
The clear() method is used to set each element of the array to a given value,
which it does by iterating over the elements using an index variable.
The len method, which returns the number of elements in the array, simply returns the
value of size that was saved in the constructor. The iter method creates
and returns an instance of the ArrayIterator private iterator class
First, the getitem operator method takes the array index as an argument and returns
the value of the corresponding element. The precondition must first be verified to
ensure the subscript is within the valid range.
The setitem operator method is used to set or change the contents of a
specific element of the array.


"""
if __name__ == "__main__":
    count = 0
    i = 16
    while i >= 1:
        count += 1
        i = i // 2
    print(count)

