"""Ex 6.1  """
from Rance_Necaise_Algorithms_and_data_structures.LinkedStructures.SinlgyLinkedList import LinkedList
def removeAll(head):
    """The removeAll(head) function, which accepts a head reference to a singly
linked list, unlinks and remove every node individually from the list.
"""
    curNode = head
    while head is not None:
        term = curNode
        head = head.next
    return head

# a = LinkedList(1)
# a.next = LinkedList(2)
# a.next.next = LinkedList(3)
# a.next.next.next = LinkedList(4)
#
# m = removeAll(a)
# print(m)

def splitInHalf(head):
    """The splitInHalf(head) function, which accepts a head reference to a
singly linked list, splits the list in half and returns the head reference to
the head node of the second half of the list. If the original list contains a
single node, None should be returned."""
    curNode = head
    halfList = None
    size = 0
    while curNode is not None:
        size +=1
        curNode = curNode.next
    if size == 1:
        return halfList
    else:
        i = 0
        curNode = head
        while curNode is not None:
            i +=1
            if i == size // 2:
                halfList = curNode
            curNode = curNode.next
    return halfList

"""ex 6.2"""
def fc():
    box = None
    temp = None
    for i in range(4):
        if i % 3 != 0:
            temp = LinkedList(i)
            temp.next = box
            box = temp
    return box

"""Consider the following singly linked list. Provide the instructions to insert the
new node immediately following the node containing 45. Do not use a loop or
any additional external references

73 -> 2 -> 52 ->18 ->36
"""

def ex6_3(head):
    """Likewise ex 6.4"""
    newNode = LinkedList(22)
    if head.next.data != 45:
        newNode.next = head
        head = head.next

    if head.next.data != 45:
        head = head.next
    if head.next.data == 45:
        head = newNode.next
        head = head.next
        newNode.next = head.next
        head.next = newNode
    return head

def ex6_4(head):
    curNode = head.next
    if curNode.next.next.data == 18:
        curNode.next.next = curNode.next.next.next
    return head



h = LinkedList(73)
h.next = LinkedList(2)
h.next.next = LinkedList(52)
h.next.next.next = LinkedList(18)
h.next.next.next.next = LinkedList(36)

m = ex6_3(h)
n = ex6_4(h)
