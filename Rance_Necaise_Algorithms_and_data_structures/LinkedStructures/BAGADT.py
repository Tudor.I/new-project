"""The following code represents a Bag container that uses for storing
the elements a linked list"""

class Bag:
    def __init__(self):
        self._head = None
        self._tail = None
        self._size = 0

    def __len__(self):
        """Returns the number of items in the bag"""
        return self._size

    def __contains__(self, target):
        curNode = self._head
        while curNode is not None and curNode != target:
            curNode = curNode.next
        return curNode is not None

    def add(self, item):
        """Adding an item using a bag which stores linked elements given the head
        and tail references"""
        newNode = _BagListNode(item)
        if self._head is None:
            self._head = newNode
        else:
            self._tail.next = newNode
        self._tail = newNode
        self._size += 1

    def remove(self, item):
        """Given the head and tail references, removes a target from a linked list"""
        preNode = None
        curNode = self._head
        while curNode is not None and curNode.item != item:
            preNode = curNode
            curNode = curNode.next
        assert curNode is not None, "The item must be in the bag"
        self._size -=1
        if curNode is not None:
            if curNode is self._head:
                self._head = curNode.next
            else:
                preNode.next = curNode.next
            if curNode is self._tail:
                self._tail = preNode

    def __iter__(self):
        return _BagIterator(self._head)


class _BagListNode(object):
    def __init__(self, item):
        self.item = item
        self.next = None
    def __repr__(self):
        return f"{self.item}"

class _BagIterator:
    def __init__(self, head):
        self._curNode = head

    def __iter__(self):
        return self

    def __next__(self):
        if self._curNode is None:
            raise StopIteration
        else:
            item = self._curNode.item
            self._curNode = self._curNode.next
            return item




if __name__ == "__main__":
    bag = Bag()
    bag.add(1)
    bag.add(2)
    bag.add(3)
    print(bag._head.item, bag._head.next.item, bag._head.next.next.item)
    bag.add(4)
    bag.add(5)
    bag.add(2)
    bag.add(3)
    bag.add(4)
    bag.add(5)
    bag.remove(1)
    for i in bag:
        print(i)

