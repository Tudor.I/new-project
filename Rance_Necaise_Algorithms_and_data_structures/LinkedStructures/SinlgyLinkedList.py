class LinkedList:
    def __init__(self, value):
        self.data = value
        self.next = None




"""Traversing through a linked list"""
def traversal(head):
    curNode = head
    while curNode is not None:
        print(curNode.value)
        curNode = curNode.next


"""searching trough an unsorted linked list"""
def unsortedsearch(head, target):
    curNode = head
    while curNode is not None and target != curNode.value:
        curNode = curNode.next
    return curNode is not None



"""removing an element from a unsorted linked list"""

def remove(head, target):
    predCord = None
    curNode = head
    while curNode is not None and curNode.value != target:
        predCord = curNode
        curNode = curNode.next

    if curNode is not None:
        if curNode is head:
            head = curNode.next
        else:
            predCord.next = curNode.next

