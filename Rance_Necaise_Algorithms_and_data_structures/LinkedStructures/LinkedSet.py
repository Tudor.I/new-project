class SetElementNode:
    def __init__(self, value):
        assert isinstance(value, int), "Wrong input"
        self.value = value
        self.next = None

"""The implementation of an unsorted SetADT using linked lists"""
class SetADTUnsorted:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def __len__(self):
        """Returns the size of the SET"""
        return self.size

    def __contains__(self, target):
        """O(N) time complexity. Verifies whether an element is contained within the Set or not"""
        curNode = self.head
        while curNode is not None and target != curNode.value:
            curNode = curNode.next
        return curNode is not None

    def __getitem__(self, item):
        """O(N) time complexity. Returns the item if it is contained within the linked list"""
        if item in self.head:
            return item
        else:
            raise Exception("The item is not contained within the set")

    def add(self, item):
        """O(1) time complexity. Adds a new element to the set"""
        curNode = self.head
        if curNode is None:
            curNode = item
            self.tail = item
        else:
            self.tail.next = item
        self.tail = item
        self.size +=1

    def remove(self, item):
        """Removes the given element from the linked list"""
        assert item in self.head, "Can't be removed an element which" \
                                  "does not exists within the list"
        """O(N) time complexity"""
        curNode = self.head
        predNode = None
        while curNode is not None and item != curNode:
            predNode = curNode
            curNode = curNode.next
        if curNode is not None:
            if curNode is self.head:
                curNode = curNode.next
                self.head = curNode
            elif curNode is self.tail:
                predNode.next = None
                self.tail = predNode
            else:
                predNode.next = curNode.next
            self.size -=1
    def isSubSetOf(self, SetB):
        """O(N^2) time complexity. Verifies whether SetA is a sub set of SetB"""
        curNode = self.head
        while curNode is not None:
            if curNode not in SetB:
                return False
            curNode = curNode.next
        return True

    def intersection(self, SetB):
        """O(N^2) time complexity. Computes the intersection of two given sets"""
        curNode = self.head
        newSet = SetADTUnsorted()
        while curNode is not None:
            if curNode in SetB:
                newSet = newSet.add(curNode)
                curNode = curNode.next
            else:
                curNode = curNode.next
        return newSet if newSet.head is not None else None

    def __add__(self, otherSet):
        """O(N^2) time complexity. Computes the union of two sets"""
        newSet = self
        curNode = otherSet.head
        while curNode is not None:
            if curNode not in self.head:
                newSet.add(curNode)
            curNode = curNode.next
        return newSet

    def __mul__(self, otherSet):

        newSet = self
        curNode = self.head
        while curNode is not None:
            if curNode not in otherSet:
                newSet = newSet.remove(curNode)
            curNode = curNode.next
        return newSet

    def __sub__(self, otherSet):
        """O(N^2) time complexity."""
        newSet = self
        curNode = self.head
        while curNode is not None:
            if curNode in otherSet:
                newSet.remove(curNode)
            curNode = curNode.next
        return newSet



class SetADTSorted:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def addTerm(self, item):
        """Appends new items to the set, mantaining the ascending ordering
        of the Set"""
        """O(N) time complexity"""
        assert isinstance(item, SetElementNode) and item not in self.head,"Wrong instance"
        if self.head is None:
            self.head = item
            self.tail = item
        else:
            curNode = self.head
            predNode = None
            while curNode is not None:
                predNode = curNode
                curNode = curNode.next
                if curNode is None and predNode is self.head:
                    if item.value > predNode.value:
                        item.next = predNode
                        self.head = item
                        self.tail = predNode
                    elif item.value < predNode.value:
                        predNode.next = item
                        self.tail = item
                    else:
                        pass
                elif curNode is None and predNode is self.tail and item.value < self.tail.value:
                    predNode.next = item
                    self.tail = item
                elif curNode is None and predNode is self.tail and item.value == self.tail.value:
                    pass
                else:
                    if predNode is self.head and predNode.value < item.value:
                        item.next = predNode
                        self.head = item
                    elif predNode is self.head and predNode.value < item.value:
                        item.next = predNode
                        self.head = item
                    elif predNode is self.head and predNode.value == item.value:
                        pass
                    elif predNode.value > item.value > curNode.value:
                        item.next = curNode
                        predNode.next = item



    def __len__(self):
        """Returns the size of the set"""
        return self.size

    def __contains__(self, item):
        """ O(N) time complexity. Verifies whether an element is contained within the Set or not"""
        curNode = self.head
        while curNode is not None and item !=curNode.value:
            curNode = curNode.next
        return curNode is not None

    def __getitem__(self, item):
        """O(N) time complexity- Returns the item if it is contained within the linked list"""

        if item in self.head:
            return item
        else:
            raise Exception("The item is not contained within the set")

    def remove(self, item):
        assert item in self.head, "Can't be removed an element which" \
                                  "does not exists within the list"
        """O(N) time complexity. Removes an element if exists from the list"""
        curNode = self.head
        predNode = None
        while curNode is not None and item != curNode:
            predNode = curNode
            curNode = curNode.next
        if curNode is not None:
            if curNode is self.head:
                curNode = curNode.next
                self.head = curNode
            elif curNode is self.tail:
                predNode.next = None
                self.tail = predNode
            else:
                predNode.next = curNode.next
            self.size -= 1

    def isSubSetOf(self, SetB):
        """O(N) time complexity - Verifies whether SetA is a sub set of SetB"""
        curNode = self.head
        otherNode = SetB.head
        nr = 0
        while curNode is not None and otherNode is not None:
            if curNode.value < otherNode.value:
                curNode = curNode.next
            elif curNode.value > otherNode.value:
                otherNode = otherNode.next
            else:
                nr += 1
                curNode = curNode.next
                otherNode = otherNode.next
        if nr == len(self): return True
        else: return False

    def intersect(self, setB):
        """O(n) computes the intersection of two given sets"""
        curNode = self.head
        otherNode = setB.head
        newSet = SetADTSorted()
        while curNode is not None and otherNode is not None:
            if curNode.value < otherNode.value:
                curNode = curNode.next
            elif curNode.value > otherNode.value:
                otherNode = otherNode.value
            else:
                newSet.addTerm(curNode)
                curNode = curNode.next
                otherNode = otherNode.next
        return newSet

    def __add__(self, setB):
        """O(N) time-complexity -> adds the setB to the given set"""
        newSet = self
        otherNode = setB.head
        while otherNode is None:
            if otherNode not in newSet:
                newSet.addTerm(otherNode)
                otherNode = otherNode.next
            else:
                otherNode = otherNode.next

        return newSet


    def __sub__(self, otherSet):
        """O(n) time-complexity. Computes the difference of two sets"""
        a = 0
        b = 0
        newSet = SetADTSorted()
        curNode = self.head
        otherNode = otherSet.head
        while a < len(self) and b < len(otherSet):
            if curNode.value < otherNode.value and curNode.value != otherNode.value:
                newSet.addTerm(curNode)
                curNode = curNode.next
                a+=1
            elif curNode.value > otherNode.value:
                otherNode = otherNode.next
                b+=1
            else:
                curNode = curNode.next
                otherNode = otherNode.next
                a+=1
                b+=1
        while a < len(self):
            newSet.addTerm(curNode)
            curNode = curNode.next
        return newSet

    def union(self, setB):
        """O(n) time complexity. Computes the union of two sets"""
        curNode = self.head
        otherNode = setB.head
        newSet = SetADTSorted()
        while curNode is not None and otherNode is not None:
            if curNode.value < otherNode.value:
                newSet.addTerm(curNode)
                curNode = curNode.next
            elif curNode.value > otherNode.value:
                newSet.addTerm(otherNode)
                otherNode = otherNode.next
            else:
                newSet.addTerm(curNode)
                curNode = curNode.next
                otherNode = otherNode.next

        while curNode is not None:
            newSet.addTerm(curNode)
            curNode = curNode.next

        while otherNode is not None:
            newSet.addTerm(otherNode)
            otherNode = otherNode.next

        return newSet

    def __iter__(self):
        return SetIterator(self.head)

class SetIterator:
    def __init__(self, head):
        self.node = head

    def __iter__(self):
        return self

    def __next__(self):
        if self.node is not None:
            item = self.node
            self.node = self.node.next
            return item
        else:
            raise StopIteration

















