"""The ploynomial ADT
        A polynomial is a mathematical expression of a variable constructed
one or more terms.
"""
class _PolyTermNode:
    def __init__(self, degree, coefficient):
        self.degree = degree
        self.coefficient = coefficient
        self.next = None
class Polynomial:
    def __init__(self, degree = None, coefficient = None):
        """Creates a new polynomial initialized to be empty and thus
            containing no terms"""
        if degree is None:
            self._polyHead = None
        else:
            self._polyHead = _PolyTermNode(degree, coefficient)
        self._polyTail = self._polyHead

    def degree(self):
        """Returns the degree of the polynomial. If the polynomial contains
no terms, a value of −1 is returned."""
        if self._polyHead is None:
            return - 1
        else:
            return self._polyHead.degree

    def __getitem__(self, degree):
        """ Returns the coefficient for the term of the provided degree. Thus, if the expression of this polynomial is x
3 + 4x + 2 and a degree of 1 is provided, this operation returns 4.
The coefficient cannot be returned for an empty polynomial."""
        curNode = self._polyHead
        while curNode is not None and curNode.degree > degree:
            curNode = curNode.next
        if curNode is None or curNode.degree != degree:
            return 0
        else:
            return curNode.coefficient

    def evaluate(self, scalar):
        """Evaluates the polynomial at the given scalar value and
returns the result. An empty polynomial cannot be evaluated."""

        assert self.degree() >=0, "Only non-empty polynomials can be evaluated"
        result = 0
        curNode = self._polyHead
        while curNode is not None:
            result += curNode.coefficient * (scalar ** curNode.degree)
            curNode = curNode.next
        return result
    def _append(self, degree, coefficient):
        if coefficient != 0:
            newTerm = _PolyTermNode(degree,coefficient)
            if self._polyHead is None:
                self._polyHead = newTerm
            else:
                self._polyTail.next = newTerm
            self._polyTail = newTerm
    def appendTerm(self, degree, coefficient):
        """place the poly term at the right place in linked list according to the degree
        and coefficient"""
        if coefficient != 0:
            newTerm = _PolyTermNode(degree, coefficient)
            if self._polyHead is None:
                self._polyHead = newTerm
                self._polyTail = newTerm
            else:
                curNode = self._polyHead
                predNode = None
                while curNode is not None:
                    predNode = curNode
                    curNode = curNode.next
                    if curNode is None and predNode is self._polyHead:
                        if newTerm.degree > predNode.degree:
                            newTerm.next = predNode
                            self._polyHead = newTerm
                            self._polyTail = predNode
                        elif newTerm.degree == predNode.degree:
                            predNode.coefficient += newTerm.coefficient
                        else:
                            self._polyHead.next = newTerm
                            self._polyTail = newTerm
                    elif curNode is None and predNode is self._polyTail and newTerm.degree < self._polyTail.degree:
                        predNode.next = newTerm
                        self._polyTail = newTerm
                    elif curNode is None and predNode is self._polyTail and newTerm.degree == self._polyTail.degree:
                        self._polyTail.coefficient+=newTerm.coefficient
                    else:
                        if predNode is self._polyHead and predNode.degree < newTerm.degree:
                            newTerm.next = predNode
                            self._polyHead = newTerm
                        elif predNode is self._polyHead and predNode.degree == newTerm.degree:
                            predNode.coefficient+=newTerm.coefficient
                        elif predNode.degree > newTerm.degree > curNode.degree:
                            newTerm.next = curNode
                            predNode.next = newTerm

    def simple_add(self, otherPoly):
        """A more brutal approach of adding two polynomials. The new polynomial is created
        by iterating over the two original polynomials, term by term, from the largest
        degree among the two polynomials down to degree 0"""
        assert isinstance(otherPoly, Polynomial), "Wrong instance"
        newPoly = Polynomial()
        if self.degree() > otherPoly.degree():
            maxDegree = self.degree()
        else:
            maxDegree = otherPoly.degree()
        i = maxDegree
        while i >= 0:
            value = self[i] + otherPoly[i]
            newPoly.appendTerm(i, value)
            i -= 1
        return newPoly

    def __add__(self, otherPoly):
        assert self.degree() >= 0 and otherPoly.degree() >= 0, "Addition of non empty polynomials" \
                                                               "is not allowed"
        newPoly = Polynomial()
        nodeA = self._polyHead
        nodeB = otherPoly._polyHead
        while nodeA is not None and nodeB is not None:
            if nodeA.degree > nodeB.degree:
                degree = nodeA.degree
                coefficient = nodeA.coefficient
                nodeA = nodeA.next
            elif nodeA.degree < nodeB.degree:
                degree = nodeB.degree
                coefficient = nodeB.coefficient
                nodeB = nodeB.next
            else:
                degree = nodeA.degree
                coefficient = nodeA.coefficient + nodeB.coefficient
                nodeB = nodeB.next
                nodeA = nodeA.next
            newPoly.appendTerm(degree, coefficient)
        while nodeA is not None:
            newPoly.appendTerm(nodeA.degree, nodeA.coefficient)
            nodeA = nodeA.next
        while nodeB is not None:
            newPoly.appendTerm(nodeB.degree, nodeB.coefficient)
        return newPoly

    def multiply(self, otherPoly):
        assert self.degree() >= 0 and otherPoly.degree() >= 0,"Multiplication only allowed on non-empty" \
                                                    "polynomials"
        """Following lines of codes takes the poly head and multiplies
        with the degree and coefficient of it, every element from the other poly
        and returns a new poly which is the result of this operation"""
        node = self._polyHead
        newPoly = otherPoly._termMultiply(node)
        node = node.next
        while node is not None:
            temPoly = otherPoly._termMultiply(node)
            newPoly = newPoly.simple_add(temPoly)
            node = node.next
        return newPoly

    def _termMultiply(self, termNode):
        newPoly = Polynomial()
        curr = self._polyHead
        while curr is not None:
            newDegree = curr.degree + termNode.degree
            newCoeff = curr.coefficient * termNode.coefficient
            newPoly.appendTerm(newDegree, newCoeff)
            curr = curr.next
        return newPoly
    def __mul__(self, otherPoly):
        """The implementation of the Polynomial mul method is O(n^2) in the worst
case. Design and implement a more efficient solution for this operation."""
        curNode = self._polyHead
        otherNode = otherPoly._polyHead
        tmpPoly = Polynomial()
        newPoly = Polynomial()
        while curNode is not None:
            if otherNode is otherPoly._polyTail:
                newdegree = curNode.degree + otherNode.degree
                coef = curNode.coefficient * otherNode.coefficient
                tmpPoly.appendTerm(newdegree, coef)
                if newPoly._polyHead is None:
                    newPoly = tmpPoly
                else:
                    newPoly = newPoly.simple_add(tmpPoly)
                tmpPoly = Polynomial()
                otherNode = otherPoly._polyHead
                curNode = curNode.next
            else:
                newdegree = curNode.degree + otherNode.degree
                coef = curNode.coefficient * otherNode.coefficient
                tmpPoly.appendTerm(newdegree, coef)
                otherNode = otherNode.next
        return newPoly

    def __iter__(self):
        return PolyIterator(self._polyHead)

class PolyIterator:
    def __init__(self, head):
        self.curNode = head

    def __iter__(self):
        return self

    def __next__(self):
        if self.curNode is None:
            raise StopIteration
        else:
            degree = self.curNode.degree
            coef = self.curNode.coefficient
            self.curNode = self.curNode.next
            return (degree, coef)


"""Ex 6.5 The implementation of the Polynomial mul method is O(n^2)in the worst
case. Design and implement a more efficient solution for this operation.
"""






polynom1 = Polynomial()
polynom1.appendTerm(3, 1)
polynom1.appendTerm(2, 5)
polynom1.appendTerm(1,6)

# x =x^3 + 5*x^2 + 6
# y = 12x^3 + 6x^2 + 25

polynom = Polynomial()
# polynom.appendTerm(7, 1)
polynom.appendTerm(3, 2)
polynom.appendTerm(3, 10)
polynom.appendTerm(2, 6)
# polynom.appendTerm(10, 20)
# polynom.appendTerm(12, 20)
# polynom.appendTerm(16, 20)
# polynom.appendTerm(4, 10)
# polynom.appendTerm(5,2)
polynom.appendTerm(1, 25)
# polynom.appendTerm(1, 10)
# polynom.appendTerm(15, 10)
# print(polynom._polyHead.degree)
# print(polynom._polyHead.next.degree)
# print(polynom._polyHead.next.next.degree)













