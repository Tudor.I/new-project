if coefficient != 0:
    newTerm = _PolyTermNode(degree, coefficient)
    if self._polyHead is None:
        self._polyHead = newTerm
        self._polyTail = newTerm
        return
    curNode = self._polyHead
    predNode = None
    while curNode is not None:
        if (curNode is self._polyHead and
            self._polyHead.next is None) or (newTerm.degree > self._polyHead.degree):
            if curNode.degree > newTerm.degree:
                curNode.next = newTerm
                self._polyHead = curNode
            elif curNode.degree < newTerm.degree:
                newTerm.next = curNode
                self._polyHead = newTerm
            else:
                self._polyHead.coefficient += newTerm.coefficient
        else:
            if curNode is None and newTerm.degree > predNode.degree:
                newTerm.next = predNode
                self._polyHead = predNode
            elif predNode.degree > newTerm.degree > curNode.degree and curNode is not None:
                newTerm.next = curNode
                predNode.next = newTerm
            else:
                if predNode is self._polyTail and predNode.degree > newTerm.degree:
                    self._polyTail.next = newTerm
                    self._polyTail = newTerm
                elif predNode is self._polyTail and predNode.degree < newTerm.degree:
                    newTerm.next = self._polyTail
        predNode = curNode
        curNode = curNode.next
    if newTerm.degree < self._polyTail.degree:
        self._polyTail.next = newTerm
    self._polyTail = newTerm

    # else:
    #     curNode = self._polyHead
    #     predNode = None
    #     if degree < curNode.degree:
    #         while curNode is not None:
    #             predNode = curNode
    #             curNode = curNode.next
    #             if curNode is not None:
    #                 if degree < curNode.degree and predNode.degree > degree:
    #                     predNode = curNode
    #                     curNode = curNode.next
    #                 elif degree < curNode.degree and degree > predNode.degree:
    #                     break
    #             else:
    #                 break
    #     elif degree > curNode.degree:
    #         while curNode is not None:
    #             if curNode is self._polyHead:
    #                 if degree >= curNode.degree:
    #                     break
    #             else:
    #                 break
    #     else:
    #         while curNode is not None and degree == curNode.degree:
    #             predNode = curNode
    #             curNode = curNode.next
    #     if curNode is not None:
    #         if curNode is self._polyHead:
    #             if self._polyHead.degree < degree:
    #                 newTerm.next = curNode
    #                 self._polyHead = newTerm
    #             elif self._polyHead == degree:
    #                 self._polyHead.coefficient += newTerm.coefficient
    #         else:
    #             if curNode.degree > degree:
    #                 newTerm.next = curNode.next
    #                 curNode = newTerm
    #             elif predNode.degree < degree:
    #                 newTerm.next = predNode
    #                 predNode.next = newTerm
    #             else:
    #                 curNode.coefficient += newTerm.coefficient
    #         if curNode is self._polyTail:
    #             self._polyTail = newTerm
    #     self._polyTail.next = newTerm
    # self._polyTail = newTerm
    #
    # while r < self.numRows():
    #     if curNode is None:
    #         try:
    #             r += 1
    #             curNode = self._listOfRows[r]
    #             newNode = None
    #             continue
    #         except AssertionError:
    #             break
    #     newVal = curNode.value
    #     newRow = r
    #     col = curNode.col
    #     newElement = _MatrixElementNode(col, newVal)
    #     if newNode is None:
    #         newMatrix._listOfRows[newRow] = newElement
    #         newNode = newElement
    #     newNode = newNode.next
    #     curNode = curNode.next
    # r = 0
    # otherNode = otherMatrix._listOfRows[r]
    # newNode = newMatrix._listOfRows[r]
    # while r < otherMatrix.numRows():
    #     if newNode is None:
    #         try:
    #             r += 1
    #             newNode = newMatrix._listOfRows[r]
    #             otherNode = None
    #             continue
    #         except AssertionError:
    #             break
    #     if otherNode is not None and newNode.col == otherNode.col:
    #         newNode.value += otherNode.value
    #         otherNode = otherNode.next
    #         newNode = newNode.next
    #     else:
    #         newNode = newNode.next