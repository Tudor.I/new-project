from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array
"""The following code is the implementation of the Sparse Matrix ADT using an array of linked lists"""
class SparseMatrix:
    """Creates a sparse matrix of size numRows X numCols initialized to 0"""
    def __init__(self, numRows, numCols):
        self._numCol = numCols
        self._listOfRows = Array(numRows)

    def numRows(self):
        """Returns the number of rows in the matrix"""
        return len(self._listOfRows)

    def numCols(self):
        return self._numCol

    def __getitem__(self, ndxTuple):
        """Returns the given item from the ndxTuple. This is to be done as homework"""
        curNode = self._listOfRows[ndxTuple[0]]
        while curNode is not None and curNode.col != ndxTuple[1]:
            curNode = curNode.next
        if curNode is not None and curNode.col == ndxTuple[1]:
            return curNode.value
        else:
            return None



    def __setitem__(self, ndxTuple, value):
        """Sets a specific value to the given (row, col) index tuple"""
        predNode = None
        curNode = self._listOfRows[ndxTuple[0]]
        while curNode is not None and curNode.col != ndxTuple[1]:
            predNode = curNode
            curNode = curNode.next

        if curNode is not None and curNode.col == ndxTuple[1]:
            if value == 0.0:
                if curNode == self._listOfRows[ndxTuple[0]]:
                    self._listOfRows[ndxTuple[0]] = curNode.next
                else:
                    predNode.next = curNode.next
            else:
                curNode.value = value

        elif value != 0.0:
            newNode = _MatrixElementNode(ndxTuple[1], value)
            newNode.next = curNode
            if curNode == self._listOfRows[ndxTuple[0]]:
                self._listOfRows[ndxTuple[0]] = newNode
            else:
                predNode.next = newNode

    def scaleBy(self, scalar):
        """Multiplies the values of the elements within the linked list by a given scalar"""
        for row in range(self.numRows()):
            curNode = self._listOfRows[row]
            while curNode is not None:
                curNode.value *= scalar
                curNode = curNode.next

    def __add__(self, rshMatrix):
        """Adds the values of the elements from another matrix to this matrix"""

        assert (rshMatrix.numRows() == self.numRows() and
        rshMatrix.numCols() == self.numCols()), "Matrix sizes not compatible for adding"
        newMatrix = SparseMatrix(self.numRows(), self.numCols())
        for row in range(self.numRows()):
            curNode = self._listOfRows[row]
            while curNode is not None:
                newMatrix[row, curNode.col] = curNode.value
                curNode = curNode.next

        for row in range(rshMatrix.numRows()):
            curNode = rshMatrix._listOfRows[row]
            while curNode is not None:
                value = newMatrix[row, curNode.col]
                if value is None:
                    curNode = curNode.next
                    continue
                value += curNode.value
                newMatrix[row, curNode.col] = value
                curNode = curNode.next

        return newMatrix

    def add(self, otherMatrix):
        assert (self.numRows() == otherMatrix.numRows() and
        self.numCols() == otherMatrix.numCols()), "Adition can be performed only on matrices with same size MxN"
        r = 0
        newMatrix = SparseMatrix(self.numRows(), self.numCols())
        curNode = self._listOfRows[r]
        newNode = None



        return newMatrix


    def transpose(self):
        newSparseMatrix = SparseMatrix(numRows=self.numCols(), numCols=self.numRows())
        curNode = self._listOfRows
        newNode = newSparseMatrix._listOfRows
        i = 0
        while i < self.numRows():
            curNode = self._listOfRows[i]
            while curNode is not None:
                newRow = curNode.col
                val = curNode.value
                newCol = i
                newSparseMatrix[newRow, newCol] = val
                curNode = curNode.next
            i+=1
        return newSparseMatrix





class _MatrixElementNode:
    def __init__(self, col, value):
        self.col = col
        self.value = value
        self.next = None

if __name__ == "__main__":
    matrix = SparseMatrix(3, 3)
    matrix[1,2] = 2
    matrix[2, 2] = 2
    matrix[0,2] = 3
    otherMatrix = SparseMatrix(3,3)
    otherMatrix[0,0] = 4
    otherMatrix[1,2] = 3
    l = matrix + otherMatrix
    m = matrix.add(otherMatrix)


    matrixTr = matrix.transpose()








    #
    # matrix1 = SparseMatrix(5, 5)
    # matrix1[1,2] = 5
    # matrix1[4,4] = 20
    # matrix[3,3] = 10
    # print(matrix[3,2])
    # l = matrix + matrix1
    # print(l[4,4])
    # print(l[1,2])
    # print(l[3,3])
    # print(l[2,2])
