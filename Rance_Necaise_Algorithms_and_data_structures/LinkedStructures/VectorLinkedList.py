class VectorElement:
    def __init__(self, value):
        self.data = value
        self.index = 0
        self.next = None


class LinkedVector:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def __len__(self):
        """Returns the size of the Vector"""
        return self.size

    def __contains__(self, item):
        """Verifies whether an element is contained by the list"""
        curNode = self.head
        while curNode is not None and item != curNode.data:
            curNode = curNode.next
        return curNode is not None

    def __getitem__(self, item):
        """Returns the given itme, only if it is contained by the linked list"""
        curNode = self.head
        while curNode is not None and curNode.data != item:
            curNode = curNode.next
        if curNode is not None:
            return curNode
        else:
            return None

    def append(self, item):
        """Appends the element of a vector to a linked list"""
        curNode = self.head
        if curNode is None:
            self.head = item
            self.tail = item
            self.size += 1
        else:
            while curNode is not None:
                item.index +=1
                curNode = curNode.next
            if curNode is  None:
                self.tail.next = item
            self.tail = item
            self.size +=1

    def insert(self, ndx, item):
        """"" Inserts the given item in the element at position
ndx. The items in the elements at and following the given position are
shifted down to make room for the new item. ndx must be within the
valid range.
"""""
        assert 0<=ndx<self.size, "Index out of range"
        item.index = ndx
        curNode = self.head
        predNode = None
        while curNode is not None and curNode.index != ndx:
            predNode = curNode
            curNode = curNode.next
        if curNode is self.head:
            item.next = curNode
            self.head = item
            ot = self.head.next
            while ot is not None:
                ot.index +=1
                ot = ot.next
            self.size +=1
        elif curNode is self.tail:
            curNode.index +=1
            item.next = curNode
            predNode.next = item
        else:
            item.next = curNode
            predNode.next = item
            while curNode is not None:
                curNode.index +=1
                curNode = curNode.next
        return self

    def remove(self, ndx):
        """"Removes and returns the item from the element from the
given ndx position. The items in the elements at and following the given
position are shifted up to close the gap created by the removed item. ndx
must be within the valid range."""
        assert 0<=ndx<self.size, "Index out of range"
        curNode = self.head
        predNode = None
        element = None
        while curNode is not None and curNode.index != ndx:
            predNode = curNode
            curNode = curNode.next
        if curNode is self.head:
            element = curNode
            self.head = curNode.next
            curNode = curNode.next
            while curNode is not None:
                curNode.index -= 1
                curNode = curNode.next
        elif curNode is self.tail:
            element = curNode
            predNode.next = None
            self.tail = curNode
        else:
            element = curNode
            predNode.next = curNode.next
            curNode = curNode.next
            while curNode is not None:
                curNode.index -= 1
                curNode = curNode.next
        self.size -=1
        element.next = None
        return element

    def indexOf(self, item):
        """ Returns the index of the vector element containing the
given item. The item must be in the list.
"""
        assert isinstance(item, VectorElement), "Wrong input"
        data = item.data
        curNode = self.head
        while curNode is not None and curNode.data != data:
            curNode = curNode.next
        return curNode.index if curNode is not None else None

    def extend(self, otherVector):
        """Extends the current vector with the elements of a new vector"""
        lastNode = otherVector.head
        index = self.tail.index
        tailNode = self.tail
        while lastNode is not None:
            self.size +=1
            index +=1
            lastNode.index = index
            tailNode.next = lastNode
            lastNode = lastNode.next
            tailNode = tailNode.next
        self.tail = otherVector.tail
        return self

    def subVector(self, fromNdx, toNdx):
        assert 0 <= fromNdx < toNdx <= self.size, "Wrong index input"
        """: Creates and returns a new vector that contains
a subsequence of the items in the vector between and including those
indicated by the given from and to positions. Both the from and to
positions must be within the valid range.
"""
        curNode = self.head
        newVector = LinkedVector()
        while curNode is not None and curNode.index != fromNdx:
            curNode = curNode.next
        data = curNode.data
        index = 0
        newVector.size += 1
        newVector.head = VectorElement(data)
        newNode = newVector.head
        newNode.tail = newVector.head
        curNode = curNode.next
        while curNode is not None and curNode.index != fromNdx:
            index += 1
            element = VectorElement(curNode.data)
            element.index = index
            newNode.tail.next = element
            curNode = curNode.next
            newNode = newNode.next


        return newVector

    def __iter__(self):
        return VectIterator(self.head)

class VectIterator:
    def __init__(self, head):
        self.curNode = head

    def __iter__(self):
        return self

    def __next__(self):
        if self.curNode is not None:
            element = (self.curNode.index, self.curNode.data)
            self.curNode = self.curNode.next
            return element
        else:
            raise StopIteration



if __name__=="__main__":
    vect1 = LinkedVector()
    vect2 = LinkedVector()
    vect1.append(VectorElement(1))
    vect1.append(VectorElement(2))
    vect1.append(VectorElement(5))
    vect1.append(VectorElement(10))
    vect1.append(VectorElement(100))
    vect1.append(VectorElement(10))
    vect1.append(VectorElement(100))
    vect1.append(VectorElement(10))
    vect1.append(VectorElement(100))
    vect1.insert(0, VectorElement(4))
    vect2.append(VectorElement(4))
    vect2.append(VectorElement(15))
    vect2.append(VectorElement(51))
    vect2.append(VectorElement(533))
    vect2.append(VectorElement(4324))
    print(len(vect1))
    el = vect1.subVector(1,4)
    for d in el:
        print(d)
















        # if curNode is self.head:
        #     if ndx <= curNode.index:
        #         item.next = curNode
        #         self.tail = curNode
        #         self.head = item
        #     else:
        #         self.head.next = item
        #         self.tail = item
        # elif curNode is self.tail:
        #     if ndx <= self.tail:
        #         item.next = curNode
        #         predNode.next = item
        #     else:
        #         curNode.next = item
        #         self.tail = item
        # else:
        #     if
        #
        #
        #







