
#Binary search Algorithm

def binnarysearch(lista_elemente, item):
    low = 0
    high = len(lista_elemente) - 1
    mid = (high + low) // 2
    while low <= high:
        mid = (high + low) // 2
        if lista_elemente[mid] == item:
            if lista_elemente[mid - 1] == item:
                low = 0
                high = mid
            else:
                return mid
        elif item < lista_elemente[mid]:
            high = mid - 1
        else:
            low = mid + 1
    return low
lista_sorted = [1,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,6]
print(lista_sorted.index(3))
print(binnarysearch(lista_sorted, 3))


def bubbleSort(theSeq):
    n = len(theSeq)
    for i in range(n-1):
        for j in range(n-i-1):
            if theSeq[j] > theSeq[j+1]:
                tmp = theSeq[j]
                theSeq[j] = theSeq[j+1]
                theSeq[j+1] = tmp
    return theSeq




def selectionSort(theSeq):
    n = len(theSeq)
    for i in range(n - 1):
        smallNdx = i
        for j in range(i + 1, n):
            if theSeq[smallNdx] > theSeq[j]:
                smallNdx = j

        if smallNdx != i:
            tmp = theSeq[i]
            theSeq[i] = theSeq[smallNdx]
            theSeq[smallNdx] = tmp


def insertionSort(theSeq):
    n = len(theSeq)
    for i in range(1,n):
        value = theSeq[i]
        pos = i
        while pos > 0 and value < theSeq[pos - 1]:
            theSeq[pos] = theSeq[pos-1]
            pos -= 1
        theSeq[pos] = value

    return theSeq


def smallelem(theSeq):
    n = len(theSeq)
    smallndx = 0
    for j in range(n):
        if theSeq[smallndx] > theSeq[j]:
            smallndx = j
    return theSeq[smallndx]


def negative_list(givenList):
    newList = []
    for ele in givenList:
        if ele < 0:
            newList.append(ele)
    return newList

def mergeSortedLists(ListA, ListB):
    newLista = list()
    a = 0
    b = 0
    while a < len(ListA) and b < len(ListB):
        if ListA[a] < ListB[b]:
            newLista.append(ListA[a])
            a+=1
        else:
            newLista.append(ListB[b])
            b +=1
    while a < len(ListA):
        newLista.append(ListA[a])
        a+=1
    while b < len(ListB):
        newLista.append(ListB[b])
        b+=1
    return newLista


listaL = [1,2,3,4,5,6,35,47,60]
listaB = [3,5,33,36,40,100]

print(mergeSortedLists(listaL, listaB))
