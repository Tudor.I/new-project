def nr_of_possible_moves(Array):
    lista_open_squares = []
    nr_of_possible_moves = 0
    list_of_index = [(r - 1, c - 1), (r - 1, c), (r - 1, c + 1), (r, c - 1), (r, c + 1), (r + 1, c - 1),
                     (r + 1, c),
                     (r + 1, c + 1)]
    list_of_index_of_possible_moves = [(r - 2, c - 2), (r - 2, c), (r - 2, c + 2), (r, c - 2), (r, c + 2),
                                       (r + 2, c - 2),
                                       (r + 2, c), (r + 2, c + 2)]
    j = 0
    while j < len(list_of_index_of_possible_moves):
        try:
            enemyCellndx = list_of_index[j]
            possibleMoves = list_of_index_of_possible_moves[j]
            if (self._grid[enemyCellndx[0], enemyCellndx[1]] == ReversiGame.Player_two and
                    self._grid[possibleMoves[0], possibleMoves[1]] is None):
                nr_of_possible_moves += 1
                lista_open_squares.append(self._grid[possibleMoves[0], possibleMoves[1]])
                j += 1
            else:
                j += 1
        except AssertionError:
            j += 1
            continue