from Arrays.Array2D_ADT import Array2D

class ReversiGame:
    Player_one = 1
    Player_two = 2
    offset = ((0,1),(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1))
    def __init__(self):
        self._grid = Array2D(numRows=8, numCols=8)
        self.configure(list(), list())

    def __len__(self):
        return self._grid.numRows() * self._grid.numCols()

    def __getitem__(self, item):
        return self._grid.__getitem__(item)

    def __setitem__(self, ndxTuple, value):
        self._grid.__setitem__(ndxTuple=ndxTuple, value=value)

    def numRows(self):
        return self._grid.numRows()

    def numCols(self):
        return self._grid.numCols()

    def setCell(self, row,col, value):
        if self._grid[row, col] is None:
            self._grid[row, col] = value
        else:
            raise Exception("The cell can't be occupied")



    def configure(self, coordList_player1, coordList_player2):
        self._grid.clear(None)
        i = 0
        k = 0
        if len(coordList_player1) == len(coordList_player2):
            while i < len(coordList_player1) and k < len(coordList_player2):
                coordPl1 = coordList_player1[i]
                coordPl2 = coordList_player2[k]
                self.setCell(coordPl1[0], coordPl1[1], value=ReversiGame.Player_one)
                self.setCell(coordPl2[0], coordPl2[1], value=ReversiGame.Player_two)
                if (self.LineOfAttack(r=coordPl1[0], c=coordPl1[1], player=ReversiGame.Player_one) and
                    self.LineOfAttack(r=coordPl2[0], c=coordPl2[1], player=ReversiGame.Player_two)):
                    i+=1
                    k+=1
                    continue
                else:
                    raise Exception("Wrong configuration. Please, place the chips on the table according to the game's rules!")
        else:
            raise Exception("Both playes need to place the same number of chips on the table!")

    def LineOfAttack(self,r,c,player):
        if player == 1:
            nr_of_possible_moves= 0
            list_of_index = [(r - 1, c - 1), (r - 1, c), (r - 1, c + 1), (r, c - 1), (r, c + 1), (r + 1, c - 1),
                             (r + 1, c),
                             (r + 1, c + 1)]
            list_of_index_of_possible_moves = [(r - 2, c - 2), (r - 2, c), (r - 2, c + 2), (r, c - 2), (r, c + 2),
                                               (r + 2, c - 2),
                                               (r + 2, c), (r + 2, c + 2)]
            j = 0

            if self.isAdjecent(r=r, c=c,player=ReversiGame.Player_one):
                while j < len(list_of_index_of_possible_moves):
                    try:
                        enemyCellndx = list_of_index[j]
                        possibleMoves = list_of_index_of_possible_moves[j]
                        if (self._grid[enemyCellndx[0], enemyCellndx[1]] == ReversiGame.Player_two and
                                (self._grid[possibleMoves[0], possibleMoves[1]] is None or
                                 self._grid[possibleMoves[0], possibleMoves[1]] == ReversiGame.Player_one)):
                                nr_of_possible_moves +=1
                                j+=1
                        else:
                            j+=1
                    except AssertionError:
                        j+=1
                        continue
            if nr_of_possible_moves >=1:
                return True
            else:
                return False
        elif player == 2:
            nr_of_possible_moves= 0
            list_of_index = [(r - 1, c - 1), (r - 1, c), (r - 1, c + 1), (r, c - 1), (r, c + 1), (r + 1, c - 1),
                             (r + 1, c),
                             (r + 1, c + 1)]
            list_of_index_of_possible_moves = [(r - 2, c - 2), (r - 2, c), (r - 2, c + 2), (r, c - 2), (r, c + 2),
                                               (r + 2, c - 2),
                                               (r + 2, c), (r + 2, c + 2)]
            j = 0

            if self.isAdjecent(r=r, c=c,player=ReversiGame.Player_two):
                while j < len(list_of_index_of_possible_moves):
                    try:
                        enemyCellndx = list_of_index[j]
                        possibleMoves = list_of_index_of_possible_moves[j]
                        if (self._grid[enemyCellndx[0], enemyCellndx[1]] == ReversiGame.Player_one and
                                (self._grid[possibleMoves[0], possibleMoves[1]] is None or
                        self._grid[possibleMoves[0], possibleMoves[1]] == ReversiGame.Player_one)):
                                nr_of_possible_moves +=1
                                j+=1
                        else:
                            j+=1
                    except AssertionError:
                        j+=1
                        continue
            if nr_of_possible_moves >=1:
                return True
            else:
                print(nr_of_possible_moves)
                return False
        else:
            raise Exception("Wrong player")


    def isAdjecent(self,r,c,player):
        if player == 1:
            if self.NumberOfEnemyChips(player) >=1:
                return True
            else:
                return False
        elif player == 2:
            if self.NumberOfEnemyChips(player) >= 1:
                return True
            else:
                return False
        else:
            raise Exception("No more than two players")


    def NumberOfEnemyChips(self, player):
        """Returns the number of enemy chips"""
        list_of_index = [(r - 1, c - 1), (r - 1, c), (r - 1, c + 1), (r, c - 1), (r, c + 1), (r + 1, c - 1),
                         (r + 1, c),
                         (r + 1, c + 1)]
        if player == 1:
            nr_of_neighbours = 0
            for offset in self.offset:
                j = 0
                while j < len(list_of_index):
                    idx = list_of_index[j]
                    try:
                        if self._grid[idx[0], idx[1]] == 2:
                            nr_of_neighbours +=1
                            j+=1
                        else:
                            j+=1
                    except AssertionError:
                        j+=1
                        continue
                return nr_of_neighbours
        elif player == 2:
            nr_of_neighbours = 0

            j = 0
            while j < len(list_of_index):
                idx = list_of_index[j]
                try:
                    if self._grid[idx[0], idx[1]] == 1:
                        nr_of_neighbours += 1
                        j += 1
                    else:
                        j += 1
                except AssertionError:
                    j += 1
                    continue
            return nr_of_neighbours
        else:
            raise Exception("No more than two players")

    def numOfChips(self, player):
        number_of_chips = 0
        assert player == ReversiGame.Player_one or player == ReversiGame.Player_two, "There can be only two players"
        for i in range(self.numRows()):
            for j in range(self.numCols()):
                if self._grid[i,j] == player:
                    number_of_chips +=1
        return number_of_chips

    def numOpenSquares(self, r, c):
        assert self._grid[r, c] == ReversiGame.Player_one or \
               self._grid[r, c] == ReversiGame.Player_two, 'There is no player at the given index'
        lista_open_squares = []
        nr_of_possible_moves_of_player_1 = 0
        nr_of_possible_moves_of_player_2 = 0
        list_of_index = [(r - 1, c - 1), (r - 1, c), (r - 1, c + 1), (r, c - 1), (r, c + 1), (r + 1, c - 1),
                         (r + 1, c),
                         (r + 1, c + 1)]
        list_of_index_of_possible_moves = [(r - 2, c - 2), (r - 2, c), (r - 2, c + 2), (r, c - 2), (r, c + 2),
                                           (r + 2, c - 2),
                                           (r + 2, c), (r + 2, c + 2)]
        j = 0
        if self._grid[r, c] == ReversiGame.Player_one:
            while j < len(list_of_index_of_possible_moves):
                try:
                    enemyCellndx = list_of_index[j]
                    possibleMoves = list_of_index_of_possible_moves[j]
                    if (self._grid[enemyCellndx[0], enemyCellndx[1]] == ReversiGame.Player_two and
                            self._grid[possibleMoves[0], possibleMoves[1]] is None):
                        nr_of_possible_moves_of_player_1 += 1
                        lista_open_squares.append(self._grid[possibleMoves[0], possibleMoves[1]])
                        j += 1
                    else:
                        j += 1
                except AssertionError:
                    j += 1
                    continue
            return nr_of_possible_moves_of_player_1
        else:
            while j < len(list_of_index_of_possible_moves):
                try:
                    enemyCellndx = list_of_index[j]
                    possibleMoves = list_of_index_of_possible_moves[j]
                    if (self._grid[enemyCellndx[0], enemyCellndx[1]] == ReversiGame.Player_one and
                            self._grid[possibleMoves[0], possibleMoves[1]] is None):
                        nr_of_possible_moves_of_player_2 +=1
                        j += 1
                    else:
                        j += 1
                except AssertionError:
                    j += 1
                    continue
            return nr_of_possible_moves_of_player_2

    def whoseTurn(self, player):
        if self.Player_one == player:
            return 1
        elif  self.Player_two == player:
            return 2
        else:
            return 0


    def getWiner(self):
        nr_of_player1_chips = self.numOfChips(1)
        nr_of_player2_chips = self.numOfChips(2)
        if nr_of_player1_chips < nr_of_player2_chips:
            return f'Player 2 is winner!'
        elif nr_of_player1_chips > nr_of_player2_chips:
            return f'Player 1 is winner!'
        else:
            return f'There is equality between the two players'

    def isLegalMove(self, row, col, player):
        if self.whoseTurn(player) == 1 and self.isOccupiedBy(row, col) == 0:
            self._grid[row, col] = 1
            if self.isAdjecent(row, col, 1):
                return True
            else:
                return False
        elif self.whoseTurn(player) == 2 and self.isOccupiedBy(row, col) == 0:
            self._grid[row, col] = 2
            if self.isAdjecent(row, col, 2):
                return True
            else:
                return False
        else:
            return "You can't select a box which is already occupied!"

    def isOccupiedBy(self, row, col):
        assert row >= 0 and row <self.numRows(), "Subscript out of range"
        assert col >=0 and row <self.numCols(), "Subscript out of range"

        if self._grid[row, col] == ReversiGame.Player_one:
            return 1
        elif self._grid[row, col] == ReversiGame.Player_two:
            return 2
        else:
            return 0

def draw(grid):
    representation = ""
    for i in range(grid.numRows()):
        for j in range(grid.numCols()):
            if grid[i,j] == 1:
                representation += "1"
            elif grid[i,j] == 2:
                representation += "2"
            else:
                representation += "."
    substrings = [substring[:grid.numCols()] for substring in representation]
    i = 0
    x = ''
    lista_sub_string = []
    while i < len(representation):
        x += substrings[i]
        if len(x) % 8 == 0:
            print(x)
            lista_sub_string.append(x)
            x = ''
        i += 1
    return lista_sub_string


if __name__ == "__main__":
    x = ReversiGame()
    player1 = [(3,3), (4,4)]
    player2 = [(4,3), (3,4)]
    x.configure(coordList_player1=player1, coordList_player2=player2)
    draw(x)
    print(x.numOpenSquares(r=3,c=3))
    print(x.getWiner())
    print(x.whoseTurn(5))
    print(x.isLegalMove(3,5, player=1))




