from Arrays.Array2D_ADT import Array2D

class ReversiGame:
    Player_one = 1
    Player_two = 2
    offsets = ((0,1),(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1))
    def __init__(self):
        self._grid = Array2D(numRows=8, numCols=8)
        self.configure()

    def __len__(self):
        return self._grid.numRows() * self._grid.numCols()

    def __getitem__(self, item):
        return self._grid.__getitem__(item)

    def __setitem__(self, ndxTuple, value):
        self._grid.__setitem__(ndxTuple=ndxTuple, value=value)

    def numRows(self):
        return self._grid.numRows()

    def numCols(self):
        return self._grid.numCols()

    def setCell(self, row,col, value):
        if self._grid[row, col] is None:
            self._grid[row, col] = value
        else:
            raise Exception("The cell can't be occupied")

    def configure(self):
        self._grid.clear(None)
        white = [(self.numRows() // 2 - 1, self.numCols()//2 - 1), (self.numRows() // 2 + 1 - 1, self.numCols() // 2 + 1 - 1)]
        black = [(self.numRows() // 2 + 1 - 1, self.numCols() // 2 - 1), (self.numRows() // 2 - 1, self.numCols() // 2 + 1 - 1)]
        for piece in white:
            self._grid[piece[0], piece[1]] = self.Player_one
        for piece in black:
            self._grid[piece[0], piece[1]] = self.Player_two


    def whoseTurn(self, player):
        if player == 1:
            return 1
        elif  player == 2:
            return 2
        else:
            return 0

    def numOfChips(self, player):
        number_of_chips = 0
        assert player == ReversiGame.Player_one or player == ReversiGame.Player_two, "There can be only two players"
        for i in range(self.numRows()):
            for j in range(self.numCols()):
                if self._grid[i,j] == player:
                    number_of_chips +=1
        return number_of_chips

    def getWiner(self):
        nr_of_player1_chips = self.numOfChips(1)
        nr_of_player2_chips = self.numOfChips(2)
        if nr_of_player1_chips < nr_of_player2_chips:
            return f'Player 2 is the winner!'
        elif nr_of_player1_chips > nr_of_player2_chips:
            return f'Player 1 is the winner!'
        else:
            return f'There is equality between the two players'


    def numbOfOpenSquares(self):
        nr = 0
        for i in range(self.numRows()):
            for j in range(self.numCols()):
                if self._grid[i,j] == None:
                    nr +=1
        return nr


    def inverse(self, player):
        return 1 if player == 2 else 2


    def isLegalMove(self,row, col, player):
        if self._grid[row, col] is not None:
            return False
        for offset in self.offsets:
            check = [row + offset[0], col + offset[1]]
            while (0 <= check[0] < self.numRows() and 0 <= check[1] < self.numCols() and
                   self._grid[check[0], check[1]] == self.inverse(player)):
                check[0] += offset[0]
                check[1] += offset[1]
                try:
                    if self._grid[check[0], check[1]] == player:
                        return True
                except AssertionError:
                    continue
        return False

    def makeMove(self, row, col, player):
        self._grid[row, col] = player
        for offset in self.offsets:
            check = [row + offset[0], col + offset[1]]
            while 0<=check[0]<self.numRows() and 0<=check[1]<self.numCols():
                try:
                    if self._grid[check[0],check[1]] is None: break
                    if self._grid[check[0],check[1]] is player:
                        self.flip(player,row,col, offset)
                        break
                    check[0] += offset[0]
                    check[1] += offset[1]
                except AssertionError:
                    check[0] += offset[0]
                    check[1] += offset[1]
                    continue
        return

    def flip(self, player, row, col, offset):
        check = [row + offset[0], col + offset[1]]
        while(self._grid[check[0],check[1]] is self.inverse(player)):
            self._grid[check[0],check[1]] = player
            check[0] += offset[0]
            check[1] += offset[1]
        return

    def has_valid_move(self, player):
        for y in range(self.numRows()):
            for x in range(self.numCols()):
                if self.isLegalMove(row=y, col=x, player=player):
                    return True
        return False



    def occupiedBy(self,row, col):
        if self._grid[row, col] == 1:
            return 1
        elif self._grid[row,col] == 2:
            return 2
        else:
            return None

    def game(self, player, row, col):
        while True:
            try:
                print(f"It's {player} player turn!")
                row = int(input("Insert the Row: "))
                col = int(input("Insert the Col: "))
                if self.isLegalMove(row, col, player):
                    self.makeMove(row, col, player)
                    draw(self)
                    return
                else:
                    raise AssertionError
            except(ValueError, IndexError, TypeError, SyntaxError, AssertionError):
                print("Invalid move.Try again!")

    def game_played_once(self, player, row, col):
        draw(self._grid)
        while True:
            try:
                print(f"It's {player} player turn!")
                row = int(input("Insert the Row: "))
                col = int(input("Insert the Col: "))
                if self.isLegalMove(row, col, player):
                    self.makeMove(row, col, player)
                    draw(self)
                    return
                else:
                    raise AssertionError
            except(ValueError, IndexError, TypeError, SyntaxError, AssertionError):
                print("Invalid move.Try again!")
def draw(grid):
    representation = ""
    for i in range(grid.numRows()):
        for j in range(grid.numCols()):
            if grid[i,j] == 1:
                representation += "1"
            elif grid[i,j] == 2:
                representation += "2"
            else:
                representation += "0"
    substrings = [substring[:grid.numCols()] for substring in representation]
    i = 0
    x = ''
    lista_sub_string = []
    while i < len(representation):
        x += substrings[i]
        if len(x) % 8 == 0:
            print(x)
            lista_sub_string.append(x)
            x = ''
        i += 1
    return lista_sub_string


def main():
    print("Welcome to Reversi Game! Let's start the game!")
    game = ReversiGame()
    draw(game)
    player = int(input("Insert the number of the player who starts first! "))
    if player not in [1, 2]:
        while player not in [1,2]:
            player = int(input("Wrong player.\nPlayer number two starts first. Please insert number two"))
    row = int(input("Insert the Row: "))
    col = int(input("Insert the Col: "))
    flag = True
    while game.has_valid_move(player):
        if flag:
            while True:
                try:
                    print(f"The current player: {player}")
                    if game.isLegalMove(row, col, player):
                        game.makeMove(row, col, player)
                        player = game.inverse(player)
                        draw(game)
                        flag = False
                        break
                    else:
                        raise AssertionError
                except:
                    print("Invalid move.Try again!")
                    row = int(input("Insert the Row: "))
                    col = int(input("Insert the Col: "))

        else:
            game.game(player = player, row = row, col = col)
            if game.has_valid_move(game.inverse(player)):
                player = game.inverse(player)
            quit = input("Do you want to exit? If yes, press 'q' button!")
            if quit.lower() == 'q':
                break

    winner = game.getWiner()
    print(winner)


if __name__ == "__main__":
    main()









