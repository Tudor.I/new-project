from Rance_Necaise_Algorithms_and_data_structures.Arrays.Array2D_ADT import Array2D

class LifeGrid:
    #defines constants to represent the cell states
    DEAD_CELL = 0
    LIVE_CELL = 1
    def __init__(self, numRows, numCols):
        # We creat de grid
        self._grid = Array2D(numRows, numCols)
        self.configure(list())

    def numRows(self):
        # return the number of Rows of the Grid
        return self._grid.numRows()

    def numCols(self):
        # return the number of columns of the grid
        return self._grid.numCols()
    def clearCell(self, row, col):
        self._grid[row, col] = LifeGrid.DEAD_CELL

    def setCell(self,row,col):
        self._grid[row, col] = LifeGrid.LIVE_CELL

    def configure(self, coordList):
        for i in range(self.numRows()):
            for j in range(self.numCols()):
                self.clearCell(i, j)

        for coord in coordList:
            self.setCell(coord[0], coord[1])


    def isLiveCell(self, row, col):
        return self._grid[row, col] == LifeGrid.LIVE_CELL

    def numberNeighbours(self, r, c):
        nr_of_neighbours = 0
        list_of_index = [(r-1, c-1), (r-1, c), (r-1, c+1),(r, c-1), (r, c+1), (r+1,c-1), (r+1, c), (r+1, c+1)]
        j = 0
        while j<len(list_of_index):
            idx = list_of_index[j]
            try:
                if self.isLiveCell(idx[0], idx[1]):
                    nr_of_neighbours +=1
                    j+=1
                else:
                    j+=1
            except AssertionError:
                j+=1
                continue
        return  nr_of_neighbours







