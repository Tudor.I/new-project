from Rance_Necaise_Algorithms_and_data_structures.LifeGridGame.thelifegridgame import LifeGrid

def init_config():
    lista_coord = []
    while True:
        first_nr = int(input("Introduce the row: "))
        sec_nr = int(input("Introduce the column: "))
        lista_coord.append((first_nr,sec_nr))
        ask = input("Do you want to introduce more coordonites (You need to insert minimum)'\n'Press q key for exit")
        if ask == 'q':
            return lista_coord
        else:
            continue

def main():
    grid_width = int(input("Insert the width:"))
    grid_height = int(input("Insert the height: "))
    num_gens = int(input("Insert the numb of generations: "))
    config = init_config()
    grid1 = LifeGrid(grid_width, grid_height)
    grid1.configure(config)
    with open("file_csv.txt", 'w') as f:
        u = draw(grid1)
        for i in u:
            z = i,'\n'
            f.writelines(z)
        for i in range(num_gens):
            evolve(grid1)
            print('\n')
            z = draw(grid1)
            for m in z:
                i = m,'\n'
                f.writelines(i)
def evolve(grid):
    liveCells = list()
    for i in range(grid.numRows()):
        for j in range(grid.numCols()):
            neighbors = grid.numberNeighbours(i,j)

            if (neighbors == 2 and grid.isLiveCell(i,j)) or (neighbors == 3):
                liveCells.append((i,j))


    grid.configure(liveCells)

def draw(grid):
    representation = ""
    for i in range(grid.numRows()):
        for j in range(grid.numCols()):
            if grid.isLiveCell(i,j):
                representation  += "@"
            else:
                representation += '.'
    substrings = [substring[:grid.numCols()] for substring in representation]
    i = 1
    x = ''
    lista_sub_string = []
    while i <= len(representation):
        x += substrings[i-1]
        if len(x) % 5 == 0:
            print(x)
            lista_sub_string.append(x)
            x = ''
        i += 1
    return lista_sub_string

if __name__ == "__main__":
    main()


