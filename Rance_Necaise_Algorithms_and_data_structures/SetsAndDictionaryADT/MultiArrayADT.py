from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array
class MultiArray:
    def __init__(self, *dimensions):
        assert len(dimensions) > 1, "The array must have 2 or more dimensions"
        self._dims = dimensions
        # The total number of elements in the Array
        size = 1
        for d in dimensions:
            assert d>0, "Dimensions must be > 0 "
            size *= d
        self._elements = Array(size)
        self._factors = Array(len(dimensions))
        self._computeFactors()

    #number of dimensions
    def numDims(self):
        return len(self._dims)

    def length(self, dim):
        assert dim >= 1 and dim <=len(self._dims), "Dimension component out of range"
        return self._dims[dim - 1]

    def clear(self, value):
        self._elements.clear(value)

    def __getitem__(self, ndxTuple):
        assert len(ndxTuple) == self.numDims(), 'Invalid # of array subscripts'
        index = self._computeIndex(ndxTuple)
        assert index is not None, "Array subscript out of range"
        return self._elements[index]

    def __setitem__(self, ndxTuple, value):
        assert len(ndxTuple) == self.numDims(), "Invalid # of array subscript"
        index = self._computeIndex(ndxTuple)
        assert index is not None, 'Array subscript out of range'
        self._elements[index] = value

    def _computeIndex(self, idx):
        offset = 0
        for j in range(len(idx)):

            if idx[j] < 0 or idx[j] >= self._dims[j]:
                raise IndexError("Index out of range")
            else:
                offset += idx[j] * self._factors[j]
        return offset

    def _computeFactors(self):
        factors = 1
        for i in range(len(self._factors)):
            for j in range(len(self._dims)-1):
                try:
                    j = i+j+1
                    factors *= self._dims[j]
                except IndexError:
                    pass
            self._factors[i] = factors
            factors = 1
        return self._factors



