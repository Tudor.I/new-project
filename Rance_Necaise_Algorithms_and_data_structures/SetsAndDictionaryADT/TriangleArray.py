from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array

class TriangleArray:
    def __init__(self, size):
        assert size > 1, "The size should be greater than 1"
        self._size = size
        array_size = 0
        for i in range(self._size + 1):
            array_size += i
        self._elements = Array(array_size)

    def _computeIndex(self, ndxTuple):
        """Compute de index in the 1D array from the given index tuple in 2D"""
        # trebuie sa veriicam ca j-ul nu este out of range
        assert len(ndxTuple), "Wrong index tuple"
        sum = 0
        size = 0
        for i in range(ndxTuple[0]+1):
            sum += i
        size_of_column = ndxTuple[0] + 1
        offset = sum + ndxTuple[1] # e nevoie sa se verifice ca j-ul se afla in range ul sau
        assert ndxTuple[1]>=0 and ndxTuple[1] < size_of_column, 'Index out of range'
        assert offset >= 0 and offset < len(self._elements), "Index out of range"
        return offset

    def __len__(self):
        return len(self._elements)

    def __getitem__(self, ndxTuple):
        index = self._computeIndex(ndxTuple)
        return self._elements[index]

    def __setitem__(self, ndxtuple, value):
        index = self._computeIndex(ndxtuple)
        self._elements[index] = value


if __name__ == "__main__":
    triangle = TriangleArray(5)
    print(len(triangle))
    print(triangle._computeIndex((3,1)))
    print(triangle._computeIndex((3,4)))
    # triangle[3,1] = 1
    # print(triangle[3,1])





