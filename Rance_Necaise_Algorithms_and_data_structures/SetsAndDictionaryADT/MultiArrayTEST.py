from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.MultiArrayADT import MultiArray

import unittest

class TestMultiArray(unittest.TestCase):
    def setUp(self):
        self.matrix5D = MultiArray(5,5,5,5,5)

    def test_num_of_dimensions(self):
        self.assertEqual(self.matrix5D.numDims(), 5)

    def test_length_of_a_dimension(self):
        self.assertEqual(self.matrix5D.length(dim=5), 5)

    def test_compute_index(self):
        self.assertEqual(self.matrix5D._computeIndex((5,3,4,2,1)), 3611)

    def test_factor(self):
        self.assertEqual(self.matrix5D._factors[0], 625)


