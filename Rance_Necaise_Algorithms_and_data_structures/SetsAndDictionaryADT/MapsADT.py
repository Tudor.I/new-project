from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array
class Map:
    def __init__(self):
        self._entryList = list()

    def __len__(self):
        return len(self._entryList)

    def __contains__(self, key):
        ndx = self._findPosition(key)
        return ndx is not None

    def __getitem__(self, ndx):
        assert ndx >=0 and ndx < len(self), "Index out of range"
        return self._entryList[ndx]

    def setItem(self, key, value):
        ndx = self._findPosition(key)
        if ndx is None:
            raise ValueError("The key does not exist.")
        else:
            self._entryList[ndx].value = value

    def add(self,key,value):
        ndx = self._findPosition(key)
        if ndx is not None:
            pass
        else:
            entry = _MapEntry(key, value)
            self._entryList.append(entry)
        self._entryList = sorted(self._entryList)


    def valueOf(self, key):
        ndx = self._findPosition(key)
        assert ndx is not None, 'Invalid map key'
        return self._entryList[ndx].value

    def remove(self,key):
        ndx = self._findPosition(key)
        assert ndx is not None, 'Invalid map key'
        self._entryList.pop(ndx)

    def __iter__(self):
        return _MapIterator(self._entryList)

    def _findPosition(self, key):
        """for i in range(len(self._entryList)):
            if self._entryList[i].key == key:
                return i
        return None

        Varianta de cautare a pozitiei unui element in complexitate O(n)"""


        """Varianta de cautare a pozitiei unui element in complexitate O(logn) utilizand binary search"""
        low = 0
        high = len(self._entryList)
        mid = (high + low) // 2
        while low < high:
            mid = (high +low)//2
            if self._entryList[mid].key == key:
                return mid
            elif self._entryList[mid].key > key:
                high = mid - 1
            else:
                low = mid + 1
        return None

    def keyArray(self):
        array_of_keys = Array(len(self))
        i = 0
        for element in self._entryList:
            array_of_keys[i] = element.key
            i+=1
        string = ''
        j = 0
        for key in array_of_keys:
            if j == len(array_of_keys) - 1:
                string += str(key)
            else:
                string += str(key)+', '
        return f"{{{string}}}"




class _MapEntry:
    def __init__(self, key, value):
        assert isinstance(key, int), "The key must be integer"
        self.key = key
        self.value = value

    def __gt__(self, otherMap):
        if self.key > otherMap.key:
            return self

    def __lt__(self, otherMap):
        if self.key < otherMap.key:
            return otherMap

class _MapIterator:
    def __init__(self, Map):
        self.map = Map
        self.curndx = 0

    def __int__(self):
        return self

    def __next__(self):
        if self.curndx < len(self.map):
            entry = self.map[self.curndx]
            self.curndx += 1
            return entry
        else:
            raise StopIteration

# Add a new operation keyArray() to the Map Class that returns an array containing
#all of the keys stored in the map. The array of keys should be in no particualr ordering
if __name__ == "__main__":
    n = _MapEntry(231, "PROST")
    m = _MapEntry(42, "BOU")
    ma = Map()
    ma.add(n.key, n.value)
    ma.add(m.key, m.value)
    ma.add(533, "ALEX")


    print(ma._findPosition(533))