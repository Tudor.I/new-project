
import random
import csv

# def get_nume_pren(nume, prenume):
#     n = nume[random.randint(0, len(nume)-1)]
#     p = prenume[random.randint(0, len(prenume)-1)]
#     return n, p


prima_luna = []
items = []
index = 0
luni = ['Ianuarie', 'Februarie', 'Martie', "Aprilie", 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie']
for item in range(100):
    for luna in range(12):
        valoare = random.randint(100, 5000)
        prima_luna.append(valoare)
    prima_luna = tuple(prima_luna)
    items.append(prima_luna)
    prima_luna = list()
with open("items8store.csv", "w+", newline='') as csvfile:
    n = [dict(zip(('Ianuarie', 'Februarie', 'Martie', "Aprilie", 'Mai', 'Iunie', 'Iulie',
                   'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie'), el)) for el in items]
    writer = csv.DictWriter(csvfile, fieldnames=luni)
    writer.writeheader()
    writer.writerows(n)

# with open("studenti.csv", "w+") as csvfile:
#     n = [dict(zip(("Index", "Prenume", "Nume", "Clasa"), el)) for el in studenti]
#     writer = csv.DictWriter(csvfile, fieldnames=["Index", "Prenume", "Nume", "Clasa"])
#     writer.writeheader()
#     writer.writerows(n)