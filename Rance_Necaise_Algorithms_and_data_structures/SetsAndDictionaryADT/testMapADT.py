from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.MapsADT import Map, _MapEntry
import unittest

class TestMapCase(unittest.TestCase):
    def setUp(self):
        self.map = Map()

        self.map.add(key='id', value='1')
        self.map.add(key='prenume', value='andrei')
        self.map.add(key='nume', value='1')
        self.map.add(key='rez', value='1')

    def test_get_item(self):
        self.assertEqual(self.map[1].key, 'prenume')
        self.assertTrue(isinstance(self.map[2], _MapEntry))

    def test_Set_item(self):
        self.map.setItem('prenume', 'marius')
        self.assertEqual(self.map[1].key, 'prenume')

    def test_add(self):
        self.map.add(2, [100,200,300,400,500])
        self.assertEqual(self.map[4].value,[100,200,300,400,500])