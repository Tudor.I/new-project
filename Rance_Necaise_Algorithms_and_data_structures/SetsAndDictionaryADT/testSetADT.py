import unittest
from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.SetADT import SetADT
class TestCaseSetADT(unittest.TestCase):
    def setUp(self):
        self.set1 = SetADT()
        self.set2 = SetADT()
        for i in range(10):
            self.set1.add(i)
        for i in range(13):
            if i > 3:
                self.set2.add(i)

    def test_add(self):
        self.set1.add(10)
        self.assertEqual(len(self.set1), 11)
        self.assertEqual(self.set1[10], 10)

    def test_remove(self):
        self.set2.remove(4)
        self.assertEqual(len(self.set2), 8)

    def test_intersect(self):

        self.assertEqual(len(self.set1.intersect(self.set2)), 6)

    def test_difference(self):
        self.assertEqual(self.set1.difference(self.set2)[3], 3)

    def test_equal(self):
        self.assertFalse(self.set1 == self.set2)

