class SetADT:
    def __init__(self, *initialElements):
        self._elements = list()
        self._arg = sorted(initialElements)
        if len(self._arg) != 0:
            for elem in self._arg:
                if elem not in self._elements:
                    self._elements.append(elem)


    def __len__(self):
        return len(self._elements)

    def __contains__(self, element):
        ndx = self.find_posotion(element)
        return ndx < len(self) and self._elements[ndx] == element


    def __getitem__(self, ndx):
        assert ndx >= 0 and ndx < len(self), "Index out of range"
        return self._elements[ndx]


    def add(self, element):
        if element not in self._elements:
            ndx = self.find_posotion(element)
            self._elements.insert(ndx, element)

    def remove(self, element):
        assert element in self._elements, "Element must be in Set"
        ndx = self.find_posotion(element)
        self._elements.pop(ndx)

    def __eq__(self, otherSet):
        if len(self._elements) != len(otherSet):
            return False
        else:
            for i in range(len(self)):
                if self._elements[i] != otherSet._elements[i]:
                    return False
            return True

    def isSubsetof(self, setB):
        # time complexity quadratic
        nr = 0
        a = 0
        b = 0
        while a < len(self) and b < len(setB):
            if self._elements[a] == setB._elements[b]:
                a+=1
                b=0
                nr+=1
            else:
                b+=1
        if nr == len(self):
            return True
        else:
            return False


    def issubsetof(self, setB):
        #time complexity linear PS: This works only with sorted sets
        nr = 0
        a = 0
        b = 0
        while a < len(self) and b < len(setB):
            if self._elements[a] < setB._elements[b]:
                a +=1
            elif self._elements[a] > setB._elements[b]:
                b+=1
            else:
                nr+=1
                a+=1
                b+=1
        if nr == len(self):
            return True
        else:
            return False

    def isSubsetOf(self, setB):
        #time complexity quadratic
        for element in self._elements:
            if element not in setB:
                return False
        return True

    def intersect(self, setB):
        """Computes the intersection of two sets in linear time merging sorted lists"""
        a = 0
        b = 0
        newSet = SetADT()
        while a < len(self) and b <len(setB):
            if self._elements[a] < setB._elements[b]:
                a+=1
            elif self._elements[a] > setB._elements[b]:
                b+=1
            else:
                newSet._elements.append(self._elements[a])
                a+=1
                b+=1
        return newSet



    def __add__(self, setB):
        newSet = SetADT()
        newSet._elements.extend(self._elements)
        for element in setB:
            if element not in self._elements:
                newSet.add(element)

        return newSet

    def __mul__(self, setB):
        """computes the intersection of two sets in quadratic time"""
        newSet = SetADT()
        newSet._elements.extend(self._elements)
        for element in self._elements:
            if element in setB:
                pass
            else:
                newSet.remove(element)
        return newSet

    def __sub__(self, setB):
        """Computes the difference between two sets in quadratic time complxity
        """
        newSet = SetADT()
        newSet._elements.extend(self._elements)
        for element in self._elements:
            if element not in setB:
                pass
            else:
                newSet.remove(element)
        return newSet

    def difference(self, setB):
        """Computes the difference between two sets in linear time complexity"""
        a = 0
        b = 0
        newSet = SetADT()
        while a < len(self) and b < len(setB):
            if self._elements[a] < setB._elements[b] and self._elements[a] != setB._elements[b]:
                newSet._elements.append(self._elements[a])
                a += 1
            elif self._elements[a] > setB._elements[b]:
                b += 1
            else:
                a+=1
                b+=1
        while a <len(self):
            newSet._elements.append(self._elements[a])
            a+=1
        return newSet


    def __iter__(self):
        return _SetIterator(self._elements)

    def properSubset(self, SetB):
        if self.isSubsetOf(SetB) and len(self) != len(SetB):
            return True
        else:
            return False

    def __str__(self):
        elemente = ''
        i  = 0
        for element in self._elements:
            if i != len(self._elements) - 1:
                elemente += str(element) + ', '
                i+=1
            else:
                elemente += str(element)
        return f"{{{elemente}}}"

    def find_posotion(self, element):
        """search for a given element using binary search algorithm"""
        high = len(self._elements) - 1
        low = 0
        while low <= high:
            mid = (high + low) // 2
            if self._elements[mid] == element:
                return mid
            elif element < self._elements[mid]:
                high = mid - 1
            else:
                low = mid + 1
        return low

    def union(self, setB):
        """computes the union of two sorted sets in linear time complexity"""
        newSet = SetADT()
        a = 0
        b = 0
        while a < len(self._elements) and b < len(setB):
            valueA = self._elements[a]
            valueB = self._elements[b]
            if valueA < valueB:
                newSet._elements.append(valueA)
                a +=1
            elif valueA > valueB:
                newSet._elements.append(valueB)
                b +=1
            else:
                newSet._elements.append(valueA)
                a +=1
                b+=1
        while a < len(self):
            newSet._elements.append(self._elements[a])
            a+=1

        while b < len(setB):
            newSet._elements.append(setB[b])
            b+=1

        return newSet


class _SetIterator:
    def __init__(self, Set):
        self._set = Set
        self.curNdx = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.curNdx < len(self._set):
            entry = self._set[self.curNdx]
            self.curNdx +=1
            return entry
        else:
            raise StopIteration




if __name__ == "__main__":
    set = SetADT(6,3,5,3,4,5,6,9,10,11,12,15,248,324,23,5,1,5,21,5,53,2)
    setb = SetADT(5,1,53,30 ,2,300, 76,8,5,4,8,9,9,89,9,4,56,654,235,7,567,435,765,234,236,236,654,7,4)
    # print(setb.isSubsetOf(set))
    # print(setb.isSubsetof(set))
    # print(setb.issubsetof(set))
    print(set)
    print(setb)
    #
    # print(set * setb)
    # print(setb.intersect(set))
    print(setb - set)
    print(setb.difference(set))
    # set.add(1)
    # print(set)
    # set1 = SetADT()
    # print(set1)
