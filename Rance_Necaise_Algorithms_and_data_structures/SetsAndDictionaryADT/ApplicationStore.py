from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.MultiArrayADT import MultiArray
from Rance_Necaise_Algorithms_and_data_structures.SetsAndDictionaryADT.ArrayADT import Array
import csv

# The total sales of all items for all months in a given store
def adaugare_date_in_sales_data(fisier, store, SalesData):
    with open(fisier) as f:
        readercsv = csv.reader(f)
        lista = list(readercsv)
        lista.pop(0)
    if store == 1:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s,item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 2:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 3:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 4:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 5:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 6:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData
    elif store == 7:
        s = store - 1
        for item in range(100):
            for luna in range(12):
                SalesData[s, item, luna] = float(lista[item][luna])
        return SalesData

def totalSalesByStore(saleData, store):
    s = store - 1 #The store indices are 0-7, so always we need to substract one
    total = 0.0
    for i in range(saleData.length(2)):
        for m in range(saleData.length(3)):
            total += saleData[s,i,m]
    return total

#Total sales of all items in all stores for a given month
def totalSalesForAMonth(saleData, month):
    m = month - 1
    total = 0.0
    for s in range(saleData.length(1)):
        for i in range(saleData.length(2)):
            total += saleData[s,i,m]
    return total

def totalSalesByOneItem(saleData, item):
    i = item - 1
    total = 0.0
    for s in range(saleData.length(1)):
        for m in range(saleData.length(3)):
            if s < 7:
                total += saleData[s, i, m]

    return total

def totalMonthlySalesfor12monthsgivenstore(saleData, store):
    s = store-1
    total = Array(12)
    sum = 0.0
    for m in range(saleData.length(3)):
        for i in range(saleData.length(2)):
            sum+=saleData[s,i,m]
        total[m] = sum
    return total

def totalSalesForAllStores(saleData):
    total_of_sales_for_all_stores = list()
    for i in range(8):
        sale_per_store = totalSalesByStore(saleData, i+1)
        total_of_sales_for_all_stores.append(sale_per_store)
    return sorted(total_of_sales_for_all_stores, reverse=True)

def totalSalesForEachItemFromAllStores(saleData):
    items = []
    for i in range(100):
        i += 1
        item_sale = totalSalesByOneItem(saleData, i)
        items.append((i, item_sale))
        i -= 1
    return items




saleData = MultiArray(8, 100, 12)
new_sales1 = adaugare_date_in_sales_data(fisier='items1store.csv', store=1, SalesData=saleData)
new_sales2 = adaugare_date_in_sales_data(fisier='items2store.csv', store=2, SalesData=new_sales1)
new_sales3 = adaugare_date_in_sales_data(fisier='items3store.csv', store=3, SalesData=new_sales2)
new_sales4 = adaugare_date_in_sales_data(fisier='items4store.csv', store=4, SalesData=new_sales3)
new_sales5 = adaugare_date_in_sales_data(fisier='items5store.csv', store=5, SalesData=new_sales4)
new_sales6 = adaugare_date_in_sales_data(fisier='items6store.csv', store=6, SalesData=new_sales5)
salesData = adaugare_date_in_sales_data(fisier='items7store.csv', store=7, SalesData=new_sales6)


def main():
    while True:
        print("Welcome to the Gloria Program.")
        print("As it follows, you can choose what kind of report would you like to generate")
        print("Available reports:")
        reports = ["Total Sales By Store", "Total Sales by Month", "Total Sales by Item",
                   "Monthly Sales by Store", "The total sales for all stores",
                   "The total sales for each item sorted by item number"]
        for report in enumerate(reports, 1):
            print(f"{report[0]}:  " + report[1])
        try:
            user_input = int(input("What report would you like to produce?(type 1,2,3..6): "))
            if user_input not in [1,2,3,4,5,6]:
                print("Invalid input. Please try again")
                continue
        except ValueError:
            print("Invalid input.Please try again")
            continue
        if user_input == 1:
            while True:
                print("To produce this report, we would like to ask you to what store\n"
                      "you would like compute the total sales?  ")
                try:
                    store = int(input("Insert your answer here (1,2,3..7): "))
                    if store not in [1,2,3,4,5,6,7]:
                        print("Invalid input. Try again")
                        continue
                except ValueError:
                    print("Invalid input. Try again")
                    continue
                report = totalSalesByStore(salesData, store)
                print(f"The total sales by the {store} has a vlue of: {report} dollars!")
                break

        exit=input("Do you want to generate another report?")
        if exit.strip().lower() == "no":
            break


if __name__ == "__main__":

    # print(saleData[4,99,3])
    # print(totalSalesByStore(salesData, 3))
    # for m in totalSalesForEachItemSortedByItemNumber(salesData):
    #     print(m)
    main()

