import math

class PointADT:
    def __init__(self, x, y):
        self._xCoord = x
        self._yCoord = y

    def getX(self):
        return self._xCoord

    def getY(self):
        return self._yCoord


    def shift(self, xInc, yInc):
        """As the object is immutable, we use shift() function in order to
                        change the initial state of the object"""
        self._xCoord += xInc
        self._yCoord += yInc

    def distance(self, otherPoint):
        """We will further implement the Euclidean distance between two instances
                                 of the Point class"""

        xDiff = self._xCoord - otherPoint.xCoord
        yDiff = self._yCoord - otherPoint.yCoord
        return math.sqrt(xDiff ** 2 + yDiff ** 2)

    def slope(self, otherPoint):
        """Returns the slope of a segment"""

        delta_X = otherPoint._xCoord - self._xCoord
        delta_Y = otherPoint._yCoord - self._yCoord
        return delta_Y / delta_X

