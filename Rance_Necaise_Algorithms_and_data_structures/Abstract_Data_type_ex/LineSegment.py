from Rance_Necaise_Algorithms_and_data_structures.Abstract_Data_type_ex.Point import PointADT
import math
import matplotlib.pyplot as plt
import numpy as np
import shapely
from shapely.geometry import LineString, Point

class LineSegment:
    def __init__(self, fromPoint, toPoint):
        self.pointA = fromPoint
        self.pointB = toPoint

    def endPointA(self):
        """Returns the frist endpoint of the line"""

        return self.pointA

    def endPointB(self):
        """Returns the second endpoint of the line"""

        return self.pointB

    def length(self):
        """Retruns the length of the line segment given as the Euclidean distance
                            between the two endpoints"""

        return self.pointA.distance(self.pointB)

    def toString(self):
        """Returns a string repreentation of the line segment"""
        x = (self.pointA.getX(), self.pointA.getY())
        y = (self.pointB.getX(), self.pointB.getY())
        return f'{x} # {y}'

    def isVertical(self):
        """Verifies wether the line segment is parallel to the y-axis"""
        if self.pointA.getX() == self.pointB.getX():
            return True
        else:
            return False

    def isHorizontal(self):
        """Verifies wether the line segment is parallel to the x-axis"""
        if self.pointA.getY() == self.pointB.getY():
            return True
        else:
            return False

    def isParallel(self, otherLine):
        """Verifies wether a line segment is parallel with another line segment"""

        slope_of_self = self.pointA.slope(self.pointB)
        slope_of_otherLine = otherLine.pointA.slope(otherLine.pointB)

        if slope_of_self == slope_of_otherLine:
            return True
        else:
            return False

    def isPerpendicular(self, otherLine):
        """Verifies wether a line segment is perpendicular with another line segment"""
        try:
            slope_of_self = self.pointA.slope(self.pointB)
            slope_of_otherLine = otherLine.pointA.slope(otherLine.pointB)
        except ZeroDivisionError:
            return f'We have one horizontal and one vertical line. They are always perpendicular.'
        else:
            if slope_of_self * slope_of_otherLine == -1:
                return True
            else:
                return False



class PlotTwoSegmentsATD(LineSegment):
    def __init__(self, segA, segB):
        self._segA = segA
        self._segB = segB

    def plot_segments(self):
        """Plot the segments in Cartesian Coordinate System"""
        end_pointA_of_segA = self._segA.endPointA()
        end_pointB_of_segA = self._segA.endPointB()
        end_pointA_of_segB = self._segB.endPointA()
        end_pointB_of_segB = self._segB.endPointB()
        x_values_of_segA = [end_pointA_of_segA.getX(), end_pointB_of_segA.getX()]
        y_values_of_segA = [end_pointA_of_segA.getY(),end_pointB_of_segA.getY()]
        x_values_of_segB = [end_pointA_of_segB.getX(), end_pointB_of_segB.getX()]
        y_values_of_segB = [end_pointA_of_segB.getY(), end_pointB_of_segB.getY()]
        plt.plot(x_values_of_segA, y_values_of_segA, marker='o')
        plt.plot(x_values_of_segB, y_values_of_segB, marker= 'o')



    def findIntersection(self):
        """Returns the point of intersection of 2 given segments"""
        try:
            end_pointA_of_segA = self._segA.endPointA()
            A = (end_pointA_of_segA.getX(), end_pointA_of_segA.getY())
            end_pointB_of_segA = self._segA.endPointB()
            B = (end_pointB_of_segA.getX(), end_pointB_of_segA.getY())
            end_pointA_of_segB = self._segB.endPointA()
            C = (end_pointA_of_segB.getX(), end_pointA_of_segB.getY())
            end_pointB_of_segB = self._segB.endPointB()
            D = (end_pointB_of_segB.getX(), end_pointB_of_segB.getY())
            px = ((A[0] * B[1] - A[1] * B[0]) * (C[0] - D[0]) - (A[0] - B[0]) * (C[0] * D[1] - C[1] * D[0])) / (
                        (A[0] - B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] - D[0]))
            py = ((A[0] * B[1] - A[1] * B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] * D[1] - C[1] * D[0])) / (
                        (A[0] - B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] - D[0]))
            return [px, py]
        except ZeroDivisionError:
            print("The segments do not intersect. They are parallel")


    def plot_intersection(self):
        """Plot the intersection point of two segments, if exists"""
        try:
            point_of_intersection = self.findIntersection()
            point_of_intersection_x = point_of_intersection[0]
            point_of_intersection_y = point_of_intersection[1]
            end_pointA_of_segA = self._segA.endPointA()
            end_pointB_of_segA = self._segA.endPointB()
            end_pointA_of_segB = self._segB.endPointA()
            end_pointB_of_segB = self._segB.endPointB()
            x_values_of_segA = [end_pointA_of_segA.getX(), end_pointB_of_segA.getX()]
            y_values_of_segA = [end_pointA_of_segA.getY(), end_pointB_of_segA.getY()]
            x_values_of_segB = [end_pointA_of_segB.getX(), end_pointB_of_segB.getX()]
            y_values_of_segB = [end_pointA_of_segB.getY(), end_pointB_of_segB.getY()]
            plt.plot(x_values_of_segA, y_values_of_segA, marker='o')
            plt.plot(x_values_of_segB, y_values_of_segB, marker='o')
            plt.plot([point_of_intersection_x], [point_of_intersection_y], marker='o')
        except TypeError:
            print("Therefore we cannot plot the intersection point.")
            self.plot_segments()


    def plot_sth(self):

        fig = plt.figure(num="Exponential")
        axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        x = np.arange(0, 11)
        axes.plot(x, x ** 2, marker='o')
        axes.set_xlim([0, 10])
        axes.set_ylim([0, 40])
        plt.show()

if __name__ == "__main__":
    pointA = PointADT(x=4, y=-3)
    pointB = PointADT(x=0, y=-15)
    pointC = PointADT(x=-2, y=-8)
    pointD = PointADT(x=4, y=-10)


    lineSegment1 = LineSegment(pointA, pointB)
    lineSegment2 = LineSegment(pointC, pointD)

    print(lineSegment1.isPerpendicular(lineSegment2))
    print(lineSegment1.isParallel(lineSegment2))
    plot1 = PlotTwoSegmentsATD(lineSegment1, lineSegment2)
    # print(plot1.plot_segments())
    print(plot1.findIntersection())
    plot1.plot_intersection()












