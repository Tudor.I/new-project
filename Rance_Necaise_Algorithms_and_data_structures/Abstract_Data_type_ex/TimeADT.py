
import datetime
import time
import struct
class TimeADT:
    def __init__(self, hours, minutes, seconds):
        # time_day = datetime.time(hour=hours, minute=minutes, second=seconds)
        assert datetime.time(hour=hours, minute=minutes, second=seconds), "Wrong values"
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):
        """Returns a TimeADT string"""
        time1 = str(datetime.time(self.hours, self.minutes, self.seconds))
        return time1
    def hour(self):
        return self.hours

    def minute(self):
        return self.minutes

    def second(self):
        return self.seconds

    def toSeconds(self):
        ADTtime_in_seconds = self.hours * 3600 + self.minutes * 60 + self.seconds
        return ADTtime_in_seconds
    def numSeconds(self, otherTime):
        ADTtime_in_seconds = self.toSeconds()
        otherTime_in_seconds = otherTime.toSeconds()
        the_seconds_difference = ADTtime_in_seconds - otherTime_in_seconds
        if the_seconds_difference < 0:
            return f'Between {otherTime} and {self} is a difference of {-the_seconds_difference} seconds'
        else:
            return f'Between {self} and {otherTime} is a difference of {the_seconds_difference} seconds'

    def isAM(self):
        if self.hours < 12:
            return True
        else:
            return False

    def isPM(self):
        if self.hours > 12:
            return True
        else:
            return False

    def comparable(self, other):
        assert isinstance(other, TimeADT or datetime.time), "Wrong type of the oject"
        this_time_to_sec = self.toSeconds()
        the_other_time_to_sec = other.toSeconds()
        if this_time_to_sec < the_other_time_to_sec:
            print( f'{self} < {other}')
        if this_time_to_sec <= the_other_time_to_sec:
            print (f'{self} <= {other}')
        if this_time_to_sec > the_other_time_to_sec:
            print (f'{self} > {other}')
        if this_time_to_sec == the_other_time_to_sec:
            print(f'{self} == {other}')
        if this_time_to_sec >= the_other_time_to_sec:
            print(f'{self} >= {other}')
        if this_time_to_sec != the_other_time_to_sec:
            print(f'{self} != {other}')

    def toString(self):

        time_12h = time.strptime(str(self), "%H:%M:%S")
        times = time.strftime("%I:%M:%S %p", time_12h)
        return times

class TimeDate:
    def __init__(self, month, day, year, hour, minute, second):
        self.month = month
        self.day = day
        self.year = year
        self.hour = hour
        self.minute = minute
        self.second = second

    def __str__(self):
        time = datetime.datetime(year=self.year,month=self.month, day=self.day,
                                 hour=self.hour, minute=self.minute, second=self.second)
        return str(time)
if __name__ == "__main__":
    this_Time = TimeADT(hours=10, minutes=30, seconds=10)
    othertime = TimeADT(hours=15, minutes=10, seconds=34)
    print(this_Time)
    this_Time.comparable(othertime)
    print(othertime.toString())

    timp_date = TimeDate(year=2020, month=4, day=14, hour=14, second=10, minute=14)
    print(timp_date)