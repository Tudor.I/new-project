"""
When implementing a Bag which can contain elements, we should take into account
the following conditions:
 -> Does the data structure provide for the storage requirments as
 specified by the domain of the ADT?
 -> Does the data structure provide the necessary data access and manipulation
 functionality to fully implement the ADT?
 -> Does the data structure lend itself to an efficient implementation of
 the operations?
"""
from Rance_Necaise_Algorithms_and_data_structures.Abstract_Data_type_ex.BagITerator import _BagIterator
#Implements the Bag ADT container using a Python List
class Bag:
    # Constructs an empty bag
    def __init__(self):
        self._theItems = list()

    #Returns the number of items in the bag
    def __len__(self):
        return len(self._theItems)

    #Determines if an item is contained in the bag
    def __contains__(self, item):
        return item in self._theItems

    #Adds a new item to the bag
    def add(self,item):
        self._theItems.append(item)

    # Removes and returns an instance of the item from the bag
    def remove(self, item):
        assert item in self._theItems, "The item must be in the bag"
        ndx = self._theItems.index(item)
        return self._theItems.pop(ndx)

    def __iter__(self):
        return _BagIterator(self._theItems)

import random
class GrabBag:
    # Constructs an empty bag
    def __init__(self):
        self.theItems = []

    #Returns the number of items in the bag
    def __len__(self):
        return len(self.theItems)

    #Determines if an item is contained in the bag
    def __contains__(self, item):
        return item in self.theItems

    #Adds a new item to the bag
    def add(self, it):
        self.theItems.append(it)

    # Randomly grabs an item from the bag
    def grabItem(self):
        ndx = random.randint(0, len(self.theItems) - 1)
        return self.theItems.pop(ndx)

    # Returns the nu,ber of occurrences of the given item
    def nomOf(self, item):
        assert item in self.theItems, "The item does not belong to the bag"
        num_of_occurences = self.theItems.count(item)
        return num_of_occurences
    def __iter__(self):
        return _BagIterator(self.theItems)

m = 0
n = 1
z = 'andrei'
u = 'marius'
x = 1+3
j = [1,2]
q = 'andrei'

items = GrabBag()
items.add(m)
items.add(n)
items.add(z)
items.add(u)
items.add(x)
items.add(j)
items.add(q)

print(items.grabItem())
print(items.nomOf('andrei'))