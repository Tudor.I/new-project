from tkinter import *

class Application(Frame):

    def __init__(self, m):
        super(Application, self).__init__(m)
        self.grid()
        self.bttn_clicks = 0
        self.create_widget()

    def create_widget(self):
        self.bttn = Button(self)
        self.bttn1 = Button(self)
        self.bttn['text'] = "Total Clicks: 0"
        self.bttn['command'] = self.update_count
        self.bttn1['text'] = 'Reset'
        self.bttn1['command'] = self.reset_count
        self.bttn.grid()
        self.bttn1.grid()

    def update_count(self):
        self.bttn_clicks += 1
        self.bttn['text'] = "Total Clicks: " + str(self.bttn_clicks)
    def reset_count(self):
        self.bttn_clicks = 0
        self.bttn['text'] = "Total Clicks: 0"

    # def reset_count(self):
    #     self.bttn1 = Button(self)
    #     self.bttn1['text'] = "Reset"
    #

root = Tk()
root.title("Click Counter")
root.geometry('200x100')

app = Application(root)

root.mainloop()