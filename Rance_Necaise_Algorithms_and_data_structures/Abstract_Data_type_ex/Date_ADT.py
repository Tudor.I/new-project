"""
Defining a Date ADT, which represents a single day in the propleptic Gregorian
calendar in which the first day start on Nov 14, 4713 BC
"""

"""
The following example ilustrates an advantage of working with an abstraction
by focusing on what functionality the ADT provides instead of how that
functionality is implemented. Let's suppose that we want to extracte the dates
from a standard input. Those dates that indicate the individual is least 21 years of age
based on a target date are printed to standard output.
By hiding the implementation details, we can use an ADT independent of tis implementation.
In fact, the choice of implementation for the Date ADT
will have no effect on the instructions in our example program.
"""

from datetime import date
import datetime
import time
def main():
    def prompt_and_extract_date():
        print("Enter a b-day date.")
        month = int(input("month (0 to quit): "))
        if month == 0:
            return None
        else:
            day = int(input("Day: "))
            year = int(input("Year: "))

            return datetime.date(year= year, day=day, month=month)





    born_before = datetime.date(day=6, month=1, year=1998)
    date = prompt_and_extract_date()
    while date is not None:
        if date <= born_before:
            print("Is at least 22 years of age:", date)
        else:
            print("Is younger than 22 years old:", date)
        date = prompt_and_extract_date()

# main()


"""
Implementing the ADT 

We will write a simple class which will define a simple ADT for representing a date in
the propleptic Greogrian Calendar.
 The proleptic Gregorian calendar is an
extension for accommodating earlier dates with the first date on November 24,
4713 BC.

By hiding the implementation details, we can use an ADT
independent of its implementation.
After defining the ADT, we need to provide an implementation in an appropriate
language. In our case, we will always use Python and class definitions, but any
programming language could be used.

There are two common approaches to storing a date in an object. One approach
stores the three components—month, day, and year—as three separate fields. With
this format, it is easy to access the individual components, but it’s difficult to
compare two dates or to compute the number of days between two dates since the
number of days in a month varies from month to month. The second approach
stores the date as an integer value representing the Julian day, which is the number
of days elapsed since the initial date of November 24, 4713 BC (using the Gregorian
calendar notation). Given a Julian day number, we can compute any of the three
Gregorian components and simply subtract the two integer values to determine 
which occurs first or how many days separate the two dates
"""
"""

To access the Gregorian date components the Julian day must be converted back to Gregorian.
This conversion is needed in several of the ADT operations.
The toGregorian() method returns a tuple containing the day, month, and
year components. As with the conversion from Gregorian to Julian, 
integer arithmetic 
operations are used throughout the conversion formula
"""

    #Implements a proleptic Greogrian calendar date as a Julian Day Number
from _datetime import date
a = date(year=1998, day=14, month=1)
import calendar
class Date:
    """Creates an object instance for the specified Gregorian date"""
    def __init__(self, month = 0, day = 0, year = 0):
        if month == 0 or day == 0 or year == 0:
            today_date = date.today()
            month = today_date.month
            day = today_date.day
            year = today_date.year
            self.month = month
            self.day = day
            self.year = year
        else:
            self.month = month
            self.day = day
            self.year = year

        self._julianDay = 0
        # self._isValidGregorian = date(year=year, month=month, day=day)
        assert date(year=self.year, month=self.month, day=self.day), "Invalid Gregorian Date"
    # The first line of equation, T = (M-14) / 12, has to be changed
    #since Python's implementation of integer division is not the same
    #as the mathematical definition
        tmp = 0
        if self.month < 3:
            tmp = -1
        x = day - 32075
        y = 1461 * (year + 4800 + tmp) // 4
        z = 367 * (month - 2 - tmp * 12) // 12
        w = 3 * ((year + 4900 + tmp) // 100) // 4
        self._julianDay = x + y + z - w
    #The following function returns the Gregorian date as a tuple:(month, day,year)
    def _toGregorian(self):
        A = self._julianDay + 68569
        B = 4 * A // 146097
        A = A - (146097 * B + 3) // 4
        year = 4000 * (A + 1) // 1461001
        A = A - (1461 * year // 4) + 31
        month = 80 * A // 2447
        day = A - (2447 * month // 80)
        A = month // 11
        month = month + 2 - (12 * A)
        year = 100 * (B - 49) + year + A
        return month, day, year
    # Here we extract the appropiate Gregorian date component
    def the_month(self):
        """Returns (M,d,y)"""
        return self._toGregorian()[0]
    def the_day(self):
        """Returns (m,D,y)"""
        return self._toGregorian()[1]

    def the_year(self):
        """Returns (m,d,Y)"""
        return self._toGregorian()[2]

    def dayOfWeek(self):
        month, day, year = self._toGregorian()
        if month < 3:
            month = month + 12
            year = year - 1
        return ((13 * month + 3) // 5 + day + year + year // 4 -
                year // 100 + year // 400) % 7

    # Returns the date as a string in Gregorian format
    def __str__(self):
        month, day, year = self._toGregorian()
        return "%2d/%02d/%04d" % (month, day, year)

    def __eq__(self, otherDate):
        """Compare two Gregorian dates. Return True if they are the same date"""
        return self._julianDay == otherDate._julianDay

    def __lt__(self, otherDate):
        return self._julianDay < otherDate._julianDay

    def __le__(self, otherDate):
        return self._julianDay <= otherDate._julianDay

    def __gt__(self, otherDate):
        return self._julianDay > otherDate._julianDay

    def __ge__(self, otherDate):
        return self._julianDay >= otherDate._julianDay

    def __ne__(self, otherDate):
        return self._julianDay != otherDate._julianDay

    def monthName(self):
        """Returns the name of the month"""
        y = self._toGregorian()
        date1 = date(month=y[0], day= y[1], year=y[2])
        return date1.strftime("%B")

    def isLeapYear(self):
        """Returns True if Date is Leap Year"""
        date_in_gregorian = self._toGregorian()
        if date_in_gregorian[2] % 4 == 0:
            return True
        else:
            return False

    def advanceDays(self, days):
        assert isinstance(days, int), "The given number is not integer positive/negative"
        if days > 0:
            self._julianDay += days
            month, day, year = self._toGregorian()
            return "%2d/%02d/%04d" % (month, day, year)
        else:
            self._julianDay -= -days
            month, day, year = self._toGregorian()
            return "%2d/%02d/%04d" % (month, day, year)

    def theCalendar(self, Date_ADT):
        assert isinstance(Date_ADT, Date), "This is not a Date type"
        month, day, year = Date_ADT._toGregorian()
        return calendar.month(theyear=year, themonth=month)

if __name__ == "__main__":
    # firstDay = Date(month=11, year=1926, day=6)
    # secondDay = Date(month=11, year=2020, day=5)
    #
    # print(firstDay.theCalendar(secondDay))
    # # if firstDay == secondDay:
    # #     print(True)
    # # else:
    # #     print(False)
    #
    # print(firstDay)
    # next_days = firstDay.advanceDays(50000)
    # print(next_days)
    # month = 4
    # print(calendar.month(2020, 4))

    thirdDay = Date()
    print(thirdDay)

