from Rance_Necaise_Algorithms_and_data_structures.Abstract_Data_type_ex.Date_ADT import Date
from datetime import date
class ActivitiesCalendar:
    def __init__(self, dateFrom, dateTo):
        try:
            assert isinstance(dateTo and dateFrom, Date), "Wrong input of dates"
            if dateFrom < dateTo and dateFrom != dateTo:
                self._dateFrom = dateFrom
                self._dateTo = dateTo
                self.activites = [(dateFrom, 'Starting date of the calendar'), (dateTo, 'Last day of the calendar')]
            else:
                print("DateFrom should be lower than DateTo and different. Try again")
        except AssertionError:
            print("Wrong Date objects.")



    def __repr__(self):
        return f'{[(date.__str__(), activity) for date,activity in self.sortingTheDates()]}'

    def sortingTheDates(self):
        """Returns a new sorted list of dates"""

        first_element = self.activites.pop(0)
        last_element = self.activites.pop(-1)
        lista_date_sortate = sorted([activity[0] for activity in self.activites if isinstance(activity[0], Date)])
        lista_activitati = [activity[1] for activity in self.activites if isinstance(activity[0], Date)]
        lista_sortata = []
        for data in lista_date_sortate:
            for activitate in self.activites:
                if isinstance(activitate[0], Date) and activitate[0] == data:
                    lista_sortata.append(activitate)
        lista_sortata.insert(0, first_element)
        lista_sortata.append(last_element)
        self.activites = []
        self.activites.extend(lista_sortata)
        return self.activites

    def length(self):
        """Returns the number of activities within the calendar"""

        return len(self.activites) - 2

    def addActivity(self, date, activity):
        """Adds a new activity"""

        if date > self._dateFrom and date < self._dateTo:
            if len(self.activites) == 2:
                tmp = self.activites.pop(1)
                self.activites.append((date, activity))
                self.activites.append(tmp)
            else:
                self.activites.insert(1, (date, activity))
            print("New activity has been added")
        else:
            print("The date does not belongs to the interval")

    def getActivity(self, date1):
        """Returns the activity which is taking place on the given date"""

        assert date1 in [date[0] for date in self.activites], "This date does not exist in the activities calendar. You can add one."
        try:
            for date in self.activites:
                if date[0] == date1:
                    return f'On {date[0].__str__()} we have the following activity: {date[1]}'
        except IndexError:
            return f'On {date1.__str__()} is no activity taking place'

    def displayMonth(self, month):
        """Displays the activites which take place in a given month"""

        try:
            activites_from_the_given_month = []
            for activites in self.sortingTheDates():
                if activites[1] in ['Starting date of the calendar','Last day of the calendar']:
                    pass
                else:
                    if activites[0].the_month() == month:
                        activites_from_the_given_month.append((activites[0].the_day(), activites[1]))
            given_month = date(month=month, day=10, year=2020)
            time_fromat = given_month.strftime('%B')
            if len(activites_from_the_given_month) > 0:
                print(f"In {time_fromat} take place the following activities: ")
                for day, activity in activites_from_the_given_month:
                    print(" - %d: %a " % (day, activity))
            else:
                print(f"In {time_fromat} takes place no activity.")
        except ValueError:
            print("Invalid month. Please try again")







dateFrom = Date(month=1, day=1, year=2020)
dateTo = Date(month=12, day=31, year=2020)
calendar_of_activities = ActivitiesCalendar(dateFrom=dateFrom, dateTo=dateTo)
calendar_of_activities.addActivity(date=Date(month=2, day=23, year=2020), activity='Playing a footbal game')
calendar_of_activities.addActivity(date=Date(month=2, day=10, year=2020), activity="Mom's b-day")
calendar_of_activities.addActivity(date=Date(month=1, day=3, year=2019), activity='Playing a footbal game')
calendar_of_activities.addActivity(date=Date(month=10, day=13, year=2020), activity='blabla')
calendar_of_activities.addActivity(date=Date(month=6, day=13, year=2020), activity='blabla1')
calendar_of_activities.addActivity(date=Date(month=5, day=13, year=2020), activity='blabla2')
calendar_of_activities.addActivity(date=Date(month=5, day=14, year=2020), activity='blabla3')
calendar_of_activities.addActivity(date=Date(month=11, day=1, year=2020), activity="Nimic")

if __name__ == "__main__":
    calendar_of_activities.displayMonth(10)


