
class PlotTwoSegmentsATD(LineSegment):
    def __init__(self, segA, segB):
        self._segA = segA
        self._segB = segB

    def plot_segments(self):
        end_pointA_of_segA = self._segA.endPointA()
        end_pointB_of_segA = self._segA.endPointB()
        end_pointA_of_segB = self._segB.endPointA()
        end_pointB_of_segB = self._segB.endPointB()
        x_values_of_segA = [end_pointA_of_segA.getX(), end_pointB_of_segA.getX()]
        y_values_of_segA = [end_pointA_of_segA.getY(),end_pointB_of_segA.getY()]
        x_values_of_segB = [end_pointA_of_segB.getX(), end_pointB_of_segB.getX()]
        y_values_of_segB = [end_pointA_of_segB.getY(), end_pointB_of_segB.getY()]
        # fig = plt.figure(num="The representation of A segment and B segment")
        # # axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        plt.plot(x_values_of_segA, y_values_of_segA, marker='o')
        plt.plot(x_values_of_segB, y_values_of_segB, marker= 'o')
        # plt.xlabel("x - axis")
        # plt.ylabel("y - axis")
        # # plt.figure(num='The representation of A segment and B segment')
        # plt.xlim(0,10)
        # plt.ylim(-15, 10)
        # plt.show()


    def findIntersection(self):
        end_pointA_of_segA = self._segA.endPointA()
        A = (end_pointA_of_segA.getX(), end_pointA_of_segA.getY())
        end_pointB_of_segA = self._segA.endPointB()
        B = (end_pointB_of_segA.getX(), end_pointB_of_segA.getY())
        end_pointA_of_segB = self._segB.endPointA()
        C = (end_pointA_of_segB.getX(), end_pointA_of_segB.getY())
        end_pointB_of_segB = self._segB.endPointB()
        D = (end_pointB_of_segB.getX(), end_pointB_of_segB.getY())
        px = ((A[0] * B[1] - A[1] * B[0]) * (C[0] - D[0]) - (A[0] - B[0]) * (C[0] * D[1] - C[1] * D[0])) / (
                    (A[0] - B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] - D[0]))
        py = ((A[0] * B[1] - A[1] * B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] * D[1] - C[1] * D[0])) / (
                    (A[0] - B[0]) * (C[1] - D[1]) - (A[1] - B[1]) * (C[0] - D[0]))
        return [px, py]


    def plot_intersection(self):
        point_of_intersection = self.findIntersection()
        point_of_intersection_x = point_of_intersection[0]
        point_of_intersection_y = point_of_intersection[1]
        end_pointA_of_segA = self._segA.endPointA()
        end_pointB_of_segA = self._segA.endPointB()
        end_pointA_of_segB = self._segB.endPointA()
        end_pointB_of_segB = self._segB.endPointB()
        x_values_of_segA = [end_pointA_of_segA.getX(), end_pointB_of_segA.getX()]
        y_values_of_segA = [end_pointA_of_segA.getY(), end_pointB_of_segA.getY()]
        x_values_of_segB = [end_pointA_of_segB.getX(), end_pointB_of_segB.getX()]
        y_values_of_segB = [end_pointA_of_segB.getY(), end_pointB_of_segB.getY()]
        # fig = plt.figure(num="The representation of the point of intersection of A segment and B segment")
        # axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        plt.plot(x_values_of_segA, y_values_of_segA, marker='o')
        plt.plot(x_values_of_segB, y_values_of_segB, marker='o')
        plt.plot(point_of_intersection_x, point_of_intersection_y, marker='o')
        # plt.xlabel("x - axis")
        # plt.ylabel("y - axis")
        # plt.figure(num='The representation of A segment and B segment')
        # plt.xlim(-10, 10)
        # plt.ylim(-15, 10)
        # plt.show()

    def plot_sth(self):

        fig = plt.figure(num="Exponential")
        axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        x = np.arange(0, 11)
        axes.plot(x, x ** 2, marker='o')
        axes.set_xlim([0, 10])
        axes.set_ylim([0, 40])
        plt.show()