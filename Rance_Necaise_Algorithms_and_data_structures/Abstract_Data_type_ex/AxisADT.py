import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axisartist import SubplotZero
import math
from Rance_Necaise_Algorithms_and_data_structures.Abstract_Data_type_ex.LineSegment import PlotTwoSegmentsATD, LineSegment, PointADT
class Axes:

    def __init__(self, xlim=(-5, 5), ylim=(-5, 5), figsize=(12, 5)):
        self.xlim = xlim
        self.ylim = ylim
        self.figsize = figsize
        self.points = []
        self.segments = []
        self.vectors = []
        self.lines = []
        self.scale_arrows()

    def __arrow__(self, x, y, dx, dy, width, length):
        plt.arrow(
            x, y, dx, dy,
            color='k',
            clip_on=False,
            head_width=self.head_width,
            head_length=self.head_length
        )

    def __drawAxis__(self):
        """
        Draws the 2D cartesian axis
        """
        # A subplot with two additional axis, "xzero" and "yzero"
        # corresponding to the cartesian axis
        ax = SubplotZero(self.fig, 1, 1, 1)
        self.fig.add_subplot(ax)

        # make xzero axis (horizontal axis line through y=0) visible.
        for axis in ["xzero", "yzero"]:
            ax.axis[axis].set_visible(True)
        # make the other axis (left, bottom, top, right) invisible
        for n in ["left", "right", "bottom", "top"]:
            ax.axis[n].set_visible(False)

        # Plot limits
        plt.xlim(self.xlim)
        plt.ylim(self.ylim)
        # Draw the arrows
        self.__arrow__(self.xlim[1], 0, 0.01, 0, 0.3, 0.2)  # x-axis arrow
        self.__arrow__(0, self.ylim[1], 0, 0.01, 0.2, 0.3)  # y-axis arrow

    def scale_arrows(self):
        """ Make the arrows look good regardless of the axis limits """
        xrange = self.xlim[1] - self.xlim[0]
        yrange = self.ylim[1] - self.ylim[0]

        self.head_width = min(xrange / 30, 0.25)
        self.head_length = min(yrange / 30, 0.3)

    def draw(self, seg1, seg2,plot_segments = False, plot_intersection = False, image=None):
        self.scale_arrows()
        self.fig = plt.figure(figsize=self.figsize, num="Cartesian representation")
        # First draw the axis
        self.__drawAxis__()
        # Plot each point
        plot_segment = PlotTwoSegmentsATD(seg1, seg2)
        if plot_segments == True:
            plot_segment.plot_segments()
        elif plot_intersection == True:
            plot_segment.plot_intersection()
        for point in self.points:
            point.draw()
        # Save the image?
        if image:
            plt.savefig(image)
        plt.show()


if __name__ == "__main__":
    axes = Axes(xlim=(-15, 15), ylim=(-15,15), figsize=(8,6))
    pointA = PointADT(x=9, y=7)
    pointB = PointADT(x=7, y=5)
    pointC = PointADT(x=5, y=13)
    pointD = PointADT(x=13, y=0)
    lineSegment1 = LineSegment(pointA, pointB)
    lineSegment2 = LineSegment(pointC, pointD)
    print(lineSegment1.isPerpendicular(lineSegment2))
    print(lineSegment1.isParallel(lineSegment2))
    segments = PlotTwoSegmentsATD(lineSegment1, lineSegment2)
    print(segments.findIntersection())
    axes.draw(seg1=lineSegment1, seg2=lineSegment2,plot_intersection=True)

