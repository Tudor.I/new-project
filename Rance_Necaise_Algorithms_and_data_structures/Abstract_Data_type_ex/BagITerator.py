class _BagIterator:
    def __init__(self, theList):
        self._bagItems = theList
        self._curITem = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._curITem < len(self._bagItems):
            item = self._bagItems[self._curITem]
            self._curITem += 1
            return item
        else:
            raise StopIteration
