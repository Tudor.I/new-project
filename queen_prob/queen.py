import sys
import random
import math
def test(test_OK):
    """ printare rezultat test """
    nr_crt = sys._getframe(1).f_lineno # nr liniei de unde s-a apelat testul
    if test_OK:
        msg = "Testul din lina {0} este trecut.".format(nr_crt)
    else:
        msg = ("Testul din linia {0} NU ESTE TRECUT.".format(nr_crt))
    print(msg)

bd3 = [(0,6), (1,4), (2,2), (3,0), (4,5), (5,7), (6,1), (7,3)]

def share_diagonal(x0, y0, x1, y1):
    """ Is (x0, y0) on a shared diagonal with (x1, y1)? """
    dy = abs(y1 - y0)        # Calc the absolute y distance
    dx = abs(x1 - x0)        # CXalc the absolute x distance
    return dx == dy         # They clash if dx == dy

def col_clashes_reflection(bs, c):
    """ Return True if the queen at column c clashes
         with any queen to its left.
    """
    for i in range(c):     # Look at all columns to the left of c
          if share_diagonal(i, bs[i], c, bs[c]):
              return True

    return False
def col_clashes(bs, c):
    """ Return True if the queen at column c clashes
         with any queen to its left.
    """
    for i in range(c):     # Look at all columns to the left of c
          if share_diagonal(i, bs[i], c, bs[c]):
              return True

    return False           # No clashes - col c has a safe placement.
# Solutions cases that should not have any clashes
def has_clashes(the_board):
    """ Determine whether we have any queens clashing on the diagonals.
        We're assuming here that the_board is a permutation of column
        numbers, so we're not explicitly checking row or column clashes.
    """
    for col in range(1,len(the_board)):
        if col_clashes(the_board, col):
            return True
    return False


def main():
    import random
    # rng = random.Random()  # Instantiate a generator
    list_of_solutions = []
    bd = list(range(6))  # Generate the initial permutation
    num_found = 0
    tries = 0
    res = 0
    while num_found < 30:
        res = random.sample(bd,len(bd))
        tries += 1
        if not has_clashes(res):
            # print("{0}Found solution {1} in {2} tries.".format(num_found,res, tries))
            list_of_solutions.append(res)
            num_found += 1
            if list_of_solutions.count(res) == 1 or res not in list_of_solutions:
                print("{0}Found solution {1} in {2} tries.".format(num_found, res, tries))
                tries = 0
    return list_of_solutions
#
# main()

def Y_reflexion(nr_solution, rng):
    # rng = random.Random()  # Instantiate a generator
    list_of_solutions = []
    list_y_reflections = []
    list_of_solutions1 = []
    bd = list(range(rng))  # Generate the initial permutation
    num_found = 0
    res = 0
    x, y = 0, 0
    while num_found < nr_solution:
        res = random.sample(bd,len(bd))
        if not has_clashes(res):
            list_of_solutions.append(res)
            num_found += 1
    idx = 0

    for solution in list_of_solutions:
        list_of_solutions1.append(solution)
        if list_of_solutions1.count(solution) == 1 or res not in list_of_solutions1:
            idx = 0
            lista_temp = []
            while idx < len(solution):
                x,y = -idx, solution[idx]
                lista_temp.append((x,y))
                idx += 1
            list_y_reflections.append(lista_temp)
        else:
            continue
    return list_y_reflections

def X_reflexion(nr_solution, rng):
    # rng = random.Random()  # Instantiate a generator
    list_of_solutions = []
    list_x_reflections = []
    list_of_solutions1 = []
    bd = list(range(rng))  # Generate the initial permutation
    num_found = 0
    res = 0
    x, y = 0, 0
    while num_found < nr_solution:
        res = random.sample(bd,len(bd))
        if not has_clashes(res):
            list_of_solutions.append(res)
            num_found += 1
    idx = 0

    for solution in list_of_solutions:
        list_of_solutions1.append(solution)
        if list_of_solutions1.count(solution) == 1 or res not in list_of_solutions1:
            idx = 0
            lista_temp = []
            while idx < len(solution):
                x,y = idx, -solution[idx]
                lista_temp.append((x,y))
                idx += 1
            list_x_reflections.append(lista_temp)
        else:
            continue
    return list_x_reflections

def rotation_90_180_270(nr_solution, rng, degree):
    # rng = random.Random()  # Instantiate a generator
    list_of_solutions = []
    list_y_reflections = []
    list_of_solutions1 = []
    list_90_degrees = []
    list_180_degrees = []
    list_270_degrees = []
    try:
        if (degree in [90,180,270] and isinstance(degree,int) and
            rng > 0 and isinstance(rng, int)  and rng % 2 == 0 and
            nr_solution > 0 and isinstance(nr_solution, int)):

                bd = list(range(rng))     # Generate the initial permutation
                num_found = 0
                res = 0
                x, y = 0, 0
                while num_found < nr_solution:
                    res = random.sample(bd,len(bd))
                    if not has_clashes(res):
                        list_of_solutions.append(res)
                        num_found += 1

                if degree == 90:
                    idx = 0
                    for solution in list_of_solutions:
                        list_of_solutions1.append(solution)
                        if list_of_solutions1.count(solution) == 1 or res not in list_of_solutions1:
                            idx = 0
                            lista_temp = []
                            while idx < len(solution):
                                x,y = -idx, solution[idx]
                                lista_temp.append((x,y))
                                idx += 1
                            list_90_degrees.append(lista_temp)
                        else:
                            continue
                    return list_90_degrees

                elif degree == 180:
                    idx = 0
                    for solution in list_of_solutions:
                        list_of_solutions1.append(solution)
                        if list_of_solutions1.count(solution) == 1 or res not in list_of_solutions1:
                            idx = 0
                            lista_temp = []
                            while idx < len(solution):
                                x, y = -idx, -solution[idx]
                                lista_temp.append((x, y))
                                idx += 1
                            list_180_degrees.append(lista_temp)
                        else:
                            continue
                    return list_180_degrees

                if degree == 270:
                    idx = 0
                    for solution in list_of_solutions:
                        list_of_solutions1.append(solution)
                        if list_of_solutions1.count(solution) == 1 or res not in list_of_solutions1:
                            idx = 0
                            lista_temp = []
                            while idx < len(solution):
                                x, y = idx, -solution[idx]
                                lista_temp.append((x, y))
                                idx += 1
                            list_270_degrees.append(lista_temp)
                        else:
                            continue
                    return list_270_degrees
        else:
            raise ValueError
    except ValueError:
        if not(degree in [90,180,270]) or not(isinstance(degree,int)):
            print("Va rugam sa introduceti numarul de grade de tip integer si sa aiba una idn urmatoarele valori: 90, 180, 270")
        if not(rng > 0 and isinstance(rng, int)):
            print("Va rugam sa introduceti un numar intreg pozitiv")
        if rng % 2 != 0:
            print("Va rugam introduceti un range care sa fie multiplu de 2")

        if not(nr_solution > 0) or not(isinstance(nr_solution, int)):
            print("Va rugam introduceti un numar de solutii, pe care le doriti afisate,  pozitiv, de tip integer\n"
                  "Atentie! Solutiile nu vor fi returnate in tip duplicat, asadar exista posibilitatea sa se afiseze mai putine rezultate decat numarul introdus\n"
                  "Modul in care sunt returnate solutiile depinde de numarul de solutii existente pentru fiecare numar n dat")



x = rotation_90_180_270(nr_solution=10, degree=90, rng=8)
print(x)


