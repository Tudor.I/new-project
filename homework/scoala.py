from homework.Clasa import Clasa
from homework.Nota import Nota
from homework.student import Student
from timeit import timeit
from datetime import datetime
import time
class Scoala(Clasa):
    def __init__(self):
        # super().__init__()
        self.clase = []
    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.clase):
            result = self.clase[self.n]
            self.n += 1
            return result
        else:
            raise StopIteration
    def incarcare_studenti(self, lista_elevi, lista_note):
        alta_lista_elevi = []
        for elev in lista_elevi:
            elevul_clasei = Student()
            elevul_clasei.adaugare_studentii(
                id=elev["Index"], clasa=elev["Clasa"],
                nume=elev["Nume"], prenume=elev["Prenume"]
            )
            note_student_romana = [int(note["nota"]) for note in lista_note if
                                   note["index"] == elev["Index"] and note["materie"] == 'romana']
            nota_student_informatica = [int(note["nota"]) for note in lista_note if
                                        note["index"] == elev["Index"] and note["materie"] == 'informatica']
            nota_student_matematica = [int(note["nota"]) for note in lista_note if
                                       note["index"] == elev["Index"] and note["materie"] == 'matematica']
            nota_student_engleza = [int(note["nota"]) for note in lista_note if
                                    note["index"] == elev["Index"] and note["materie"] == 'engleza']
            note_romana = Nota()
            note_matematica = Nota()
            note_informatica = Nota()
            nota_engleza = Nota()
            note_romana.adaugare_valorile_notei(id_student=elev['Index'], valoare=note_student_romana,materie='romana')
            note_matematica.adaugare_valorile_notei(id_student=elev['Index'], valoare=nota_student_matematica,
                                                    materie='matematica')
            nota_engleza.adaugare_valorile_notei(id_student=elev['Index'], valoare=nota_student_engleza,
                                                 materie='engleza')
            note_informatica.adaugare_valorile_notei(id_student=elev['Index'], valoare=nota_student_informatica,
                                                     materie='informatica')
            elevul_clasei.adaugare_nota_la_student(note_romana)
            elevul_clasei.adaugare_nota_la_student(note_matematica)
            elevul_clasei.adaugare_nota_la_student(nota_engleza)
            elevul_clasei.adaugare_nota_la_student(note_informatica)
            alta_lista_elevi.append(elevul_clasei)
        clase = sorted(set([student.clasa for student in alta_lista_elevi]))
        for nume_clasa in clase:
            clasaa = Clasa()
            clasaa.adaugare_clasa(an=nume_clasa[0], serie=nume_clasa[1])
            l = [student for student in alta_lista_elevi if nume_clasa == student.clasa]
            clasaa.adaugare_studenti(l)
            self.clase.append(clasaa)
        return self.clase
    # functia creaza o instanta de scoala, luand ca parametru doua liste, una cu note si una cu studenti


    def masurare_incarcare_studenti(self, lista_elevi, lista_note):
        timp_start = time.time()
        self.incarcare_studenti(lista_elevi, lista_note)
        delta_time = time.time() - timp_start
        return delta_time
    # functia masoara timpul in care se incarca datele.
    def medie_matematica(self):
        lista_elevi_medii_mate = []
        for clasa in self:
            for clasa1 in clasa:
                for elev in clasa1:
                    lista_elevi_medii_mate.append(elev.media_matematica())
        return lista_elevi_medii_mate
    # functia returneaza media la matematica a fiecarui student, intr-o singura lista
    def medie_romana(self):
        media_romana= 0
        lista_medii_romana = []
        for clasa in self:
            for clasa1 in clasa:
                for elev in clasa1:
                    lista_medii_romana.append(elev.media_romana())
        return lista_medii_romana
    # functia returneaza media la romana a fiecarui student, intr-o singura lista

    def eligibil_olimpiada(self):
        eligibili_olimpiada = []
        for clasa in self:
            for clasa1 in clasa:
                for elev in clasa1:
                    if elev.eligibil_olimpiadaa():
                        eligibili_olimpiada.append(elev)
        elevi_eligibili = [elev1.reprezentare_student() for elev1 in eligibili_olimpiada]
        return elevi_eligibili
    #functia returneaza o lista cu reprezentarea studentilor, eligibili pentru olimpiada

    def materia_preferataa(self):
        u = []
        m = []
        materia = []
        lista_materii_preferate = []
        dictionar = {}
        for clasa in self:
            for clasa1 in clasa:
                for elev in clasa1:
                    media_romana = elev.media_romana()
                    media_matematica = elev.media_matematica()
                    media_informatica = elev.media_informatica()
                    media_engleza = elev.media_engleza()
                    m = [media_engleza, media_informatica, media_romana, media_matematica]

                    media_maxima = max(m)
                    numarul_aparitiei_notei_maxime = m.count(media_maxima)
                    if numarul_aparitiei_notei_maxime > 1:
                        i = 0
                        dictmate = {}
                        dictromana = {}
                        dictinfo = {}
                        dictengleza= {}
                        while i < numarul_aparitiei_notei_maxime:
                            if media_maxima == elev.media_matematica() and dictmate not in lista_materii_preferate:
                                u = elev
                                m = media_maxima
                                materia = "matematica"
                                dictmate = {
                                    "Student": u,
                                    "nota_preferata": m,
                                    "materia": materia
                                }
                                lista_materii_preferate.append(dictmate)
                                i+=1
                            elif media_romana == media_maxima and dictromana not in lista_materii_preferate:
                                u = elev
                                m = media_maxima
                                materia = "romana"
                                dictromana ={
                                    "Student": u,
                                    "nota_preferata": m,
                                    "materia": materia
                                }
                                lista_materii_preferate.append(dictromana)
                                i+=1
                            elif media_engleza == media_maxima and dictengleza not in lista_materii_preferate:
                                u = elev
                                m = media_maxima
                                materia = "engleza"
                                dictengleza = {
                                    "Student": u,
                                    "nota_preferata": m,
                                    "materia": materia
                                }
                                lista_materii_preferate.append(dictengleza)
                                i += 1
                            elif media_informatica == media_maxima and dictinfo not in lista_materii_preferate:
                                u = elev
                                m = media_maxima
                                materia = "informatica"
                                dictinfo ={
                                    "Student": u,
                                    "nota_preferata": m,
                                    "materia": materia
                                }
                                lista_materii_preferate.append(dictinfo)
                                i += 1
                    else:
                        if media_matematica == media_maxima:
                            u = elev
                            m = media_maxima
                            materia = "matematica"
                        elif media_romana == media_maxima:
                            u = elev
                            m = media_maxima
                            materia = "romana"
                        elif media_engleza == media_maxima:
                            u = elev
                            m = media_maxima
                            materia = "engleza"
                        elif media_informatica == media_maxima:
                            u = elev
                            m = media_maxima
                            materia = "informatica"
                        dictionar = {
                            "Student": u,
                            "nota_preferata": m,
                            "materia": materia
                        }
                        lista_materii_preferate.append(dictionar)
        return lista_materii_preferate
        #functia returneaza mo lista de dictionare, cu materia preferata a fiecarui student in parte
        #functia calculeaza si cazul special in care un student are doua materii preferate (sau mai multe)



    def lista_studenti_eligibili_olimpiadaa(self, clasa):
        lista_studenti_olimpiada = []
        for elev in clasa:
            if elev.eligibil_olimpiadaa():
                lista_studenti_olimpiada.append(elev)
        return lista_studenti_olimpiada
    #functia este folosita ulterior, pentru a determina lista cu studentii eligibili pentru olimpiada

    def lista_studenti_eligibili_pentru_olimpiada(self):
        lista_studenti_olimpiada = []
        for scoala in self:
            for clasa in scoala:
                for elev in clasa:
                    if elev.eligibil_olimpiadaa():
                        lista_studenti_olimpiada.append(elev.reprezentare_student())
        return lista_studenti_olimpiada
    #este returnata reprezentarea studentilor eligibili pentru olimpiada, intr-o lista

    def clasa_olimpici(self):
        def countX(lista, serie):
            count = 0
            for ele in lista:
                if (ele == serie):
                    count = count + 1
            return count
        aa = 0
        bb = {}
        clase_olimpice = []
        i = 0
        for clasa in self:
            serie = clasa.an + clasa.serie
            for clasa1 in clasa:
                x = self.lista_studenti_eligibili_olimpiadaa(clasa1)
                y = [student.clasa for student in x]
                while True:
                    if countX(lista=y, serie=serie) >= 1:
                        aa = f"Clasa {serie} este olimpica"
                        bb = {
                            "clasa": serie,
                            "numar_olimpici":countX(lista=y, serie=serie)
                        }

                        clase_olimpice.append(bb)
                    break
        return clase_olimpice
    # functia este folosita in clasa_cu_cei_mai_multi_olimpici, pentru a determina clasa cu cei mai multi olimpici pe an

    def cel_mai_bun_ca_si_medie_pe_clasa(self):
        lista_cu_cei_mai_buni_pe_clasa = []
        for clasa in self:
            serie = clasa.an + clasa.serie
            lista_elevi_medii_generale = []
            dict_cel_mai_bun_pe_clasa = {}
            nota_max = 0
            for clasa1 in clasa:
                lista_elevi_medii_generale = [clas.lista_medii_generale_pe_clasa() for clas in self]
                for elevi in lista_elevi_medii_generale:
                    cele_mai_bune_note1 = [ele['media_generala'] for ele in elevi]
                    y = max(cele_mai_bune_note1)
                    i = 0
                    while True:
                        if y == elevi[i]['media_generala']:
                            dict_cel_mai_bun_pe_clasa = {
                                "nume": elevi[i]['nume'],
                                'clasa': elevi[i]['clasa'],
                                'media': y,
                                'cel_bun_din_clasa': True
                            }
                            lista_cu_cei_mai_buni_pe_clasa.append(dict_cel_mai_bun_pe_clasa)
                            break
                        else:
                            i += 1
        return lista_cu_cei_mai_buni_pe_clasa
        # functia returneaza studentii cu mediile generale cele mai bune, pe clasa
    def cel_mai_slab_ca_si_medie_pe_clasa(self):
        lista_cu_cei_mai_slabi_pe_clasa = []
        for clasa in self:
            serie = clasa.an + clasa.serie
            lista_elevi_medii_generale = []
            dict_cel_mai_slab_pe_clasa = {}
            nota_max = 0
            for clasa1 in clasa:
                lista_elevi_medii_generale = [clas.lista_medii_generale_pe_clasa() for clas in self]
                for elevi in lista_elevi_medii_generale:
                    cele_mai_slabe_note1 = [ele['media_generala'] for ele in elevi]
                    y = min(cele_mai_slabe_note1)
                    i = 0
                    while True:
                        if y == elevi[i]['media_generala']:
                            dict_cel_mai_slab_pe_clasa = {
                                "nume": elevi[i]['nume'],
                                'clasa': elevi[i]['clasa'],
                                'media': y,
                                'cel_bun_din_clasa': True
                            }
                            lista_cu_cei_mai_slabi_pe_clasa.append(dict_cel_mai_slab_pe_clasa)
                            break
                        else:
                            i += 1
        return lista_cu_cei_mai_slabi_pe_clasa
    # functia retunreaza o lista cu studentii care au cea mai slaba medie generala pe clasa
    def medii_generale_pe_materie(self):
        y = []
        x = 0
        for clasa in self:
            x = clasa.media_clase_pe_materie()
            y.append(x)
        return y
    def clasa_cu_cei_mai_multi_olimpici(self):
        def countXY(lista, serie):
            count = 0
            for ele in lista:
                if (ele == serie):
                    count = count + 1
            return count
        clasa_cu_cei_mai_multi_olimpici_pe_an = []
        clasa_olimpici = []
        clasa1 = {}
        m = 0
        n = 0
        for clasa in self:
            x = clasa.lista_studenti_eligibili_olimpiada(clasa)
            clasa_cu_cei_mai_multi_olimpici_pe_an.append(x)
        m = [len(clasa) for clasa in clasa_cu_cei_mai_multi_olimpici_pe_an]
        n = max(m)
        if countXY(lista=m, serie=n) >= 2:
            x = [clasa for clasa in clasa_cu_cei_mai_multi_olimpici_pe_an if len(clasa) == n]
            for u in x:
                clasa1 = {
                    "clasa": u[0].clasa,
                    "Numar olimpici": n
                }
                clasa_olimpici.append(clasa1)

        else:
            y = [clasa for clasa in clasa_cu_cei_mai_multi_olimpici_pe_an if len(clasa) == n]
            z = y[0]
            clasa1 = {
                "clasa": z.clasa,
                "Numar olimpici": n
            }
            clasa_olimpici.append(clasa1)
        return clasa_olimpici
        # retunreaza o lista cu clasele cu cei mai multi olimpici pe an.
    def clasa_cu_cea_mai_mare_medie(self):
        clasa2 = []
        clase = []
        for clasa in self:
            x = clasa.media_clase_pe_materie()
            clase.append(x)
        i = 1
        for z in clase:
            u = z[0]['clasa']
            u = int(u[0])
            media_rom = [media[0]['media_clasei_romana'] for media in clase if int(media[0]['clasa'][0])== u]
            media_mate = [media[0]['media_clasei_matematica'] for media in clase if int(media[0]['clasa'][0]) == u]
            media_engl = [media[0]['media_clasei_engleza'] for media in clase if int(media[0]['clasa'][0]) == u]
            media_info = [media[0]['media_clasei_informatica'] for media in clase if int(media[0]['clasa'][0]) == u]
            media_rom= max(media_rom)
            media_mate = max(media_mate)
            media_engl = max(media_engl)
            media_info = max(media_info)
            if media_rom == z[0]['media_clasei_romana']:
                clas = {
                    'cea_mai_mare_medie_rom': media_rom,
                    'clasa': z[0]['clasa']
                }
                clasa2.append(clas)
            if media_mate == z[0]['media_clasei_matematica']:
                clas = {
                    'cea_mai_mare_medie_mate': media_mate,
                    'clasa': z[0]['clasa']
                }
                clasa2.append(clas)
            if media_info == z[0]['media_clasei_informatica']:
                clas = {
                    'cea_mai_mare_medie_info': media_info,
                    'clasa': z[0]['clasa'],
                }
                clasa2.append(clas)
            if media_engl == z[0]['media_clasei_engleza']:
                clas = {
                    'cea_mai_mare_medie_engleza': media_engl,
                    'clasa': z[0]['clasa']

                }
                clasa2.append(clas)
        return clasa2
        # este retrunata clasa, cu cea mai mare medie, pe an, in functie de materie.
    def sef_de_promotie_pe_an(self):
        lista_sefi_promotie = []
        lista_elevi_cei_mai_buni = self.cel_mai_bun_ca_si_medie_pe_clasa()
        medii_elevi_cei_mai_buni5 = [elev['media'] for elev in lista_elevi_cei_mai_buni if elev['clasa'][0] == '5']
        medii_elevi_cei_mai_buni6 = [elev['media'] for elev in lista_elevi_cei_mai_buni if elev['clasa'][0] == '6']
        medii_elevi_cei_mai_buni7 = [elev['media'] for elev in lista_elevi_cei_mai_buni if elev['clasa'][0] == '7']
        medii_elevi_cei_mai_buni8 = [elev['media'] for elev in lista_elevi_cei_mai_buni if elev['clasa'][0] == '8']
        sef_promotie5 = [elev for elev in lista_elevi_cei_mai_buni if elev['clasa'][0] == '5' and elev['media'] == max(medii_elevi_cei_mai_buni5)]
        sef_promotie6 = [elev for elev in lista_elevi_cei_mai_buni if
                         elev['clasa'][0] == '6' and elev['media'] == max(medii_elevi_cei_mai_buni6)]
        sef_promotie7 = [elev for elev in lista_elevi_cei_mai_buni if
                         elev['clasa'][0] == '7' and elev['media'] == max(medii_elevi_cei_mai_buni7)]
        sef_promotie8 = [elev for elev in lista_elevi_cei_mai_buni if
                         elev['clasa'][0] == '8' and elev['media'] == max(medii_elevi_cei_mai_buni8)]
        lista_sefi_promotie = [sef_promotie5, sef_promotie6, sef_promotie7, sef_promotie8]
        return lista_sefi_promotie
    # este returnata o lista cu sefii de promotie pe fiecare an
    def clasa_cu_cei_mai_multi_olimpici_pe_an(self):
        lista_clase_olimpici = []
        lista_studenti_olimpici = self.clasa_olimpici()
        an5 = [clasa for clasa in lista_studenti_olimpici
               if clasa['clasa'][0] == '5' and max([clasu['numar_olimpici'] for clasu in lista_studenti_olimpici
                                                    if clasu['clasa'][0] == '5']) == clasa['numar_olimpici']]
        an6 = [clasa for clasa in lista_studenti_olimpici
               if clasa['clasa'][0] == '6' and max([clasu['numar_olimpici'] for clasu in lista_studenti_olimpici
                                                    if clasu['clasa'][0] == '6']) == clasa['numar_olimpici']]
        an7 = [clasa for clasa in lista_studenti_olimpici
               if clasa['clasa'][0] == '7' and max([clasu['numar_olimpici'] for clasu in lista_studenti_olimpici
                                                    if clasu['clasa'][0] == '7']) == clasa['numar_olimpici']]
        an8 = [clasa for clasa in lista_studenti_olimpici
               if clasa['clasa'][0] == '8' and max([clasu['numar_olimpici'] for clasu in lista_studenti_olimpici
                                                    if clasu['clasa'][0] == '8']) == clasa['numar_olimpici']]
        dictio = {
            'an5': '5',
            'clasa_cu_cei_mai_multi_olimpici_an_5': an5,
            'an6': '6',
            'clasa_cu_cei_mai_multi_olimpici_an_6': an6,
            'an7': '7',
            'clasa_cu_cei_mai_multi_olimpici_an_7': an7,
            'an8': '8',
            'clasa_cu_cei_mai_multi_olimpici_an_8': an8
        }
        lista_clase_olimpici.append(dictio)
        return lista_clase_olimpici

    # returneaza clasa cu cei mai multi olimpici pe an
