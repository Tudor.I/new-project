import csv, re, requests, json


with open("note.csv") as f:
    reader = csv.DictReader(f)
    note_totale = [dict(row) for row in reader]
with open("studenti.csv") as n:
    reader = csv.DictReader(n)
    elevi_scolii = [dict(row) for row in reader]




if __name__ == "__main__":
    #pentru fiecare student din scoala, (id, clasa, nume, prenume)
    #1trebuie sa calculam media la matematica de genlul Soanca andrei are media la mate x
    #2iar mai apoi, sa calculam media la romana pe acelasi principiu
    #3mai apoi, daca vreun student, are media mai mare de 9,30, la vreo materie
    # sa spunem daca este eligibil pentru olimpiada sau nu
    #4si in final, la fiecare student, sa calculam media, la toate disciplinele
    #si sa returnam materia care are media cea mai mare
    """elevii_scolii, note_totale"""




    # PRima incercare.. esuata de asemenea


    
    # #1-> Media matematica

    elevii_cu_media_la_mate = []
    for elev in elevi_scolii:
        nota_matematica = [int(note["nota"]) for note in note_totale if note['materie'] == 'matematica' and elev['Index'] == note['index']]
        media_matematica = sum(nota_matematica) / len(nota_matematica)
        elev_medie_matematica ={
            "index": elev["Index"],
            "nume": elev["Nume"],
            "clasa": elev["Clasa"],
            "media_matematica": round(media_matematica, 3)
        }
        elevii_cu_media_la_mate.append(elev_medie_matematica)
    # with open("medii_matematica.csv", "w+") as f:
    #     n = []
    #     writer = csv.DictWriter(f, fieldnames=["index", "nume", "clasa", "media_matematica"])
    #     writer.writeheader()
    #     writer.writerows(dict(rows) for rows in elevii_cu_media_la_mate)
    # print(elevii_cu_media_la_mate)

    #2 Elevii cu media la romana
    # 1-> Media matematica
    elevii_cu_media_la_romana = []
    for elev in elevi_scolii:
        nota_romana = [int(note["nota"]) for note in note_totale if note['materie'] == 'romana' and elev['Index'] == note['index']]
        media_romana = sum(nota_romana) / len(nota_romana)
        elev_medie_romana = {
            "index": elev["Index"],
            "nume": elev["Nume"],
            "clasa": elev["Clasa"],
            "media_romana": round(media_romana, 3)
        }
        elevii_cu_media_la_romana.append(elev_medie_romana)
    # print(elevii_cu_media_la_romana)
    #     with open("medii_romana.csv", "w+") as f:
    #         writer = csv.DictWriter(f, fieldnames=["index", "nume", "clasa", "media_romana"])
    #         writer.writeheader()
    #         writer.writerows(dict(rows) for rows in elevii_cu_media_la_romana)
    # def cauta_materii_peste_9():



    # eligibil pentru olimpiada
    eligibili_pentru_olimpiada = []
    i = 0
    for elev in elevi_scolii:
        i+=1
        nota_romana2 = [int(note["nota"]) for note in note_totale if note['materie'] == 'romana'  and elev['Index'] == note['index']]
        nota_matematica2 = [int(note["nota"]) for note in note_totale if note['materie'] == 'matematica'  and elev['Index'] == note['index']]
        nota_informatica = [int(note["nota"]) for note in note_totale if note['materie'] == 'informatica'  and elev['Index'] == note['index']]
        nota_engleza = [int(note["nota"]) for note in note_totale if note['materie'] == 'engleza'  and elev['Index'] == note['index']]
        media_romana2 = sum(nota_romana2) / len(nota_romana2)
        media_matematica2 = sum(nota_matematica2) / len(nota_matematica2)
        media_informatica = sum(nota_informatica) / len(nota_informatica)
        media_engleza = sum(nota_engleza) / len(nota_engleza)
        if media_romana2 >= 9.3:
            elev_olimpic_romana = {
                "index": elev["Index"],
                "nume": elev["Nume"],
                "prenume": elev["Prenume"],
                'clasa': elev['Clasa'],
                'materie':'romana',
                'nota': media_romana2
            }
            eligibili_pentru_olimpiada.append(elev_olimpic_romana)
        if media_engleza >= 9.3:
            elev_olimpic_engleza = {
                "index": elev["Index"],
                "nume": elev["Nume"],
                "prenume":elev["Prenume"],
                'clasa': elev['Clasa'],
                'materie': 'engleza',
                'nota': media_engleza
            }
            eligibili_pentru_olimpiada.append(elev_olimpic_engleza)
        if media_informatica >= 9.3:
            elev_olimpic_informatica = {
                "index": elev["Index"],
                "nume": elev["Nume"],
                "prenume": elev["Prenume"],
                'clasa': elev['Clasa'],
                'materie': 'informatica',
                'nota': media_informatica
            }
            eligibili_pentru_olimpiada.append(elev_olimpic_informatica)
        if media_matematica2 >= 9.3:
            elev_olimpic_matematica = {
                "index": elev["Index"],
                "nume": elev["Nume"],
                "prenume": elev["Prenume"],
                'clasa': elev['Clasa'],
                'materie': 'matematica',
                'nota': media_matematica2
            }
            eligibili_pentru_olimpiada.append(elev_olimpic_matematica)

    print(eligibili_pentru_olimpiada)