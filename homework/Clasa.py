from homework.student import Student
from homework.Nota import Nota


class Clasa(Student):
    def __init__(self):
        # super().__init__()????
        self.an = ""
        self.serie = ""
        self.elevi = []
    def reprezentare_clasa_studenti(self):
        return f"Anul: {self.an} Serie: {self.serie}"
    def nume_clasa(self):
        return f"{self.an}{self.serie}"
        """nume_clasa ofera reprezentarea clasei"""

    def adaugare_clasa(self, an, serie):
        self.an = str(an)
        self.serie = serie
        """Adauga parametrii respectivi instantei de clasa """
    def adaugare_studenti(self, student):
        self.elevi.append(student)
        """Adauga fiecare instanta de student in lista de studenti"""
    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.elevi):
            result = self.elevi[self.n]
            self.n += 1
            return result
        else:
            raise StopIteration


    def lista_studenti_eligibili_olimpiada(self, clasa):
        lista_studenti_olimpiada = []
        for elev in clasa:
            for elev1 in elev:
                if elev1.eligibil_olimpiadaa():
                    lista_studenti_olimpiada.append(elev1)
        return lista_studenti_olimpiada
        """Returneaza o lista cu studentii eligibili din "clasa", functie ce ulterior va fi folosita in Clasa Scoala."""
    def countX(self, list, serie):
        count = 0
        for ele in list:
            if (ele == serie):
                count = count + 1
        return count
        """Functia count returneaza numarul aparitiei parametrului 'serie' in 'list'
        Functia a fost folosita ulterior, in determinarea clasei cu cei mai multi olimpici pe an"""

    def media_clase_pe_materie(self):
        serie = self.an + self.serie
        media_clasei_romana = []
        media_clasei_matematica = []
        media_clasei_informatica = []
        media_clasei_engleza = []
        m = 0
        y = 0
        u = 0
        media_clasei = {}
        lista_clasa_medii = []
        for elev in self.elevi:
            for elev1 in elev:
                if elev1.clasa == serie:
                    med_rom = elev1.media_romana()
                    med_info = elev1.media_informatica()
                    med_engl = elev1.media_engleza()
                    med_mate = elev1.media_matematica()
                    media_clasei_romana.append(med_rom)
                    media_clasei_informatica.append(med_info)
                    media_clasei_engleza.append(med_engl)
                    media_clasei_matematica.append(med_mate)
            media_clasei_romana = sum(media_clasei_romana) / len(media_clasei_romana)
            media_clasei_informatica = sum(media_clasei_informatica) / len(media_clasei_informatica)
            media_clasei_matematica = sum(media_clasei_matematica) / len(media_clasei_matematica)
            media_clasei_engleza = sum(media_clasei_engleza) / len(media_clasei_engleza)
            media_clasei = {
                    "clasa": serie,
                    "media_clasei_romana": media_clasei_romana,
                    "media_clasei_informatica": media_clasei_informatica,
                    "media_clasei_engleza": media_clasei_engleza,
                    "media_clasei_matematica": media_clasei_matematica,
                 }
            lista_clasa_medii.append(media_clasei)
        return lista_clasa_medii

        """ Functia returneaza media pe ficare materie din clasa, in functie de an, a fiecarui student.""""

