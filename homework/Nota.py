class Nota:
    def __init__(self):
        self.id_student = 0
        self.valoare = 0
        self.materie = ""
    def adaugare_valorile_notei(self, id_student, valoare, materie):
        self.id_student = id_student
        self.valoare = valoare
        self.materie = materie
    def reprezentarea_notei(self):
        return f"ID student: {self.id_student} Valoarea notei: {self.valoare} Materia: {self.materie}"


