from homework.Nota import Nota
class Student(Nota):
    def __init__(self):
        # super().__init__()
        self.id = 0
        self.clasa = ""
        self.nume = ""
        self.prenume = ""
        self.nota = []
    def reprezentare_student(self):
        return f"Id: {self.id} Clasa: {self.clasa} Nume: {self.nume} Prenume: {self.prenume} Nota: {self.nota}"
    def adaugare_studentii(self, id, clasa, nume, prenume):
        self.id = id
        self.clasa = clasa
        self.nume = nume
        self.prenume = prenume
    def adaugare_nota_la_student(self, nota):
        self.nota.append(nota)
    def media_materie(self, materie):
        note = [nota.valoare for nota in self.nota if nota.materie == materie]
        return sum(note[0]) / len(note[0])
    def media_matematica(self):
        return self.media_materie("matematica")

    def media_romana(self):
        return self.media_materie("romana")

    def media_engleza(self):
        return self.media_materie("engleza")

    def media_informatica(self):
        return self.media_materie("informatica")

    def eligibil_olimpiadaa(self):
        eligibili_olimpiada = []
        if self.media_engleza() >= 9.3:
            eligibili_olimpiada.append(self)
        elif self.media_informatica() >= 9.3:
            eligibili_olimpiada.append(self)
        elif self.media_romana() >= 9.3:
            eligibili_olimpiada.append(self)
        elif self.media_matematica() >= 9.3:
            eligibili_olimpiada.append(self)
        return eligibili_olimpiada

    def materia_preferata(self):
        u = []
        m = []
        materia = []
        media_romana2 = self.media_romana()
        media_matematica2 = self.media_matematica()
        media_informatica2 = self.media_informatica()
        media_engleza2 = self.media_engleza()
        m = [media_engleza2, media_informatica2, media_romana2, media_matematica2]
        media_maxima = max(m)
        if media_matematica2 == media_maxima:
            u = self
            m = media_maxima
            materia = "matematica"
        elif media_romana2 == media_maxima:
            u = self
            m = media_maxima
            materia = "romana"
        elif media_engleza2 == media_maxima:
            u = self
            m = media_maxima
            materia = "engleza"
        elif media_informatica2 == media_maxima:
            u = self
            m = media_maxima
            materia = "informatica"
        dictionar = {
            "Student": u,
            "nota_preferata": m,
            "materia": materia
        }
        return dictionar

    def media_cea_mai_slaba(self):
        media_romana3 = self.media_romana()
        media_matematica3 = self.media_matematica()
        media_informatica3 = self.media_informatica()
        media_engleza3 = self.media_engleza()
        u = []
        m = []
        materia = []
        dictionar = {}
        m = [media_engleza3, media_informatica3, media_romana3, media_matematica3]
        media_minima = min(m)
        if media_matematica3 == media_minima:
            u = self
            m = media_minima
            materia = "matematica"
        elif media_romana3 == media_minima:
            u = self
            m = media_minima
            materia = "romana"
        elif media_engleza3 == media_minima:
            u = self
            m = media_minima
            materia = "engleza"
        elif media_informatica3 == media_minima:
            u = self
            m = media_minima
            materia = "informatica"
        dictionar = {
            "Student": u.reprezentare_student(),
            "nota_preferata": m,
            "materia": materia
        }
        return dictionar



