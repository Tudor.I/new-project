from homework.Clasa import Clasa
from homework.scoala import Scoala
from homework.Nota import Nota
from homework.student import Student
import time
import csv
with open("note.csv") as f:
    reader = csv.DictReader(f)
    note_totale = [dict(row) for row in reader]
with open("studenti.csv") as n:
    reader = csv.DictReader(n)
    elevii_scolii = [dict(row) for row in reader]

scoala = Scoala()
start_time = time.time()
scoala.incarcare_studenti(lista_elevi=elevii_scolii, lista_note=note_totale)
delta_time = time.time() - start_time

# aici testez metodele si outcome ul lor
if __name__ == "__main__":
    print(scoala.masurare_incarcare_studenti(lista_note=note_totale, lista_elevi=elevii_scolii))
