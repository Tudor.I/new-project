suma_cheltuieli = 0
from datetime import datetime
from datetime import date, time
def adaugare_cheltuieli():
    lista = []
    x = float(input("Adaugati cheltuieli: "))
    lista.append(x)
    y = input("Doriti sa mai adaugati cheltuieli: ?")
    if y != 'q':
        while True:
            x = float(input("Adaugati cheltuieli: "))
            lista.append(x)
            y = input("Doriti sa mai introduceti cheltuieli? (press q for exit): ?")
            if y == 'q':
                break
            else:
                continue
    else:
        pass
    with open("cheltuieli.txt", "a+") as f:
        data_start = date(year=2020, month=10, day=15)
        curent_date = date.today()
        curent_date1 = datetime.now()
        interval_timp = curent_date - data_start
        f = f.write(f"Cuantum cheltuieli: {sum(lista)} lei  |   Bilant facut in data de: {curent_date1.strftime('%d %b , %Y - %H:%M:%S')}   | Pe un interval de timp de: {interval_timp} \n")
    return f"{sum(lista)}"
if __name__ == "__main__":
    print(adaugare_cheltuieli())