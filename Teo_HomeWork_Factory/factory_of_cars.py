
from Teo_HomeWork_Factory.Car_Factory import Dacia, Audi, Tesla

class Factory:
    def __init__(self):
        self.masina = "Masina"
    def comanda_masina(self):
        tip_masina = input("Va rog sa introduceti un model de masina:")
        tip_masina = tip_masina.strip()
        # assert <'expression'>, "Assertion Error" -> plecam de la prezumtia ca expresia trebuie sa fie mereu adevarata,
        # iar daca nu se confirma expresia, se va ridica un Assertion Error
        assert tip_masina.lower() in ['dacia', 'audi', 'tesla'], "Ati introdus o masina ce nu se gaseste in showroom-ul nostru"
        if tip_masina.lower() == 'dacia':
            return Dacia()
        elif tip_masina.lower() == 'audi':
            return Audi()
        elif tip_masina.lower() == 'tesla':
            return Tesla()

if __name__ == "__main__":

    masina = Factory.comanda_masina(Factory)
    print(masina.test_drive())
    print(masina.tip_combustibil())
    print(type(masina))