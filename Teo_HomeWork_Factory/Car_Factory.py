from Teo_HomeWork_Factory.Car_framework import Masina

class Dacia(Masina):
    def __init__(self):
        super().__init__('Dacia', 'benzina')

class Audi(Masina):
    def __init__(self):
        super().__init__('Audi', 'motorina')

class Tesla(Masina):
    def __init__(self):
        super().__init__('Tesla', 'electric')

if __name__ == "__main__":
    dacia = Dacia()
    print(dacia.tip_combustibil())
    print(dacia.test_drive())
    tesla = Tesla()
    print(tesla.test_drive())


