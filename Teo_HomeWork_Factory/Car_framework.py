class Masina:
    def __init__(self, model, combustibil):
        self.model = model
        self.combustibil = combustibil

    def tip_combustibil(self):
        return f'{self.model} ruleaza cu {self.combustibil}'

    def test_drive(self):
        return f'Facem test drive cu {self.model}, avand combustie pe {self.combustibil}'
