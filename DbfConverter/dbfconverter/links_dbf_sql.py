from dbfread import DBF
import pyodbc


def links_dbf():
    return DBF("links.dbf")

def generare_sql():
    data = links_dbf()
    for m in data:
        print(len(dict(m).keys()))
        break

if __name__ == "__main__":
    generare_sql()
