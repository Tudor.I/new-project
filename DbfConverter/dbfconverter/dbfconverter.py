import pyodbc
from pyodbc import ProgrammingError
from dbfpy3.dbf import Dbf


def conexiune_dbf():
    dbf = Dbf("links.dbf", ignore_errors=True)
    for rec in dbf:
        for fldname in dbf.field_names:
            print("%s    -    %s     " %(fldname.decode(), rec[fldname]))
    dbf.close()


"""def links_dbf():
    return DBF("links.dbf")"""


def conexiune_db():
    connection = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                                "Server=DESKTOP-VTKFRFL\SQLEXPRESS;"
                                "Database=links;"
                                "Trusted_Connection=yes;")
    return connection.cursor()


def creare_manual_dbf():
    conn_SQL = conexiune_db()
    dict_dbf = conexiune_dbf()
    counter = 0
    for dic in dict_dbf:
        if counter == 2:
            break
        manual = dict(dic)["MANUAL"].strip() if dict(dic)["MANUAL"] is not None else ''
        link_pdf = dict(dic)["LINK_PDF"].strip() if dict(dic)["LINK_PDF"] is not None else ''
        link_md = dict(dic)["LINK_MD"].strip() if dict(dic)["LINK_MD"] is not None else ''
        isbn = dict(dic)["ISBN5"].strip() if dict(dic)["ISBN5"] is not None else ''
        isbn_long = dict(dic)["ISBN1"].strip() if dict(dic)["ISBN1"] is not None else ''
        clasa = dict(dic)["CLASA"] if dict(dic)["CLASA"] is not None else ''
        limba = dict(dic)["LIMBA"].strip() if dict(dic)["LIMBA"] is not None else ''
        qr_code = dict(dic)["QR_CODE"].strip() if dict(dic)["QR_CODE"] is not None else ''
        pdf = dict(dic)["PDF"].strip() if dict(dic)["PDF"] is not None else ''
        jpg = dict(dic)["JPG"].strip() if dict(dic)["JPG"] is not None else ''
        qr = dict(dic)["QR_CODE"].strip() if dict(dic)["QR_CODE"] is not None else ''
        mp3 = dict(dic)["MP3"].strip() if dict(dic)["MP3"] is not None else ''
        html = dict(dic)["HTML"].strip() if dict(dic)["HTML"] is not None else ''
        pag_start = dict(dic)["PAG_START"] if dict(dic)["PAG_START"] is not None else ''
        pag = dict(dic)["PAG"] if dict(dic)["PAG"] is not None else ''
        pag_man = dict(dic)["PAG_MAN"] if dict(dic)["PAG_MAN"] is not None else ''
        title = dict(dic)["TITLE"].strip() if dict(dic)["TITLE"] is not None else ''
        title_edu = dict(dic)["TITLE_EDU"].strip() if dict(dic)["TITLE_EDU"] is not None else ''
        if title == "Jussike's seven friends":
            title = "Jussikes seven friends"
        authors = dict(dic)["AUTHORS"].strip() if dict(dic)["AUTHORS"] is not None else ''
        if authors == "Felycity O'Dell, Annie Broathead":
            authors = "Felycity ODell, Annie Broathead"
        if authors == "Felicity O'Dell, Annie Broadhead":
            authors = "Felicity ODell, Annie Broadhead"
        erori_pdf = dict(dic)["ERORI_PDF"].strip() if dict(dic)["ERORI_PDF"] is not None else ''
        size_pdf = dict(dic)["SIZE_PDF"] if dict(dic)["SIZE_PDF"] is not None else ''
        tip_md = dict(dic)["TIP_MD"].strip() if dict(dic)["TIP_MD"] is not None else ''
        editura = dict(dic)["EDITURA"].strip() if dict(dic)["EDITURA"] is not None else ''
        string = "INSERT INTO dbo.manuale VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'," \
                 "'{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'," \
                 "'{}','{}');".format(manual, link_pdf, link_md, isbn, isbn_long, clasa, limba, qr_code, pdf,
                                      jpg, qr, mp3, html, pag_start, pag, pag_man, title, title_edu, authors, erori_pdf,
                                      size_pdf, tip_md, editura)
        conn_SQL.execute(string)
    conn_SQL.commit()


def manuale_pag_dbf_to_dict():
    return DBF("manuale_pag.dbf",)


def creare_manuale_pag_dbf():
    links = links_dbf()
    conn = conexiune_db()
    keys = []
    for dic in links:
        keys.extend(dict(dic).keys())
        break
    stergere = "DROP TABLE IF EXISTS Links"
    conn.execute(stergere)
    conn.commit()
    creare_tabel = "CREATE TABLE Links("
    counter = 1
    for col in keys:
        if counter == len(keys):
            creare_tabel += f"{col} VARCHAR(500) Null"
            break
        creare_tabel += f"{col if col != 'USER' else 'userr'} VARCHAR(500) Null,"
        counter += 1
    creare_tabel += ");"
    print(creare_tabel)
    conn.execute(creare_tabel)
    conn.commit()
    return


""""""

def introducere_date():
    """Succesful function for importing data from dbf to SQL"""
    dbf = Dbf("links.dbf", ignore_errors=True)
    conn = conexiune_db()
    values = []
    error_values = []
    for dic in dbf:
        counter = 0
        insert_string = "INSERT INTO Links VALUES("
        for fld in dbf.field_names:
            if counter < 26:
                insert_string += f"'{dic[fld]}',"
            else:
                insert_string += f"'{dic[fld]}');"
            counter += 1
        print(insert_string)
        try:
            conn.execute(insert_string)
            conn.commit()
        except:
            error_values.append(insert_string)
            print("ERROARE", insert_string)
            values = []
    dbf.close()
    if len(error_values) > 0:
        with open("error_files.txt", "w+", encoding="UTF-8") as f:
            f.writelines(error_values)

if __name__ == "__main__":
    introducere_date()
