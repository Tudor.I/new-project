'''
AUTOR: Bița Răzvan-Nicolae
DATA: 10/01/2021
Aplicatie care ne permite partajarea si adaugarea de link-uri intr-o baza de date SQLite salvata pe Google Drive sub numele links.db
Inainte de a rula codul:
WINDOWS CMD: pip install pysimplegui
WINDOWS CMD: pip install sqlite3
*modulul webbrowser este preinstalat
'''
import PySimpleGUI as sg
import webbrowser
import sqlite3
#Alegem tema aplicatiei
sg.theme('Light Gray1')
#Creem o conexiune cu baza de date
connection = sqlite3.connect('links.db')
cursor = connection.cursor()
#Selectam datele din aceasta intr-o variabile
cursor.execute("SELECT * FROM links")
rows = cursor.fetchall()
#Ne ocupam de interfata aplicatiei si de structura acesteia prin lungimea tabelului, adaugarea butoanelor etc..
lungime = len(rows)  + 10
headings = ["Tags", "Title", "Link"]
data = [[j for j in range(3)] for i in range(lungime)]
lista = [[j for j in range(3)] for i in range(lungime)]
i = 1

for row in rows:
    data[i][0] = row[0]
    data[i][1] = row[1]
    data[i][2] = row[2]
    i += 1
#Structura este luata dintr-un exemplu din demo-urile modulului PySimpleGUI
#https://github.com/PySimpleGUI/PySimpleGUI/tree/master/DemoPrograms
#Pentru a intelege mai bine recomand si documentia
#https://pysimplegui.readthedocs.io/en/latest/
layout = [[sg.Table(values=data[1:][:], headings=headings, max_col_width=80,
                    col_widths=[30,150, 50],
                    vertical_scroll_only= False,
                    display_row_numbers=True,
                    justification='left',
                    num_rows=15,
                    alternating_row_color='lightyellow',
                    key='-TABLE-',
                    row_height=35,
                    enable_events=True,
                    )],
            [sg.Button('Acceseaza'), sg.Button('Adauga'), sg.Button('Filtrare'), sg.Button(button_text='Revenire la original', disabled=True, key = '-REV-')],
            [sg.Text('Tags:')],
            [sg.Input(enable_events=True, key = '-TAGS-', size=(80, 1))],
            [sg.Text('Title:')],
            [sg.Input(enable_events=True, key='-TITLE-', size=(80, 1))],
            [sg.Text('Link:')],
            [sg.Input(enable_events=True, key='-LINK-', size=(80, 1))],
            [sg.Text('Acceseaza = Acceseaza link-ul ales                       Filtrare = Cauta in tabel si afiseaza doar randurile in care s-a gasit')],
            [sg.Text('Adauga = Adauga valorile din campurile de input       Revenire la original = Daca s-a folosit filtrarea se foloseste pentru a arata din nou datele')]]
window = sg.Window('The Links Table', layout)
#Pentru a maximiza fereastra trebuie sa o finalizam prima data ??
window.finalize()
window.maximize()
filtrat = False
while True:
    #Citim evenimentele si valorile in permanenta(Interactiunea utilizatorului)
    event, values = window.read()
    if event == sg.WIN_CLOSED:
        break
    #Daca apasam butonul acceseaza se verifica daca rezultatele sun filtrate sau nu pentru ca sunt 2 liste separate
    #si trebuie sa stie de unde acceseaza
    if event == 'Acceseaza':
        if filtrat is False:
            try:
                link = data[values['-TABLE-'][0] + 1][2]
                webbrowser.open_new(link)
            except:
                sg.Popup("Nu s-a putut accesa pagina!")
        else:
            try:
                link = lista[values['-TABLE-'][0]][2]
                webbrowser.open_new(link)
            except:
                sg.Popup("Nu s-a putut accesa pagina!")
    #Daca se apasa butonul adauga se va adauga in tabel ca si in baza de date campurile  completate in imputuri
    if event == 'Adauga':
        tags = values['-TAGS-']
        titlu = values['-TITLE-']
        url = values['-LINK-']
        tp = (tags, titlu, url)
       #Executam comanda specifica SQLite si salvam operatia(commit)
        cursor.execute("INSERT INTO links VALUES (?,?,?)", tp)
        connection.commit()
        data[i][0] = tags
        data[i][1] = titlu
        data[i][2] = url
        i = i + 1
        #Modificam valorie din tabel pentru a aparea si noul rand introdus
        window['-TABLE-'].update(values=data[1:][:])
        window['-TAGS-'].update("")
        window['-LINK-'].update("")
        window['-TITLE-'].update("")
    #Daca apasam butonul filtrare se vor cauta toate aparitiile unui string in tot tabelul si va fi afisat fiecare rand
    #care contine string-ul cel putin o data.
    if event == 'Filtrare':
        contor = 0
        filtru = sg.PopupGetText('Ce doriti sa cautati?')
        for rec in data:
            if str(rec[0]).find(filtru) != -1:
                lista[contor][0] = rec[0]
                lista[contor][1] = rec[1]
                lista[contor][2] = rec[2]
                contor += 1
                continue
            if str(rec[1]).find(filtru) != -1:
                lista[contor][0] = rec[0]
                lista[contor][1] = rec[1]
                lista[contor][2] = rec[2]
                contor += 1
                continue
            if str(rec[2]).find(filtru) != -1:
                lista[contor][0] = rec[0]
                lista[contor][1] = rec[1]
                lista[contor][2] = rec[2]
                contor += 1
                continue
        #Din nou modificam datele din tabel deoarece "jonglam" intre doua variabile, pentru a nu pierde nici originalul
        window['-TABLE-'].update(values=lista[:][:])
        filtrat = True
        window['-REV-'].update(disabled = False)
    #Daca este apast butonul "Revenire.." toate datele sunt din nou afisate
    if event == '-REV-':
        lista.clear()
        window['-TABLE-'].update(values=data[1:][:])
        filtrat = False
        window['-REV-'].update(disabled=True)