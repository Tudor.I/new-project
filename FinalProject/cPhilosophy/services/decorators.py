import functools
from mainpages.models import *
from django.shortcuts import render
from mainpages.forms import *

def decorator_cart(funct):
    """Used to decorate my_form_view function"""
    @functools.wraps(funct)
    def wrap(request, *args, **kwargs):
        if request.method == "GET":
            cart_items = UserItem.objects.all()
            if len(cart_items) == 0:
                ctx = {"error": "Chart is empty. You cannot checkout!"}
                return render(request, "cart.html", ctx)
            else:
                return funct(request)
    return wrap


def refresh_decorator(funct):
    @functools.wraps(funct)
    def wrap(request, *args, **kwargs):
        if request.method == "GET":
            temporary = TemporaryOrder.objects.last()
            temporary.delete()
            context = {'refresh':True}
            return render(request, "landing.html", context=context)
        else:
            return funct(request)
    return wrap