from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, Order, Products
import re


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    email = forms.EmailField(max_length=254, help_text='Enter a valid email address')
    street = forms.CharField(max_length=50, required=True, help_text='Required')
    number = forms.CharField(max_length=50, required=True, help_text='Required')
    city = forms.CharField(max_length=50, required=True, help_text='Required')
    country = forms.CharField(max_length=50, required=True, help_text='Required')
    zip_code = forms.CharField(max_length=50, required=True, help_text='Required')
    phone_number = forms.CharField(max_length=15, required=False, help_text="Optional")
    profile_image = forms.ImageField(required=False, help_text="Optional")


    def clean(self):
        super(SignUpForm,self).clean()
        emails = CustomUser.objects.values_list('email', flat=True)
        usernames = CustomUser.objects.values_list('username', flat=True)
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get("username")
        if email in emails:
            self.add_error('email', f"'{email}' is already taken. Please insert another email")
        if username in usernames:
            self.add_error('username', f"{username} is already taken. Please insert another username")


    class Meta:
        model = CustomUser
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
            'street',
            'number',
            'city',
            'country',
            'zip_code',
            'profile_image',
        ]

class OrderForm(forms.Form):
    first_name = forms.CharField(max_length=30,  help_text='Required')
    last_name = forms.CharField(max_length=30,  help_text='Required')
    email = forms.EmailField(max_length=254, help_text='Enter a valid email address')
    street = forms.CharField(max_length=50,  help_text='Required')
    number = forms.CharField(max_length=50, required=True, help_text='Required')
    city = forms.CharField(max_length=50, help_text='Required')
    country = forms.CharField(max_length=50, help_text='Required')
    zip_code = forms.CharField(max_length=50, required=True, help_text='Required')
    phone_number = forms.CharField(max_length=16, help_text="Required")
    choices = [("cash", "On delivery, cash"), ("online", "via online payment",)]
    payment = forms.ChoiceField(choices=choices, widget=forms.Select)


    def clean(self):
        super(OrderForm, self).clean()
        # phone_number = self.cleaned_data.get('phone_number')
        # if (not re.match("^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", phone_number)):
        #     self.add_error("phone_number", "Invalid number")
        number = self.cleaned_data.get("zip_code")
        if re.match(r"[A-Za-z]", number):
            self.add_error('zip_code', "Invalid zip code")






class ProfileForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'street',
            'number',
            'city',
            'country',
            'zip_code',
            'profile_image',
            ]

class QuantityForm(forms.Form):
    quantity = forms.IntegerField


class ReviewForm(forms.Form):
    score = forms.IntegerField(min_value=1, max_value=5, help_text="The score cannot be less than 1 and greater than 5.")
    text = forms.CharField(widget=forms.Textarea, max_length=800)
