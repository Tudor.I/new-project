from django.db import models
from django.contrib.auth.models import  AbstractUser
# Create your models here.
from datetime import datetime

class CustomUser(AbstractUser):
    street = models.CharField(max_length=50, blank=True, null=True)
    number = models.IntegerField(blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    zip_code =  models.IntegerField(blank=True, null=True)
    phone_number = models.CharField(max_length=20)
    profile_image = models.ImageField(upload_to='gallery',blank=True, null=True, default="profiledef.png")

    def __str__(self):
        return self.username


class Categories(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField(max_length=300, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class SubCategories(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    image = models.ImageField(upload_to='gallery', blank=True, null=True)
    par_category = models.ForeignKey(to=Categories, on_delete=models.CASCADE, blank=True, null=True, related_name="Category")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Subcategory"
        verbose_name_plural = "Subcategories"
        permissions = (
            ("can_view", "Can view the category"),
            ("can_create", "Can create a category"),
            ("can_delete", "Can delete a category"),
            ("can_update", "Can update a category"),
        )


class Products(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    description = models.TextField(max_length=300, blank=True, null=True)
    resealed = models.BooleanField(blank=True, null=True)
    category = models.ForeignKey(to=SubCategories, on_delete=models.CASCADE, blank=True, null=True)
    image = models.ImageField(upload_to='gallery', blank=True, null=True)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        permissions = (
            ("can_view", "Can view the product"),
            ("can_create", "Can create a product"),
            ("can_delete", "Can delete a product"),
            ("can_update", "Can update a product"),
        )


class UserItem(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    img = models.ImageField(upload_to='gallery', blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    prod_id = models.ForeignKey(to=Products, on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return self.name

    @classmethod
    def total(self, items):
        total = 0
        for itm in items:
            total += itm.quantity * itm.price
        return total

    @classmethod
    def VAT(self, total):
        return round(0.19 * total, 2)

    @classmethod
    def shipment(self, total):
        return 10 if total <= 600 else "FREE"

class Order(models.Model):
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    street = models.CharField(max_length=50, blank=True, null=True)
    number = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    zip_code = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    value = models.FloatField(blank=True, null=True)
    vat = models.FloatField(blank=True, null=True)
    shipment = models.CharField(max_length=30, blank=True, null=True)
    date_and_time  = models.DateTimeField(default=datetime.now)
    user = models.ForeignKey(to=CustomUser, on_delete=models.DO_NOTHING, blank=True, null=True)
    payment_status = models.BooleanField(default=False)
    url = models.URLField(blank=True, null=True)
    payment_method = models.CharField(max_length=40, blank=True, null=True)



class TemporaryOrder(models.Model):
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    street = models.CharField(max_length=50, blank=True, null=True)
    number = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    zip_code = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    value = models.FloatField(blank=True, null=True)
    vat = models.FloatField(blank=True, null=True)
    shipment = models.CharField(max_length=30, blank=True, null=True)
    date_and_time = models.DateTimeField(default=datetime.now)
    user = models.ForeignKey(to=CustomUser, on_delete=models.DO_NOTHING, blank=True, null=True)
    payment_status = models.BooleanField(default=False)
    url = models.URLField(blank=True, null=True)




class OrderedItem(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    order_id = models.ForeignKey(to=Order, on_delete=models.CASCADE, blank=True, null=True)
    id_prod = models.ForeignKey(to=Products, on_delete=models.CASCADE, blank=True, null=True)


class Review(models.Model):
    score = models.FloatField()
    text = models.TextField(max_length=500, blank=True, null=True)
    product = models.ForeignKey(to=Products, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(to=CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(auto_now_add=True)
    update = models.DateField(auto_now=True)
    updated = models.BooleanField(default=False)

    def __str__(self):
        return str(self.product)


