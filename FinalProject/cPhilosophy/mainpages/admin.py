from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import *
from .models import *
# Register your models here.
admin.site.register(SubCategories)
admin.site.register(Products)
admin.site.register(Review)
admin.site.register(Categories)

class CustomUserAdmin(UserAdmin):
    add_form = SignUpForm
    form = ProfileForm
    model = CustomUser
    list_display = ['email', 'username',]

admin.site.register(CustomUser, CustomUserAdmin)