from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # Index URL

    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'),
         name='password_reset'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html')),

         path('', views.HomePage.as_view(), name='home'),

         # SubCategory URL
         path('categories/<int:par_category>/', views.SubCategory.as_view(), name='computer_science'),

         # Products view URL that correspond to a SubCategory
         path('products/<int:category_pk>/list-products/', views.ProductsView.as_view(), name='category_products_list'),

         # URL for visualizing the product details
         path('details/<int:pk>/', views.ProductDetails.as_view(), name="prod_details"),

         # Product CRUD URLs for Admin Pannel
         path("product-view", views.ProductList.as_view(), name="product_view"),
         path("product-create", views.ProductCreate.as_view(), name="product_create"),
         path("product-view/update/<int:pk>/", views.ProductUpdate.as_view(), name="product_update"),
         path("product-view/delete/<int:pk>/", views.ProductDelete.as_view(), name="product_delete"),

         # Category CRUD URLs for Admin Pannel
         path("category-view", views.CategoryView.as_view(), name="category_view"),
         path("category-create", views.CategoryCreate.as_view(), name="category_create"),
         path("category-view/update/<int:pk>", views.CategoryUpdate.as_view(), name="category_update"),
         path("category-view/delete/<int:pk>", views.CategoryDelete.as_view(), name="category_delete"),

         # Review CRUD URLs for a given product
         path('product/review/<int:pk>', views.ProductReview.as_view(), name="prod-review-create"),
         path('product/review/comments/<int:pk>', views.ProductReviewDetails.as_view(), name='prod-view-review'),
         path('product/review/delete/<int:pk>', views.DeleteReview.as_view(), name='delete-review'),
         path("product/review/update/<int:review_pk>/<int:product_pk>/", views.UpdateReview.as_view(),
              name='update-review'),

         # SignUp CRUD URLs for an user
         path('signup/', views.SignUpView.as_view(), name='signup'),
         path('update_profile/<int:pk>/', views.ProfileUpdate.as_view(), name='update_profile'),
         path("update_profile/succesful", views.profile_success, name='success_update'),
         path('profile/<int:pk>/', views.ProfileView.as_view(), name="profile"),
         path('delete_profile/<int:pk>/', views.ProfileDelete.as_view(), name="delete_profile"),

         #  Order view
          path("order/view/user/<int:pk>/", views.OrdersView.as_view(), name="order-history"),
          path("order/view/user/items/<int:pk>/", views.OrderedItems.as_view(), name="ordered-items"),

         # Search engine URL
         path("search_results", views.search, name="search_results"),

         # URLs for the process of adding a product to the cart
         path("cart", views.cart_view, name="cart"),
         path("cart-add", views.add_to_cart, name="cart-add"),
         path("cart/<int:id>/increment", views.increment, name="increment"),
         path("cart/<int:id>/decrement", views.decrement, name="decrement"),
         path("cart/<int:pk>/", views.CartItemDelete.as_view(), name='cart_delete'),
         path("cart_quantity", views.quantity_input_cart, name="cart_qu"),
         path("cart-delete", views.cart_items_delete, name="cart-delete-all"),

         # URLs for placing an order
         path("order/", views.my_form_view, name='order'),
         path("order/sent", views.sent_form, name="sent-form"),

         # URLs for payment handling
         path('thank-you', views.thank_you, name='thank-you'),
         path('cancel/', views.CancelView.as_view(), name='cancel'),
         path('payment/err/', views.RefreshView.as_view(), name='refresh'),
         path('land/', views.OrderLandPageView.as_view(), name='landing-page'),
         path("payment", views.payhandling, name="payment_handling"),

         # URLs for info desk about the website
         path("privacy/", views.PrivacyView.as_view(), name="privacy"),
         path('cookies/', views.CookiesView.as_view(), name="cookies"),
         path('ads/', views.AdsView.as_view(), name='ads'),
         path('terms/', views.TermsView.as_view(), name='terms'),
         path('business/', views.BusinessView.as_view(), name='business'),
         path('help/', views.HelpView.as_view(), name='help')

]
