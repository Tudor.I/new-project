# Generated by Django 3.1.6 on 2021-02-27 19:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainpages', '0026_auto_20210225_1826'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='computersciencecategory',
            options={'permissions': (('can_view', 'Can view the category'), ('can_create', 'Can create a category'), ('can_delete', 'Can delete a category'), ('can_update', 'Can update a category')), 'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
        migrations.AlterModelOptions(
            name='products',
            options={'permissions': (('can_view', 'Can view the product'), ('can_create', 'Can create a product'), ('can_delete', 'Can delete a product'), ('can_update', 'Can update a product')), 'verbose_name': 'Product', 'verbose_name_plural': 'Products'},
        ),
        migrations.AddField(
            model_name='order',
            name='url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
