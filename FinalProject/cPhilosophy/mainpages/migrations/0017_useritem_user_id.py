# Generated by Django 3.1.6 on 2021-02-24 12:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainpages', '0016_remove_order_cart'),
    ]

    operations = [
        migrations.AddField(
            model_name='useritem',
            name='user_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
