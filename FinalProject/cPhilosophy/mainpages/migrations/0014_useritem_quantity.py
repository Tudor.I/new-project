# Generated by Django 3.1.6 on 2021-02-23 09:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainpages', '0013_remove_useritem_quantity'),
    ]

    operations = [
        migrations.AddField(
            model_name='useritem',
            name='quantity',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
