# Generated by Django 3.1.6 on 2021-02-23 09:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainpages', '0012_auto_20210222_2136'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='useritem',
            name='quantity',
        ),
    ]
