import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *
from services.decorators import decorator_cart, refresh_decorator
from services.mails import send_email


class HomePage(TemplateView):
    template_name = 'home.html'


class SubCategory(ListView):
    """Displays the categories"""
    model = SubCategories
    template_name = "computer_science.html"
    context_object_name = "category"

    def get(self, request, *args, **kwargs):
        pk_requested = self.kwargs.get('par_category')
        par_cat = get_object_or_404(Categories, pk=pk_requested)
        s_cat = SubCategories.objects.filter(par_category=par_cat)

        return render(request, 'computer_science.html', {"category": s_cat, "header": pk_requested})


class ProductsView(ListView):
    """Used for displaying the products within a specific category"""
    model = Products
    template_name = "products.html"
    context_object_name = "products"
    paginate_by = 1

    def get(self, request, *args, **kwargs):
        # If you wish to still keep the view only for specific category use below line
        category = get_object_or_404(SubCategories, pk=self.kwargs.get('category_pk'))
        queryset = Products.objects.filter(category=category)
        added = False
        if request.session.get('trigger') is not None:
            pages = request.session['normal_page']
            del request.session['trigger']
            added=True
        else:
            pages = request.GET.get('page')
        paginator = Paginator(queryset, self.paginate_by)
        try:
            products = paginator.page(pages)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        request.session['normal_page'] = pages
        return render(request, "products.html", {'products': products, 'add':added})


class ProductDetails(DetailView):
    """Displays the product details"""
    model = Products
    template_name = "product/product_details.html"
    context_object_name = "product"

    def get(self, request, *args, **kwargs):
        id = self.kwargs.get('pk')
        prod = Products.objects.get(pk=id)
        reviews = Review.objects.filter(product=prod)
        added = False
        if request.session.get('add') is not None:
            added = True
            del request.session['add']
        if len(reviews) > 0:
            avg = sum([rev.score for rev in reviews]) / len(reviews)
        else:
            avg = 0
        return render(request, 'product/product_details.html', {
            'product': prod,
            'reviews': round(avg, 2),
            'len_rev': len(reviews),
            'rev_list': [0] * int(avg),
            'add':added
        })

    def post(self, request, *args, **kwargs):
        Idd = list(request.POST.keys())
        if len(Idd) > 1:
            quantity = int(request.POST.get('quantity'))
            prod_obj = Products.objects.filter(pk=Idd[1])
            product = prod_obj.first()
            try:
                cart = UserItem.objects.get(prod_id=product)
                if quantity + cart.quantity <= product.quantity:
                    cart.quantity += quantity
                    cart.save()
            except ObjectDoesNotExist:
                cart = UserItem.objects.create(
                    name=list(prod_obj)[0].name,
                    price=list(prod_obj)[0].price,
                    img=list(prod_obj)[0].image,
                    prod_id=product,
                    quantity=quantity
                )
                cart.save()
            return redirect("prod_details", pk=Idd[1])


class ProductList(ListView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_view",)
    model = Products
    template_name = "product/product_view.html"
    context_object_name = "products"

    def get(self, request, *args, **kwargs):
        obj = Products.objects.all()
        paginator = Paginator(obj, 10)
        cur_page = self.request.GET.get("page")
        try:
            prod = paginator.page(cur_page)
        except PageNotAnInteger:
            prod = paginator.page(1)
        except EmptyPage:
            prod = paginator.page(paginator.num_pages)

        return render(request, "product/product_view.html", {'products': prod})


class ProductCreate(CreateView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_create",)
    model = Products
    template_name = "product/product_create.html"
    context_object_name = "product"
    fields = "__all__"

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.save()
            return redirect("product_view")
        else:
            return render(request, "product_create.html", {'form': form})


class ProductUpdate(UpdateView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_update",)
    model = Products
    template_name = "product/product_update.html"
    context_object_name = "products"
    fields = "__all__"
    success_url = reverse_lazy("product_view")


class ProductDelete(DeleteView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_delete",)
    model = Products
    template_name = "product/product_delete.html"
    context_object_name = "products"
    success_url = reverse_lazy("product_view")


class ProductReview(LoginRequiredMixin, CreateView):
    model = Review
    template_name = "review.html"
    context_object_name = 'form'
    fields = ['score', 'text']

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        form = ReviewForm()
        return render(request, 'review.html', {'pk': pk, 'form': form})

    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        form = self.get_form()
        if form.is_valid():
            review = Review.objects.create(
                score=form.cleaned_data['score'],
                text=form.cleaned_data['text'],
                product=Products.objects.get(pk=pk),
                user=request.user
            )
            review.save()
            return redirect("prod_details", pk=pk)
        return redirect('prod_details', pk=pk)


class ProductReviewDetails(ListView):
    model = Review
    context_object_name = "review"
    template_name = "review-view.html"
    paginate_by = 5

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        prod = Products.objects.get(pk=pk)
        rev = Review.objects.filter(product=prod)
        paginator = Paginator(rev, self.paginate_by)
        page = self.request.GET.get('page')
        try:
            review = paginator.page(page)
        except PageNotAnInteger:
            review = paginator.page(1)
        except EmptyPage:
            review = paginator.page(paginator.num_pages)

        return render(request, 'review-view.html', {'pk': pk, 'review': review})


class DeleteReview(LoginRequiredMixin, DeleteView):
    model = Review
    template_name = 'review/review_delete.html'
    context_object_name = "review"

    def post(self, request, *args, **kwargs):
        pk_com = self.kwargs.get('pk')
        obj = Review.objects.get(pk=pk_com)
        obj.delete()
        return redirect('prod-view-review', pk=obj.product.pk)


class UpdateReview(LoginRequiredMixin, UpdateView):
    model = Review
    template_name = "review/review_update.html"
    context_object_name = 'form'
    fields = ['score', 'text']

    def post(self, request, *args, **kwargs):
        global obj
        pk_com = self.kwargs.get('review_pk')
        form = self.get_form()
        if form.is_valid():
            obj = Review.objects.get(pk=pk_com)
            obj.score = form.cleaned_data['score']
            obj.text = form.cleaned_data['text']
            obj.updated = True
            obj.save()
        return redirect('prod-view-review', pk=obj.product.pk)

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('review_pk')
        obj = Review.objects.get(pk=pk)
        form = ReviewForm(initial={
            'score': obj.score,
            'text': obj.text
        })
        return render(request, 'review/review_update.html', {'form': form, 'prod_pk': self.kwargs.get('product_pk')})


class CategoryCreate(CreateView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_create",)
    model = SubCategories
    template_name = "category/category_create.html"
    context_object_name = "category"
    fields = "__all__"
    success_url = reverse_lazy("category_view")

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.save()
            return redirect("category_view")
        else:
            return render(request, "category_create.html", {'form': form})


class CategoryView(ListView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_view",)
    model = SubCategories
    template_name = "category/category_view.html"
    context_object_name = "category"


class CategoryUpdate(UpdateView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_update",)
    model = SubCategories
    template_name = "category/category_update.html"
    fields = "__all__"
    success_url = reverse_lazy("category_view")
    context_object_name = "category"


class CategoryDelete(DeleteView, PermissionRequiredMixin):
    permission_required = ("mainpages.can_delete",)
    model = SubCategories
    template_name = "category/category_delete.html"
    success_url = reverse_lazy("category_view")
    context_object_name = "category"


class SignUpView(CreateView):
    """Form responsible for registration"""
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'profile/signup.html'


class ProfileUpdate(UpdateView, LoginRequiredMixin):
    """Renders an update form for the profile"""
    model = CustomUser
    form_class = ProfileForm
    context_object_name = "profile"
    success_url = reverse_lazy("success_update")
    template_name = 'profile/update_profile.html'


def profile_success(request):
    if request.method == "GET":
        print(request.GET.keys())
    return render(request, 'profile/success_update.html')


class ProfileView(ListView, LoginRequiredMixin):
    """Profile view"""
    model = CustomUser
    context_object_name = "usr"
    template_name = 'profile/profile.html'

    def get(self, request, *args, **kwargs):
        profile = request.user.profile_image
        if profile:
            pic = True
            return render(request, 'profile/profile.html', {'pic': pic})
        else:
            pic = False
            return render(request, 'profile/profile.html', {'pic': pic})


class ProfileDelete(DeleteView, LoginRequiredMixin):
    """Delete template for a user"""
    model = CustomUser
    context_object_name = "user"
    template_name = "profile/profile_delete.html"
    success_url = reverse_lazy("home")


class OrdersView(LoginRequiredMixin, ListView):
    """Renders the orders placed by a customer"""
    model = Order
    context_object_name = 'order'
    template_name = "profile/order_view.html"

    def get(self, request, *args, **kwargs):
        order = Order.objects.filter(user_id = request.user)
        return render(request, "profile/order_view.html", {'orders':order})


class OrderedItems(LoginRequiredMixin, ListView):
    """Renders the ordered items"""
    model = OrderedItem
    context_object_name = "ordered_product"
    template_name = "profile/ordered_items.html"

    def get(self, request, *args, **kwargs):
        order_pk = self.kwargs.get('pk')
        print("HELLO")
        order = Order.objects.get(pk=order_pk)
        ordered_prod = OrderedItem.objects.filter(order_id=order)
        print(ordered_prod)
        return render(request, 'profile/ordered_items.html', {'items':ordered_prod})


class PrivacyView(TemplateView):
    template_name = 'footer/privacy.html'


class CookiesView(TemplateView):
    template_name = "footer/cookies.html"


class AdsView(TemplateView):
    template_name = 'footer/ads.html'


class TermsView(TemplateView):
    template_name = 'footer/terms.html'


class BusinessView(TemplateView):
    template_name = 'footer/business.html'


class HelpView(TemplateView):
    template_name = 'footer/help.html'


class CartItemDelete(DeleteView):
    """Delete template for a cart product"""
    model = UserItem
    template_name = "cart/cart_delete.html"
    context_object_name = "crt"
    success_url = reverse_lazy("cart")


class RefreshView(TemplateView):
    template_name = "payment/refresh.html"


class CancelView(TemplateView):
    template_name = "payment/cancel.html"


class OrderLandPageView(TemplateView):
    template_name = "payment/landing.html"


def cart_view(request):
    """Renders all the items within cart to cart.html"""
    items = UserItem.objects.all()
    total_value = UserItem.total(items)
    VAT = UserItem.VAT(total=total_value)
    shipment = UserItem.shipment(total_value)
    page = request.GET.get('page')
    paginator = Paginator(items, 3)
    try:
        cart_items = paginator.page(page)
    except PageNotAnInteger:
        cart_items = paginator.page(1)
    except EmptyPage:
        cart_items = paginator.page(paginator.num_pages)
    deleted = False
    if request.session.get('deleted') is not None:
        deleted = request.session['deleted']
        del request.session['deleted']
    return render(request, "cart/cart.html", context={"crt": cart_items,
                                                      "total": total_value,
                                                      "VAT": VAT,
                                                      "shipment": shipment,
                                                      'deleted':deleted})


def cart_items_delete(request):
    if request.method == "POST":
        cart_items = UserItem.objects.all()
        for item in cart_items:
            item.delete()
        request.session['deleted'] = True
        return  redirect("cart")
    else:
        return render(request, "cart/cart_delete_all.html")



def quantity_input_cart(request):
    """Extra validation to check whether the final quantity is the right one"""
    if request.method == 'GET':
        quant = int(request.GET.get('quantity'))
        idd = list(request.GET.keys())[1]
        prod_cart = UserItem.objects.get(pk=int(idd))
        prod_cs = Products.objects.get(pk=prod_cart.prod_id.pk)
        if 0 < quant <= prod_cs.quantity:
            prod_cart.quantity = quant
            prod_cart.save()
        else:
            form = QuantityForm(request.GET)

        return redirect('cart')


def increment(request, id):
    """It increments the quantity of a specific item with validation"""
    if request.method == "GET":
        item = UserItem.objects.get(pk=int(id))
        if 0 < (item.quantity + 1) <= item.prod_id.quantity:
            item.quantity += 1
            item.save()
        return redirect('cart')


def decrement(request, id):
    """It decerements the quantity of a specific item with validation"""
    if request.method == "GET":
        item = UserItem.objects.get(pk=int(id))
        if 0 < (item.quantity - 1) <= item.prod_id.quantity:
            item.quantity -= 1
            item.save()
        return redirect('cart')


def add_to_cart(request):
    """Adds to the cart a product"""
    if request.method == 'GET':
        Idd = list(request.GET.keys())
        if len(request.GET.keys()) > 1:
            quantity = int(request.GET.get('quantity'))
            print(request.GET.keys())
            prod_obj = Products.objects.filter(pk=Idd[1])
            product = prod_obj.first()
            try:
                cart = UserItem.objects.get(prod_id=product)
                if quantity + cart.quantity <= product.quantity:
                    cart.quantity += quantity
                    cart.save()
            except ObjectDoesNotExist:
                cart = UserItem.objects.create(
                    name=list(prod_obj)[0].name,
                    price=list(prod_obj)[0].price,
                    img=list(prod_obj)[0].image,
                    prod_id=product,
                    quantity=quantity)
                cart.save()
            if Idd[-1] == "PRODET":
                request.session['add'] = True
                return redirect('prod_details', pk=Idd[1])
            elif Idd[-1] == "PRODHTML":
                request.session['trigger'] = True
                return redirect("category_products_list", category_pk=list(request.GET.keys())[-2])
            else:
                request.session['search'] = str(Idd[-1])
                request.session['add'] = True
                return redirect("search_results")


def cart_search_request(request, search, page, validator=False):
    """Returns the output of after the input is validated"""
    added = False
    if validator:
        del request.session['search']
        if request.session.get('add') is not None:
            added = True
            del request.session['add']
    query = search.split(" ", 100)
    objects_list = []
    for word in query:
        objects_list.extend(Products.objects.filter(
            Q(name__icontains=word)
        ))
    objects_list = set(objects_list)
    paginator = Paginator(list(objects_list), 6)
    if page is None: page = request.GET.get('page')
    try:
        res = paginator.page(page)
    except PageNotAnInteger:
        res = paginator.page(1)
    except EmptyPage:
        res = paginator.page(paginator.num_pages)
    request.session['search_paginator'] = search
    request.session['paginator'] = True
    request.session['cart_page'] = page
    return {"objects_list": res,
                'result': len(objects_list),
                "search": search,
                'add':added}


def search(request):
    """Decisional tree for the search input"""
    global search_query
    if request.method == 'GET':
        search_input = request.GET.get('q')
        if search_input is not None and search_input != '':
            search_query = search_input
            page = request.GET.get('page')
        elif request.session.get('search') is not None:
            obj = request.session.get('search')
            cart_page = request.session.get('cart_page')
            res = cart_search_request(request, search=obj, page=cart_page, validator=True)
            return render(request, "search/search_result.html", res)
        elif request.session.get('paginator') and search_input != '':
            search_query = request.session.get('search_paginator')
            if request.GET.get('page') != request.session.get('cart_page'): page = request.GET.get('page')
            else: page = request.session.get('page')
            query = search_query.split(" ", 100)
            del request.session['paginator']
            del request.session['search_paginator']
            del request.session['cart_page']
        else:
            return render(request, 'search/search_error.html')
        res = cart_search_request(request, search=search_query, page=page)
        return render(request,'search/search_result.html', res )



@login_required
@decorator_cart
def my_form_view(request):
    """This function renders the order's form"""
    cart_items = UserItem.objects.all()
    form = OrderForm(initial={
        'first_name': request.user.first_name.title(),
        'last_name': request.user.last_name.title(),
        'email': request.user.email,
        'street': request.user.street.title() if request.user.street is not None else '',
        'number': request.user.number if request.user.number is not None else '',
        'city': request.user.city.title() if request.user.number is not None else '',
        'country': request.user.country.title() if request.user.country is not None else '',
    })
    total_value = UserItem.total(cart_items)
    VAT = UserItem.VAT(total_value)
    shipment = UserItem.shipment(total_value)
    context = {"form": form,
               "total": total_value,
               "VAT": VAT,
               "shipment": shipment}
    return render(request, 'order/order.html', context=context)


def sent_form(request):
    """After the validation of the form, the order and ordered items are saved within database"""
    global idd, order
    cart_items = UserItem.objects.all()
    if request.method == "GET":
        return my_form_view(request)
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if not form.is_valid():
            return render(request, "order/order.html", {'form': form})
        else:
            payment_method = form.cleaned_data['payment']
            if payment_method == 'cash':
                if UserItem.total(cart_items) == 0:
                    return HttpResponse("Something went wrong", status=404)
                order = Order.objects.create(
                    value=UserItem.total(cart_items),
                    vat=UserItem.VAT(UserItem.total(cart_items)),
                    shipment=UserItem.shipment(UserItem.total(cart_items)),
                    user=request.user,
                    first_name=form.cleaned_data['first_name'],
                    last_name=form.cleaned_data['last_name'],
                    email=form.cleaned_data['email'],
                    street=form.cleaned_data['street'],
                    number=form.cleaned_data['number'],
                    city=form.cleaned_data['city'],
                    country=form.cleaned_data['country'],
                    zip_code=form.cleaned_data['zip_code'],
                    phone_number=form.cleaned_data['phone_number'],
                    payment_status=True,
                    payment_method="Cash"
                )
                order.save()
                for item in cart_items:
                    ordered_item = OrderedItem.objects.create(
                        name=item.name,
                        price=item.price,
                        quantity=item.quantity,
                        order_id=order,
                        id_prod=item.prod_id
                    )
                    item.prod_id.quantity -= item.quantity
                    ordered_item.save()
                    item.delete()
                    item.prod_id.save()
                send_email(reciever=order.email,
                           messag=f"""
\tHi {request.user.first_name}! \n     
\tThank you for your order number: {order.id}\n    
\tThe total of your order is {order.value} lei, from which VAT {order.vat} lei and shipment is {(f"{order.shipment}" + " lei") if not (order.shipment == "FREE") else "FREE"}!     
\tWe want to thank you for ordering from cPhilosophy. May have a life full of wisdom!
                                                       """)
                request.session['order'] = order.id
                request.session["payment"] = payment_method
                return redirect('thank-you')
            else:
                previous_orders = TemporaryOrder.objects.filter(user=request.user)
                if previous_orders:
                    for previo in previous_orders:
                        previo.delete()
                if UserItem.total(cart_items) == 0:
                    return HttpResponse("Something went wrong", status=404)
                order = TemporaryOrder.objects.create(
                    value=UserItem.total(cart_items),
                    vat=UserItem.VAT(UserItem.total(cart_items)),
                    shipment=UserItem.shipment(UserItem.total(cart_items)),
                    first_name=form.cleaned_data['first_name'],
                    last_name=form.cleaned_data['last_name'],
                    email=form.cleaned_data['email'],
                    street=form.cleaned_data['street'],
                    number=form.cleaned_data['number'],
                    city=form.cleaned_data['city'],
                    country=form.cleaned_data['country'],
                    zip_code=form.cleaned_data['zip_code'],
                    phone_number=form.cleaned_data['phone_number'],
                    user=request.user
                )
                order.save()
                return render(request, "payment/landing.html", {"order": order})


def thank_you(request):
    s_order = request.session.get("order")
    s_payment = request.session.get("payment")
    if request.method == "GET":
        if s_order:
            if s_payment == "Online":
                m = True
            else:
                m = False
            return render(request, "payment/thank_you.html", {"order": s_order, "payment": s_payment, 'flag': m})

    return render(request, 'payment/thank_you.html')


def payhandling(request):
    if request.method == "GET":
        return HttpResponse("Not allowed", status=403)
    if request.method == "POST":
        cart_items = UserItem.objects.all()
        if UserItem.total(cart_items) == 0:
            return HttpResponse("Something went wrong", status=404)
        id_tmp_order = list(request.POST.keys())[0]
        try:
            tmp_order = TemporaryOrder.objects.get(pk=id_tmp_order)
        except ObjectDoesNotExist:
            return HttpResponse("Something went wrong", status=404)
        order = Order.objects.create(
            value=tmp_order.value,
            vat=tmp_order.vat,
            shipment=tmp_order.shipment,
            first_name=tmp_order.first_name,
            last_name=tmp_order.last_name,
            email=tmp_order.email,
            street=tmp_order.street,
            number=tmp_order.number,
            city=tmp_order.city,
            country=tmp_order.country,
            zip_code=tmp_order.zip_code,
            phone_number=tmp_order.phone_number,
            user=request.user,
            payment_status=True,
            payment_method="Online"
        )
        order.save()
        for item in cart_items:
            ordered_item = OrderedItem.objects.create(
                name=item.name,
                price=item.price,
                quantity=item.quantity,
                order_id=order,
                id_prod=item.prod_id
            )
            item.prod_id.quantity -= item.quantity
            item.prod_id.save()
            ordered_item.save()
            item.delete()
        send_email(reciever=order.email,
                   messag=f"""
    Hi {request.user.first_name}! \nThank you for your order number: {order.id}\n

The total of your order is {order.value} lei, from which VAT {order.vat} lei and shipment is {(f"{order.shipment}" + " lei") if not (order.shipment == "FREE") else "FREE"}!
    
    We want to thank you for ordering from cPhilosophy. May have a life full of wisdom!
    
  
                                           """)
        tmp_order.delete()
        request.session['order'] = order.id
        request.session["payment"] = order.payment_method
        return redirect('thank-you')
