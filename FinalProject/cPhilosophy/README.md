## **cPhilosophy e-commerce**
**The project represents an attempt to create a fully functional on-line store.**

**Requirements:**

**-> create a virtual environment**                           
`python -m venv venv`

**-> install django framework with all the used libraries**

`pip install -r requirements.txt`


**-> be sure you configured the interpreter**

**-> run the application**

`python manage.py runserver`

The application contains:
-
- _templates folder_
- _static folder_
- _media folder_
- _mainpages (the app)_
- _cPhilosophy_ 

Main goal:
-
**A guest should be able to create an account from where he can check his or her 
past orders. If you have an account, everybody will be rewarded with a book
until 1st of June and you can track your history orders. Based on the user/guest orders, some fidelity discounts
can be applied for you.**

**He or she should be able to add products and remove from the cart.**
**The admin is responsible for the products that are being displayed, the products
within the database etc. He is also responsible for any other updates.**
**Once an order has been placed, the user / guest will receive an email
with the ordered products**

**If the guest / user wants to cancel his or her order, they can do it within
24h once the order has been placed**

Description:
-
**Django Apps**

- mainpages


**URLs:**

- home - displays the home page
- ComputerScience/ - displays the cateogries
- products - displays the all products that belongs to a category
- signup - displays the login form, which contains 
other URLs (i.e login logout flow).
  
- profile - displays the profile account of the user
- update_profile - displays an update form of the account
- delete_profile - displays a page from where you can delete your account
- search_results - displays the results of a specific search input given by the user
- cart - displays the products that the user intends to buy
- CRUD for products and categories - encompasses the logical flow for creating 
  products and categories
- Payment - customized, basic payment method for the online payment feature
**Views.py**

- 19 classes
- 11 functions

**Templates**

 - 28 templates
 - registration file with 7 templates


