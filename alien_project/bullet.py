import pygame
from pygame.sprite import Sprite
from settings import Settings
import random
class Bullet(Sprite):
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.bullet_image = pygame.image.load("Untitled.png")
        self.bullet_image = pygame.transform.scale(self.bullet_image, (20,30))
        self.rect = self.bullet_image.get_rect()
        self.bullet_color = (random.randint(1,250), random.randint(1,250), random.randint(1,250))
        self.rect.midtop = ai_game.ship.rect.midtop
        # self.rect = self.rect.y
        # self.rect = pygame.Rect(self.settings.bullet_image)
        self.y = float(self.rect.y)

    def update(self):
        """Move the bullet up the screen"""
        self.y -= self.settings.bullet_speed
        self.rect.y = self.y
    def blitme_bullet(self):
        self.screen.blit(self.bullet_image, self.rect)

    def draw_bullet(self):
        self.screen.blit(self.bullet_image, self.rect)