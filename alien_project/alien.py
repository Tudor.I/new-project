import pygame
import sys
from pygame import sprite
from settings import Settings
from racheta import Ship
from bullet import Bullet
from alien_fleet import Alien
import random
a = pygame.image.load('somb.jpg')
class ChickenInvasion:
    """Here we create the whole needed features for configuration"""
    def __init__(self):
        """This is how we initialize the game"""
        pygame.init()
        self.settings = Settings()
        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height))
        pygame.display.set_caption("Chicken Invaders")
        pygame.display.set_icon(pygame.image.load('download.jpg'))
        self.bg_color = (100, 231, 236)
        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()
        self._create_fleet()

    def _create_fleet(self):
        """Create a fleet of enemies"""
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        available_space_x = self.settings.screen_width - (2 * alien_width)
        number_aliens_x = available_space_x // (2 * alien_width)
        #determine de nr of rouws
        ship_height = self.ship.rect.height
        available_space_y = (self.settings.screen_height -
                             (3 * alien_height) - ship_height)
        number_rows = available_space_y // (2 * alien_height)
        #Creating the first row of aliens
        for row_number in range(number_rows):
            for alien_number in range(number_aliens_x):
                self._create_alien(random.randint(0, alien_number), row_number)


    def _create_alien(self, alien_number, row_number):
        alien1 = Alien(self)
        alien_width, alien_height = alien1.rect.size
        alien1.x = alien_width + 2 * alien_width * alien_number + alien_width
        alien1.rect.x = alien1.x
        alien1.rect.y = alien1.rect.height + 2 * alien1.rect.height * row_number
        self.aliens.add(alien1)

    def _check_keydown_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_UP:
            self.ship.moving_down = True
        elif event.key == pygame.K_DOWN:
            self.ship.moving_up = True
        elif event.key == pygame.K_ESCAPE:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()

    def _check_keyup_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False
        elif event.key == pygame.K_UP:
            self.ship.moving_down = False
        elif event.key == pygame.K_DOWN:
            self.ship.moving_up = False



    def _check_events(self):
        """This is how we respond to any keypress and mouse events"""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
    def _fire_bullet(self):
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    def _update_bullets(self):
        """Update the position of a bullet"""
        self.bullets.update()
        for bullet in self.bullets.copy():
            for bullet in self.bullets.copy():
                if bullet.rect.bottom <= 0:
                    self.bullets.remove(bullet)


    def _update_screen(self):
        self.screen.blit(self.settings.bg_image, (0,0))
        # self.screen.blit(self.settings.bullet_image, (0,0))
        self.ship.blitme()
        self.ship.update()

        pygame.display.flip()
        pygame.display.update()
        for bullet in self.bullets.sprites():
            bullet.draw_bullet()

        self.aliens.draw(self.screen)
    def run_game(self):
        """This is how we start the game"""
        while True:

            self._check_events()
            self._update_screen()
            pygame.display.update()
            self.bullets.update()
            self._update_bullets()

if __name__ == '__main__':
    ai = ChickenInvasion()
    ai.run_game()