import pygame
class Settings:
    """Here we save our settings of the game"""
    def __init__(self):
        self.screen_width = 1000
        self.screen_height = 650
        self.bg_image = pygame.image.load("dasda.jpg")
        self.bg_image = pygame.transform.scale(self.bg_image, (1000, 650))
        self.ship_speed = 1.5
        self.bullet_speed = 0.9
        self.bullet_width = 5
        self.bullet_height = 10
        self.bullet_color = (230,200,100)

        self.bullet_image = pygame.image.load("bullet13.jpg")
        self.bullets_allowed = 5
# from datetime import date
# a = date()