import pygame
from pygame.sprite import Sprite

class Alien(Sprite):
    """Reprezenting the enemy fleet"""

    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen

        #we load the enemy's ship
        self.image = pygame.image.load("UFO_FUCKING.png")
        self.rect = self.image.get_rect()

        #Starting point
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height
        #store enemy's exact horizontal position
        self.x = float(self.rect.x)
