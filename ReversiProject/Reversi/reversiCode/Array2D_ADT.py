from reversiCode.ArrayADT import Array
"""
A two-dimensional array consists of a collection of elements organized into rows
and columns. Individual elements are referenced by specifying the specific row and
column indices (r, c), both of which start at 0.
pg 50 for more details about the implementation of a 2D array
"""

# Implementation of the Array2D ADT using an array of arrays

class Array2D:
    #Creates a 2-D array of size numRow x numCols.
    def __init__(self, numRows, numCols):
        #Create a 1-D array to store an array reference for each row
        self._theRows = Array(numRows)
        #Create the 1-D arrays for each row of the 2D array
        for i in range(numRows):
            self._theRows[i] = Array(numCols)

    #returns the number of rows in the 2D array
    def numRows(self):
        return len(self._theRows)

    #returns the number of columns in the 2D array
    def numCols(self):
        return len(self._theRows[0])

    #Clears the array by setting every element to the given value.
    def clear(self, value):
        for row in self._theRows:
            row.clear(value)

    #Gets the contents of the element at positiom [i,j]
    def __getitem__(self, ndxTuple):
        assert len(ndxTuple) == 2, 'Invalid numebr of array suscripts'
        row = ndxTuple[0]
        col = ndxTuple[1]
        assert (row >= 0 and row<self.numRows()) and (
            col >=0 and col<self.numCols()), "Array subscript out of range"

        the1dArray = self._theRows[row]
        return the1dArray[col]

    def __setitem__(self, ndxTuple, value):
        assert len(ndxTuple) == 2, 'Invalid numer of arrat suscripts'
        row = ndxTuple[0]
        col = ndxTuple[1]
        assert (row >= 0 and row < self.numRows()) and (
                col >= 0 and col < self.numCols()), "Array subscript out of range"
        the1dArray = self._theRows[row]
        the1dArray[col] = value



    


