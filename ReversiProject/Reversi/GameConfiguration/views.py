from django.shortcuts import render
from django.views.generic import CreateView, TemplateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import CreateView, UpdateView
from GameConfiguration.forms import SignUpForm, ProfileForm
from django.contrib.auth.models import User
from reversiCode.Thegamesetuppart2 import ReversiGame, draw, game, draw_grid
import json
from django.http import HttpResponse


class HomeView(TemplateView):
    template_name = 'home.html'

def table(request):
    reversi = ReversiGame()
    grid = draw_grid(reversi)
    player = 1
    if request.method == "POST":
        coord = request.POST.keys()
        crd = list(coord)[1].split("_")
        r = int(crd[0]) - 1
        c = int(crd[1]) - 1

        if reversi.has_valid_move(player):
            if reversi.isLegalMove(r, c, player):
                reversi.makeMove(r, c, player)
                ctx = draw_grid(game)
                return render(request, 'table.html', context=ctx)
            else:
                print("Invalid move. Try again")

        reversi.game(player, r, c)
        if reversi.has_valid_move(reversi.inverse(player)):
            player = reversi.inverse(player)

    return render(request, 'table.html', context={"table": grid})







class SignUpView(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('home')
    template_name = 'signup.html'

class ProfileView(UpdateView):
    model = User
    form_class = ProfileForm
    context_object_name = "profile"
    success_url = reverse_lazy('home')
    template_name = 'profile.html'

