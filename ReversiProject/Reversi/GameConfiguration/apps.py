from django.apps import AppConfig


class GameconfigurationConfig(AppConfig):
    name = 'GameConfiguration'
