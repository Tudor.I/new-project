def numJewelsInStones(jewels: str, stones: str) -> int:
    assert 1 <= len(jewels) <= 50, "Wrong input. It shold be between 1 and 50 characters long."
    assert 1 <= len(stones) <= 50, "Wrong input. It shold be between 1 and 50 characters long."

    """Checking whether jewels characters are unique"""
    l_car = [c for c in jewels]
    set_car = set(l_car)
    if len(set_car) != len(l_car): return False
    dic = {}
    i = 0
    for j in jewels:
        dic[j] = 1
        i+=1
    count = 0
    for c in stones:
        if c in dic:
            count +=1
    return count

j = "aA"
s = "aaaaaA"


def singleNumber( nums):
    d = {}
    for nr in nums:
        if nr not in d:
            d[nr] = 1
        else:
            d[nr] += 1
    for elem in d:
        if d[elem] == 1:
            return elem

array = [4,4,2,1,2,2,5,5,7,7]



def firstUniqChar(s: str):
    dic = {}
    i = 0
    for c in s:
        if c in dic:
            ls = dic[c]
            ls[0]+=1
            dic[c] = ls
        else:
            dic[c] = [1, i]
        i+=1
    for c in dic:
        if dic[c][0] == 1:
            return dic[c][1]
    return -1

dic = {}
ex = "dddccdbba"


def f(s):
    dic = {}
    for i in range(len(s)):
        if s[i] in dic:
            dic[s[i]] += 1
        else:
            dic[s[i]] = 1
    for i in range(len(s)):
        if dic[s[i]] == 1:
            return i
    return -1


lista = [7,1,5,3,6,4]
def maxProfit(prices):
    profit = 0
    i = 0
    first_price = prices[i]
    for i in range(1, len(prices)):
        if prices[i] > first_price:
            curprofit = prices[i] - first_price
            if curprofit > profit:
                profit = curprofit
        else:
            first_price=prices[i]

    return profit


b = "abc"
c = "ahbgdc"

def isSubsequence(s, t):
    i, j = 0, 0
    while i < len(s) and j < len(t):
        if s[i] == t[j]:
            i += 1
        j += 1
    return True if i == len(s) else False

n = [3,2,3]
t = 6
def twoSum(nums, target):
    i = 0
    j = 1
    dic = {}
    while j < len(nums):
        if nums[i] + nums[j] == target:
            return [i, j]
        i += 1
        j += 1




def fibonacci_generator(n):
    a = 0
    b = 1
    i = 0
    while i <= n:
        yield b
        a, b = b, a + b
        i +=1


def climbStairs( n):
    dic = {}
    if n <= 1:
        return 1
    if n in dic:
        return dic[n]
    dic[n] = climbStairs(n - 1) + climbStairs(n - 2)
    return dic[n]






a = {1,2,3,4}
b = {23,3,2,5,6}
# print(a.difference(b))

n = [2,7,11,15]


def twoSum(numbers, target):
    low = 0
    high = len(numbers) - 1
    while low <= high:
        mid = (low + high) // 2
        s = numbers[low] + numbers[high]
        if s == target:
            return [low + 1, high + 1]
        elif s < target:
            low = mid if (numbers[mid]+ numbers[high] < target) else low + 1
        else:
            high = mid if (numbers[mid] + numbers[low] > target) else high - 1
m = [3,24,50,79,88,150,345]



def isPerfectSquare(digits):
    if digits[-1] != 9:
        digits[-1] += 1
    else:
        i = len(digits) - 1
        while i >= 0:
            if digits[i] == 9:
                digits[i] = 0
                i -= 1
            else:
                digits[i] += 1
                break
        if digits[0] == 0:
            digits = [1] + digits
    return digits

l = [8,9,9,9]
# print(isPerfectSquare(l))

def productExceptSelf(nums):
    left, right = [0] * len(nums), [0] * len(nums)
    left[0] = 1
    for i in range(1, len(nums)):
        left[i] = left[i - 1] * nums[i - 1]
    right[len(nums) - 1] = 1
    i = len(nums) - 2
    while 0 <= i:
        right[i] = right[i + 1] * nums[i + 1]
        i -= 1
    res_list = [0] * len(nums)
    for i in range(len(nums)):
        res_list[i] = right[i] * left[i]
    return res_list


m = [1,2,3,4]

# print(productExceptSelf(m))


class HasElement:
    def __init__(self, key, value):
        self.key = key
        self.value = value


class HasTable:
    def __init__(self):
        drawers = 10**3
        self.container = list()
        for i in range(drawers):
            self.container.append([])


    def __setitem__(self, key, value):
        assert isinstance(key, int), "Wrong key"
        if key >= 10 ** 3:
            key = key % 10 ** 3
        flag = False
        for el in self.container[key]:
            if el.key == key:
                el.value = value
                flag = True
        if flag is False:
            raise KeyError("The key has not been found")

    def __getitem__(self, key):
        assert isinstance(key, int), "wrong key"
        if key >= 10 ** 3:
            key = key % 10 ** 3
        drawer = self.container[key]
        for el in drawer:
            if el.key == key:
                return key
        raise KeyError("The key does not exist")

    def __del__(self, key):
        assert isinstance(key, int), "wrong key"
        if key >= 10**3:
            key = key % 10**3
        drawer = self.container[r]
        for el in drawer:
            if el.key == key:
                drawer.remove(el)
                return el
        raise KeyError("The key does not exist")


    def append(self, item):
        assert isinstance(item, HasElement), "wrong input"
        key = item.key
        if key >= 10**3:
            key = key % 10**3
        self.container[key].append(element)


class MinStack:
    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.size = 0

    def push(self, x):
        if self.size == 0:
            self.stack.append(x)
            self.stack.append(x)
            self.size += 2
        else:
            self.stack.append(x)
            self.stack.append(min(x, self.stack[self.size - 1]))
            self.size += 2

    def pop(self):
        if self.size == 0:
            return 0
        self.stack.pop(self.size - 1)
        self.size -=1
        self.stack.pop(self.size - 1)
        self.size -= 1


    def top(self):
        if self.size == 0:
            return 0
        return self.stack[-2]

    def getMin(self):
        return self.stack[-1]


def PLM(s):
    stack = []
    size = - 1
    for token in s:
        if token in "{[(":
            stack.append(token)
            size += 1
        else:
            if size >= 0:
                close = stack[size]
                if (close == "{" and token == "}") or (close == "[" and token == "]") or (
                        close == "(" and token == ")"):
                    stack.pop(size)
                    size -= 1
                else:
                    return False

    if size == -1:
        return True
    else:
        return False
#
# m = "[([]])"
# print(PLM(m))


def canConstruct(ransomNote, magazine):
    i = 0
    j = 0
    while i < len(ransomNote) and j < len(magazine):
        if ransomNote[i] == magazine[j]:
            i += 1
            j += 1
        else:
            j += 1
    if i == len(ransomNote):
        return True
    else:
        return False


t = "aa"
g = "aab"

# print(canConstruct(t, g))


class Solution(object):
    def findComplement(self, num):
        """
        :type num: int
        :rtype: int

        why cannot use ~???
        """
        compare = 1
        rst = 0
        while compare <= num:
            if not compare & num:
                rst += compare
            compare = compare << 1
        return rst




p = 9
h_p = [1,1]

#first_problem
def capacitate_maxima_bazin(pereti, h_pereti):
    distanta_fata_de_p1 = {}
    d = 1
    for ndx in range(len(h_pereti)):
        distanta_fata_de_p1[ndx] = d
        d+=1
    longest_pool = 0
    for i in range(len(h_pereti)):
        j = len(h_pereti) - 1
        while i < j:
            if i == 0:
                distanta_fata_de_p11 = 0
            else:
                distanta_fata_de_p11 = distanta_fata_de_p1[i - 1]
            tmp_long = min(h_pereti[i], h_pereti[j]) * (distanta_fata_de_p1[j-1] - distanta_fata_de_p11)
            if tmp_long > longest_pool:
                longest_pool = tmp_long
            j -= 1
    return longest_pool

def nr_maxim_de_orase(nr_orase, h_orase):
    global numberbigerthan
    cnt_max_orase = {}
    for i in range(nr_orase):
        if h_orase[i] not in cnt_max_orase:
            cnt_max_orase[h_orase[i]] = 1
        else:
            cnt_max_orase[h_orase[i]]+=1
    max_num = 0
    for i in range(nr_orase):
        if h_orase[i] > max_num:
            max_num=h_orase[i]

    h_orase_cu_h_mai_mare_egal_decat_h  = {}
    for i in reversed(range(max_num+1)):
        try:
            bigger = h_orase_cu_h_mai_mare_egal_decat_h[i+1]
        except (IndexError, KeyError):
            bigger = 0
        try:
            cnti = cnt_max_orase[i]
        except (IndexError, KeyError):
            cnti = 0
        h_orase_cu_h_mai_mare_egal_decat_h[i] = bigger + cnti
    print(h_orase_cu_h_mai_mare_egal_decat_h)
    for h in reversed(range(max_num+1)):
        if h == h_orase_cu_h_mai_mare_egal_decat_h[h]:
            return h
    return None

m = 2
c = [3,5]


from math import inf
def minCoin(coins, amount):
    dp = [0] + [inf] * amount
    for coin in coins:
        for i in range(coin, amount+1):
            x = dp[i]
            z = dp[i-coin] + 1
            dp[i] = min(x, z)
    print(dp)


c = [3, 4, 5]
am = 20

minCoin(c,am)
def isPla():
    s = "A man, a plan, a canal: Panama"
    print(s.strip())
    s = s.lower()
    s = s.replace(' ', '')
    new_string = ''
    for i in range(len(s)):
        if s[i].isalpha():
            new_string = new_string+s[i]
    j = len(new_string) - 1
    i = 0
    while i < j:
        if new_string[i] != new_string[j]:
            return False
        i+=1
        j-=1
    return True


class Salut():

    def returN_salut(self):
        class lume():
            def returN_lume(self):
                def altceva():
                    return "UHAAAAAA"
                return 'lume ' + altceva()
        return "Salut " + lume().returN_lume()

obj = Salut()


def decorator(funct):
    def decoreaz(var):
        print(var)
        return f"<p> +{funct(var)}+ </p>"
    return decoreaz


@decorator
def ceva(txt):
    return txt.lower()

m = "Ceva CEVA"

print(ceva(m))


def finalPrices(prices):
    """
    :type prices: List[int]
    :rtype: List[int]
    """
    i, stack = [], [0]
    for j in prices[::-1]:
        while j < stack[-1]: stack.pop()
        i += [j - stack[-1]]
        stack += [j]
    return i[::-1]

print(finalPrices([8,4,6,2,3]))
