# # class numelemeu:
# #     def __init__(self):
# #         self.nume = 'python'
# #
# #     @property
# #     def materie(self, s):
# #         return s.nume
# #
# #
# # class parinte:
# #     @classmethod
# #     def __init__(c):
# #         c.var1 = "Salut Python"
# #
# # class copil(parinte):
# #     def __init__(self):
# #         parinte.__init__()
# #
# # obj = copil()
# #
# # class x:
# #     @staticmethod
# #     def __init__():
# #         print("salut lume")
# #
# # o = x()
# #
# # from functools import wraps
# #
# # def clear_me(fct):
# #     @wraps(fct)
# #     def clear(var):
# #         return fct(var).strip("<p/>")
# #     return clear
# # @clear_me
# # def tagget_out(sir):
# #     return "<p>" + str(sir) + "<p/>"
# #
# # print(tagget_out("imi place python"))
#
# # class a:
# #     class b:
# #         var1 = "salut lume"
# # class c:
# #     var2 = a.b.var1
# #
# # print(c.var2)
# #
# # class test:
# #     def __init__(self):
# #         print("salut lume")
# #
# #     @staticmethod
# #     def __call__(self, *args, **kwargs):
# #         return "imi place python"
# #
# # obj = test()
#
# class Solution:
#     @staticmethod
#     def isAdjacent(curNdx, array):
#         left = curNdx - 1
#         right = curNdx + 1
#
#         if (array[left] == 0 and array[right] == 0) or (array[left] == 1 and array[right] == 1):
#             return True
#         else:
#             return False
#
#     def prisonAfterNDays(self, cells, n):
#         for times in range(n):
#             tmp_array = [0] * len(cells)
#             for i in range(len(cells)):
#                 if i == 0:
#                     tmp_array[i] = 0
#                 elif i == len(cells) - 1:
#                     tmp_array[i] = 0
#                 elif self.isAdjacent(i, cells):
#                     tmp_array[i] = 1
#                 else:
#                     tmp_array[i] = 0
#             print(cells)
#             cells = tmp_array
#
#         return cells
#
#
# c = [0, 1, 0, 1, 1, 0, 0, 1]
# n = 7
#
# cl = Solution()
# print(cl.prisonAfterNDays(c, n))
#
#
# class Solution:
#     def prisonAfterNDays(self, cells: List[int], N: int) -> List[int]:
#         first = getNext(cells)
#         cells = getNext(first)
#         dic = {0: first}
#         period = 1
#         while (first != cells):
#             dic[period] = cells
#             cells = getNext(cells)
#             period += 1
#         address = (N - 1) % period
#         return dic[address]
#
#     def getNext(curr):
#         ret = []
#         for j in range(8)
#             if j == 0 or j == 7:
#                 ret.append(0)
#             elif curr[j - 1] == curr[j + 1]:
#                 ret.append(1)
#             else:
#                 ret.append(0)
#         return ret
#

def clear(funct):
    def dec(var):
        m = funct(var)
        return m.strip("p")
    return dec

@clear
def f(text):
    return "<p>"+text+"<p/>"

print(f("Imi place python"))

class Test:
    def __init__(self):
        print("lume")

    @staticmethod
    def __call__(self, *args, **kwargs):
        print(self)
        print("Salut ba")

x = Test()
x.__call__("STH")

class X:
    def __init__(self):
        self.a = -4
        print("LUME")

    @staticmethod
    def age(ag):
        if ag>0:
            return "YAs"
    @classmethod
    def ceva(self):
        if self.a < 0:
            print("PA")

class M(X):
    def __init__(self):
        super().__init__()
        print("AHA")


s = M()

s.ceva()

M.age(2)