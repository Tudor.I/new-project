
a = [[1, 2 ,3], [4,5,6], [7,8,9]]

# Captilul 1 -> Probleme Propuse -> Ex1
def a_a1(matrix):
    """Calculeaza suma de pe diagonala principala"""
    sum = 0
    i = 0
    while i < len(matrix):
        j = i
        while j <= i:
            sum += matrix[i][j]
            j+=1
        i+=1
    print(sum)

def a_a2(matrix):
    """Suma elementelor aflate deasupra diagonalei principale"""
    sum = 0
    i = 0
    while i<len(matrix):
        j = i+1
        while j<len(matrix):
            sum+=matrix[i][j]
            j+=1
        i+=1
    print(sum)

def a_a3(matrix):
    """Suma elementelor aflate dedesubtul diagonalei principale"""
    sum = 0
    i = 1
    while i<len(matrix):
        j = 0
        while j<i:
            sum+=matrix[i][j]
            j+=1
        i+=1
    print(sum)

def a_b1(matrix):
    """Suma elementelor de pe diagonala secundara"""
    sum = 0
    i = 0
    while i < len(matrix):
        j = len(matrix)-1-i
        while j>=len(matrix)-1-i:
            sum+=matrix[i][j]
            j-=1
        i+=1
    print(sum)


def a_b2(matrix):
    """Suma elementelor de deasupra diagonalei secundare"""
    sum = 0
    i = 0
    while i < len(matrix) - 1:
        j = 0
        while j < len(matrix)-i-1:
            sum += matrix[i][j]
            j+=1
        i+=1
    print(sum)

def a_b3(matrix):
    """Suma elementelor aflate dedesubtul diagonalei secundare"""
    sum = 0
    i = 1
    while i < len(matrix):
        j = len(matrix) - 1
        while j>=len(matrix) - i:
            sum+=matrix[i][j]
            j-=1
        i+=1
    print(sum)

# Captilul 1 -> Probleme Propuse -> Ex2

b =  [[1,2,6,9], [5,10,12,16],[3,4,20,30]]

def ex2(matrix):
    """Interschimbarea coloanelor unei matrici cu M linii si N coloane
    a.i. linia k sa aiba elementele ordonate crescator"""
    # pasul 1 -> transcriem toate elementele matricei intr-un array
    temp_arr = [0]*(len(matrix)*len(matrix[0]))
    ndxArr = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            temp_arr[ndxArr] = matrix[i][j]
            ndxArr+=1
    # pasul 2 -> sortam array ul folosind insertion sort
    for i in range(1, len(temp_arr)):
        j = i - 1
        key = temp_arr[i]
        while j >= 0 and key < temp_arr[j]:
            temp_arr[j + 1] = temp_arr[j]
            j -= 1
        temp_arr[j + 1] = key
    #pasul 3
    ndxArr = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = temp_arr[ndxArr]
            ndxArr+=1
    print(matrix)

def ordo(temp_arr):
    for i in range(1, len(temp_arr)):
        j = i-1
        key = temp_arr[i]
        while j>=0 and key < temp_arr[j]:
            temp_arr[j+1] = temp_arr[j]
            j-=1
        temp_arr[j+1] = key
    print(temp_arr)


def ex3(matrix):
    offsets = [(0, 1), (1,0), (-1, 0), (0, -1), (-1, -1), (-1,1), (1, -1),(1,1)]
    ret = True
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            for offset in offsets:
                try:
                    if matrix[i+offset[0], j+offset[1]] > matrix[i][j]:
                        ret = False
                except IndexError:
                    continue
            if ret:
                print(f"coordonatele sunt: i -> {i}, j -> {j}")

if __name__ == "__main__":
    ex3(b)



